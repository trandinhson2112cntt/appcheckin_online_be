import * as dotenv from 'dotenv';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export class ConfigService {
    constructor() {
        // tslint:disable-next-line:no-angle-bracket-type-assertion

        dotenv.config({
            path: `.env`,
        });
    }

    public get(key: string): string {
        return process.env[key];
    }

    public getNumber(key: string): number {
        return Number(this.get(key));
    }

    // get typeOrmConfig(): TypeOrmModuleOptions {
    //     let entities = [__dirname + '/../../modules/**/entities/*.entity.ts'];

    //     if ((<any> module).hot) {
    //         const entityContext = (<any> require).context('./../modules', true, /\.entity\.ts$/);
    //         entities = entityContext.keys().map((id) => {
    //             const entityModule = entityContext(id);
    //             const [entity] = Object.values(entityModule);
    //             return entity;
    //         });
    //     }
    //     return {
    //         type: 'postgres',
    //         host: this.get('POSTGRES_HOST'),
    //         port: this.getNumber('POSTGRES_PORT'),
    //         username: this.get('POSTGRES_USERNAME'),
    //         password: this.get('POSTGRES_PASSWORD'),
    //         database: this.get('POSTGRES_DATABASE'),
    //         logging: true,
    //         synchronize: true,
    //         entities: ['../modules/**/entities/*.entity.ts'],
    //     };
    // }
}
