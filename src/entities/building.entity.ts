import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Room } from './room.entity';

@Entity()
export class Building {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @OneToMany(type => Room, (r: Room) => r.building) rooms: Room[];
}
