import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Building } from './building.entity';
import { Floor } from './floor.entity';
import Schedule from './schedule.entity';

@Entity()
export class Room {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @ManyToOne(type => Building, (b: Building) => b.rooms) building: Building;
    @Column('text') buildingName: string;

    @ManyToOne(type => Floor, (b: Floor) => b.rooms) room: Floor;
    @Column('text') floorName: string;

    @OneToMany(type => Schedule, (s: Schedule) => s.classSubject) schedules: Schedule[];
}
