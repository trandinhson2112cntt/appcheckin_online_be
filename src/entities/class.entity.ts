import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Major } from './major.entity';
import { Student } from './student.entity';

@Entity()
export class Class {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @ManyToOne(type => Major, (m: Major) => m.classes) major: Major;
    @Column('text') majorName: string;

    @OneToMany(type => Student, (s: Student) => s.class) students: Student[];
}
