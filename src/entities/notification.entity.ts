import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import ClassSubject from './class-subject.entity';
import StudentClassSubject from './studentClassSubject.entity';

@Entity()
export default class Notification {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') title: string;

    @Column('text') content: string;

    @Column('timestamp') createDate: Date;

    @ManyToOne(type => ClassSubject, (cs: ClassSubject) => cs.notifications) classSubject: ClassSubject;
    @Column('text') classSubjectName: string;

    @ManyToOne(type => StudentClassSubject, (cs: StudentClassSubject) => cs.notifications) studentClassSubject: StudentClassSubject;
    @Column('text') studentName: string;

    @Column('boolean') status: boolean;
}
