import { Entity, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import Schedule from './schedule.entity';
import { Student } from './student.entity';

@Entity()
export default class Absent {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => Schedule, (sc: Schedule) => sc.points) schedule: Schedule;

    @ManyToOne(type => Student, (s: Student) => s.points) student: Student;
}
