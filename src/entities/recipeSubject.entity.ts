import { Entity, PrimaryGeneratedColumn, Column,  OneToOne, JoinColumn } from 'typeorm';
import ClassSubject from './class-subject.entity';

@Entity()
export default class RecipeSubject {
    @PrimaryGeneratedColumn('uuid') id: string;

    @OneToOne(type => ClassSubject)
    @JoinColumn() classSubject: ClassSubject;
    @Column('text') classSubjectName: string;

    @Column('real') coefficientDailyScore: number;

    @Column('real') coefficientAttendanceScore: number;

    @Column('real') coefficientMidTermScore: number;
}
