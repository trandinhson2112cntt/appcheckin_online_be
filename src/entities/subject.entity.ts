import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import ClassSubject from './class-subject.entity';

@Entity()
export default class Subject {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @Column('text') numberOfCredit: string;

    @Column('real') scorePass: number;

    @Column('text') nameGroup: string;

    @Column('text') type: string;

    @OneToMany(type => ClassSubject, (c: ClassSubject) => c.teacher) classSubjects: ClassSubject[];

}
