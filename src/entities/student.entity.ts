import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { Class } from './class.entity';
import Point from './point.entity';
import StudentClassSubject from './studentClassSubject.entity';
import { User } from './user.entity';

@Entity()
export class Student {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') education: string;

    @ManyToOne(type => Class, (c: Class) => c.students ) class: Class;

    @Column('text') className: string;

    @OneToMany(type => StudentClassSubject, (scs: StudentClassSubject) => scs.student) studentClasses: StudentClassSubject[];
    @OneToMany(type => Point, (p: Point) => p.student) points: Point[];

    @OneToOne(type => User)
    @JoinColumn()
    user: User;
}
