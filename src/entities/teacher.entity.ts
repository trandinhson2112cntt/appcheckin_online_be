import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { Major } from './major.entity';
import ClassSubject from './class-subject.entity';
import { User } from './user.entity';

@Entity()
export default class Teacher {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => Major, (m: Major) => m.teachers) major: Major;

    @Column('text') majorName: string;

    @OneToMany(type => ClassSubject, (c: ClassSubject) => c.teacher) classSubjects: ClassSubject[];

    @OneToOne(type => User)
    @JoinColumn()
    user: User;
}
