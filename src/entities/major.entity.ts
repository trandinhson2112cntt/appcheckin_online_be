import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Class } from './class.entity';
import Teacher from './teacher.entity';

@Entity()
export class Major {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @OneToMany(type => Class, (c: Class) => c.major) classes: Class[];

    @OneToMany(type => Teacher, (t: Teacher) => t.major) teachers: Teacher[];
}
