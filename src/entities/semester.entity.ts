import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import ClassSubject from './class-subject.entity';

@Entity()
export default class Semester {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @OneToMany(type => ClassSubject, (c: ClassSubject) => c.semester) classSubjects: ClassSubject[];
}
