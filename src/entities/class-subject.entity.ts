import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne, OneToMany } from 'typeorm';
import Subject from './subject.entity';
import Teacher from './teacher.entity';
import Semester from './semester.entity';
import StudentClassSubject from './studentClassSubject.entity';
import Schedule from './schedule.entity';
import Notification from './notification.entity';

@Entity()
export default class ClassSubject {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @ManyToOne(type => Subject, (s: Subject) => s.classSubjects) subject: Subject;
    @Column('text') subjectName: string;

    @ManyToOne(type => Teacher, (t: Teacher) => t.classSubjects) teacher: Teacher;
    @Column('text') teacherName: string;

    @Column('date') startDate: string;

    @Column('date') endDate: string;

    @ManyToOne(type => Semester, (se: Semester) => se.classSubjects) semester: Semester;
    @Column('text') semesterName: string;

    @OneToMany(type => StudentClassSubject, (scs: StudentClassSubject) => scs.classSubject) studentClasses: StudentClassSubject[];
    @OneToMany(type => Schedule, (s: Schedule) => s.classSubject) schedules: Schedule[];
    @OneToMany(type => Notification, (s: Notification) => s.classSubject) notifications: Notification[];
}
