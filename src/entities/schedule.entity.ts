import { Entity, PrimaryGeneratedColumn, ManyToOne, Column, OneToMany } from 'typeorm';
import ClassSubject from './class-subject.entity';
import { Room } from './room.entity';
import Point from './point.entity';

@Entity()
export default class Schedule {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => ClassSubject, (cs: ClassSubject) => cs.schedules) classSubject: ClassSubject;
    @Column('text') classSubjectName: string;

    @ManyToOne(type => Room, (cs: Room) => cs.schedules) room: Room;
    @Column('text') roomName: string;

    @Column('date') date: Date;

    @Column('text') startTime: string;

    @Column('text') endTime: string;

    @OneToMany(type => Point, (p: Point) => p.schedule) points: Point[];
}
