import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import ClassSubject from './class-subject.entity';
import Notification from './notification.entity';
import { Student } from './student.entity';

@Entity()
export default class StudentClassSubject {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => ClassSubject, (cs: ClassSubject) => cs.studentClasses) classSubject: ClassSubject;
    @Column('text') classSubjectName: string;

    @ManyToOne(type => Student, (t: Student) => t.studentClasses) student: Student;
    @Column('text') studentName: string;
    @Column('text') studentCode: string;

    @Column('real') attendanceScore: number;

    @Column('real') totalDailyScore: number;

    @Column('real') midTermScore: number;

    @Column('real') totalScore: number;

    @OneToMany(type => Notification, (n: Notification) => n.studentClassSubject) notifications: Notification[];

}
