import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, BeforeInsert } from 'typeorm';
import * as bcrypt from 'bcryptjs';
import * as jwt from 'jsonwebtoken';
import { UserRO } from 'src/modules/user/user.dto';

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @Column('text') address: string;

    @Column('text') phonenumber: string;

    @Column('date') birthday: Date;

    @Column('text') code: string;

    @Column('text') password: string;

    @Column('text') avatar: string;

    @Column('text') typeUser: string;

    @BeforeInsert()
    async hashPassword() {
        this.password = await bcrypt.hash(this.password, 10);
    }

    toResponseObject(showToken: boolean = true) {
        const { id, code, name, address, phonenumber, birthday, typeUser, token, password, avatar } = this;
        const responseObj: UserRO = { id, code, name, address, phonenumber, birthday, typeUser, password, avatar };
        if (showToken) {
            responseObj.token = token;
        }
        return responseObj;
    }

    // toToken() {
    //     const {token} = this;
    //     const responseObj = {token};
    //     return token;
    // }

    async comparePassword(attemp: string) {
        return await bcrypt.compare(attemp, this.password);
    }

    private get token() {
        const { id, code } = this;
        return jwt.sign({
            id, code,
        },
        'Death',
        {expiresIn: '7d'});
    }
}
