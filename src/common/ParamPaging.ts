import { IsString, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IPaging {
    take: number;
    skip: number;
}

export class Paging implements IPaging {
    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    take: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    skip: number;
}
