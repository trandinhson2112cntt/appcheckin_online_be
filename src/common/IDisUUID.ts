import { HttpException, HttpStatus } from '@nestjs/common';

export class IDisUUID {
    static checking(id: string) {
        const var1 = id.split('-');
        if (var1.length !== 5) {
            throw new HttpException('Invalid UUID string: ' + id, HttpStatus.BAD_REQUEST);
        }
    }
}
