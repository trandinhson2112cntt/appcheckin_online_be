import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, OneToOne, JoinColumn } from 'typeorm';

import { User } from '../../user/entities/user.entity';
import Major from '../../major/entities/major.entity';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
@Entity()
export class Teacher {
    @PrimaryGeneratedColumn('uuid') id: string;
    @Column('text') level: string;
    @ManyToOne(type => Major, (m: Major) => m.teachers) major: Major;

    @Column('uuid') majorId: string;
    @Column('text') majorName: string;
    @Column('uuid') userId: string;
    @Column('text') userName: string;
    @OneToMany(type => ClassSubject, (c: ClassSubject) => c.teacher) classSubjects: ClassSubject[];

    @OneToOne(type => User)
    @JoinColumn()
    user: User;
}
