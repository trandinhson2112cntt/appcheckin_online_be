import { Module } from '@nestjs/common';
import { TeacherService } from './teacher.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeacherController } from './teacher.controller';
import { Teacher } from './entities/teacher.entity';

@Module({
  providers: [TeacherService],
  imports: [TypeOrmModule.forFeature([Teacher])],
  controllers: [TeacherController],
})
export class TeacherModule { }
