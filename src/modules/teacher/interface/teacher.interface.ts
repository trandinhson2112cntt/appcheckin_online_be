import { IsString, IsNotEmpty, IsUUID } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { User } from '../../user/entities/user.entity';
import { UserRO } from '../../user/user.dto';

export interface ITeacher {
    level: string;
    majorId: string;
    majorName: string;
    userId: string;
    userName: string;
    user?: User;
}

export class TeacherDTO implements ITeacher {

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    level: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    majorId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    majorName: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    userId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    userName: string;

    user?: User;
}
