import { Controller, Get, UseGuards, Post, Param, Body, UsePipes, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { TeacherService } from './teacher.service';
import { AuthGuard } from '@nestjs/passport';
import { Paging } from '../../common/ParamPaging';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { TeacherDTO } from './interface/teacher.interface';

@Controller('teacher')
@ApiUseTags('Teacher')
@ApiBearerAuth()
export class TeacherController {
    constructor(
        private teachertService: TeacherService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAllTeachert() {
        return this.teachertService.showAll();
    }

    @Get('by-major:id')
    @UseGuards(AuthGuard('jwt'))
    showByMajorId(@Param('id') id: string) {
        return this.teachertService.showByMajorId(id);
    }

    @Post('by-major-paging:id')
    @UseGuards(AuthGuard('jwt'))
    showByMajorIdPaging(@Param('id') id: string, @Body() paging: Paging) {
        return this.teachertService.showByMajorIdPaging(id, paging);
    }
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: TeacherDTO) {
        return this.teachertService.create(data);
    }
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: TeacherDTO) {
        return this.teachertService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.teachertService.destroy(id);
    }
}
