import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Teacher } from './entities/teacher.entity';
import { Repository } from 'typeorm';
import { IDisUUID } from '../../common/IDisUUID';
import { IPaging } from '../../common/ParamPaging';
import { ITeacher } from './interface/teacher.interface';

@Injectable()
export class TeacherService {
    constructor(
        @InjectRepository(Teacher)
        private teacherRepository: Repository<Teacher>,
    ) { }

    async showAll() {
        const teacher = await this.teacherRepository
            .createQueryBuilder('teacher')
            .leftJoinAndSelect('teacher.major', 'major')
            .leftJoinAndSelect('teacher.user', 'user')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: teacher,
        };
    }

    async showByMajorId(id: string) {
        IDisUUID.checking(id);
        const teacher = await this.teacherRepository
            .createQueryBuilder('teacher')
            .leftJoinAndSelect('teacher.major', 'major')
            .leftJoinAndSelect('teacher.user', 'user')
            .where('teacher.majorId = :id', { id })
            .getMany();
        if (teacher.length === 0 || !teacher) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: teacher,
        };
    }

    async showByMajorIdPaging(id: string, paging: IPaging) {
        IDisUUID.checking(id);
        const data = await this.teacherRepository
            .createQueryBuilder('teacher')
            .leftJoinAndSelect('teacher.major', 'major')
            .leftJoinAndSelect('teacher.user', 'user')
            .where('teacher.majorId = :id', { id })
            .skip(paging.skip)
            .take(paging.take)
            .getMany();
        if (data.length === 0 || !data) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            take: paging.take,
            skip: paging.skip,
            data,

        };
    }

    async create(data: ITeacher) {
        const findTeacher = await this.teacherRepository.findOne({ where: { userId: data.userId } });
        if (findTeacher) {
            throw new HttpException('Teacher existed ', HttpStatus.BAD_REQUEST);
        }
        const teacher = await this.teacherRepository.create(data);
        await this.teacherRepository.save(teacher);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: teacher,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findTeacher = await this.teacherRepository
            .createQueryBuilder('teacher')
            .leftJoinAndSelect('teacher.major', 'major')
            .leftJoinAndSelect('teacher.user', 'user')
            .where('teacher.id = :id', { id })
            .getOne();
        if (!findTeacher) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findTeacher,
        };
    }

    async update(id: string, data: Partial<ITeacher>) {
        IDisUUID.checking(id);
        let findTeacher = await this.teacherRepository.findOne({ where: { id } });
        if (!findTeacher) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.teacherRepository.update({ id }, data);
        findTeacher = await this.teacherRepository
            .createQueryBuilder('teacher')
            .leftJoinAndSelect('teacher.major', 'major')
            .leftJoinAndSelect('teacher.user', 'user')
            .where('teacher.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findTeacher,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findTeacher = await this.teacherRepository.findOne({ where: { id } });
        if (!findTeacher) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.teacherRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findTeacher,
        };
    }
}
