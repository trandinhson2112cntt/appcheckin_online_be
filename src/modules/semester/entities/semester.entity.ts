import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';

@Entity()
export class Semester {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @Column('text') year: string;

    @OneToMany(type => ClassSubject, (c: ClassSubject) => c.semester) classSubjects: ClassSubject[];
}
