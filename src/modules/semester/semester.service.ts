import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { IDisUUID } from '../../common/IDisUUID';
import { Repository } from 'typeorm';
import { ISemester } from './interface/semester.interface';
import { Semester } from './entities/semester.entity';
@Injectable()
export class SemesterService {
    constructor(
        @InjectRepository(Semester)
        private semesterRepository: Repository<Semester>,
    ) { }

    async showAll() {
        const semester = await this.semesterRepository
            .find();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: semester,
        };
    }

    async create(data: ISemester) {
        const findSemester = await this.semesterRepository.findOne({ where: { name: data.name, year: data.year } });
        if (findSemester) {
            throw new HttpException('semester existed ', HttpStatus.BAD_REQUEST);
        }
        const semester = await this.semesterRepository.create(data);
        await this.semesterRepository.save(semester);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: semester,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findSemester = await this.semesterRepository
            .createQueryBuilder('semester')
            .where('semester.id = :id', { id })
            .getOne();
        if (!findSemester) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSemester,
        };
    }

    async update(id: string, data: Partial<ISemester>) {
        IDisUUID.checking(id);
        let findSemester = await this.semesterRepository.findOne({ where: { id } });
        if (!findSemester) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.semesterRepository.update({ id }, data);
        findSemester = await this.semesterRepository
            .createQueryBuilder('semester')
            .where('semester.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSemester,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findSemester = await this.semesterRepository.findOne({ where: { id } });
        if (!findSemester) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.semesterRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSemester,
        };
    }
}
