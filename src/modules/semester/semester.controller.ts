import { Controller, Get, UseGuards, Post, UsePipes,  Body, Put, Param, Delete } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SemesterService } from './semester.service';
import { SemesterDTO } from './interface/semester.interface';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { ValidationPipe } from '../../pipes/validation.pipe';

@Controller('semester')
@ApiUseTags('Semester')
@ApiBearerAuth()
export class SemesterController {
    constructor(
        private semesterService: SemesterService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAll() {
        return this.semesterService.showAll();
    }
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: SemesterDTO) {
        return this.semesterService.create(data);
    }
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: SemesterDTO) {
        return this.semesterService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.semesterService.destroy(id);
    }
}
