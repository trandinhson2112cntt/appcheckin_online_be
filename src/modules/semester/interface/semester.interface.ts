import { IsString, IsNotEmpty, IsUUID, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export interface ISemester {
    name: string;
    year: string;
}

export class SemesterDTO implements ISemester {

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    name: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    year: string;
}
