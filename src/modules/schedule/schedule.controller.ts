import { Controller, Get, UseGuards, Post, UsePipes, Body, Put, Param, Delete } from '@nestjs/common';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { ScheduleService } from './schedule.service';
import { AuthGuard } from '@nestjs/passport';
import { ScheduleDTO } from './interface/schedule.interface';
import { IMonthOfWeek, MonthOfWeek } from './params/monthAndWeek.params';
import { StudentClassSubjectService } from '../student-class-subject/student-class-subject.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Schedule } from './entities/schedule.entity';
import { Repository } from 'typeorm';
import { StudentClassSubject } from '../student-class-subject/entities/student-class-subject.entity';
import { IArrString } from './params/arrString.params';
import { DateOfWeek } from './params/dateOfWeek.params';
@Controller('schedule')
@ApiUseTags('Schedule')
@ApiBearerAuth()
export class ScheduleController {
    constructor(
        private scheduleService: ScheduleService,
        private studentClassSubjectService: StudentClassSubjectService,
        @InjectRepository(Schedule)
        private readonly scheduleRepository: Repository<Schedule>,
        @InjectRepository(StudentClassSubject)
        private readonly studentClassSubjectRepository: Repository<StudentClassSubject>,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAll() {
        return this.scheduleService.showAll();
    }

    @Post('by-class-subject-student:id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showAllByClassSubject(@Param('id') id: string, @Body() params: DateOfWeek) {
        return this.scheduleService.showScheduleByClassSubject(id, params);
    }

    @Get('by-class-subject:id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showAllByClassSubjectNoParam(@Param('id') id: string) {
        return this.scheduleService.showScheduleByClassSubjectNoParams(id);
    }

    @Get('by-teacher:id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showAllByTeacher(@Param('id') id: string) {
        return this.scheduleService.showAllByTeacherID(id);
    }

    @Post('by-teacher-month-week:id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showAllByTeacherByMonthAndWeek(@Param('id') id: string, @Body() params: MonthOfWeek) {
        return this.scheduleService.showAllByTeacherByMonthAndWeek(id, params);
    }

    @Get('get-recent-schedule:id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showScheduleRecent(@Param('id') id: string) {
        return this.scheduleService.showScheduleRecent(id);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: ScheduleDTO) {
        return this.scheduleService.create(data);
    }
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: ScheduleDTO) {
        return this.scheduleService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.scheduleService.destroy(id);
    }
}
