import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { Schedule } from './entities/schedule.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { ISchedule, ScheduleDTO } from './interface/schedule.interface';
import { IDisUUID } from '../../common/IDisUUID';
import { Teacher } from '../teacher/entities/teacher.entity';
import { StudentClassSubjectService } from '../student-class-subject/student-class-subject.service';
import { ClassSubject } from '../class-subject/entities/class-subject.entity';
import * as moment from 'moment';
import { IMonthOfWeek } from './params/monthAndWeek.params';
import { StudentClassSubject } from '../student-class-subject/entities/student-class-subject.entity';
import { async } from 'rxjs/internal/scheduler/async';
import { IArrString } from './params/arrString.params';
import { IDateOfWeek } from './params/dateOfWeek.params';
import { log } from 'util';
@Injectable()
export class ScheduleService {
    constructor(
        @InjectRepository(Schedule)
        private readonly scheduleRepository: Repository<Schedule>,
        @InjectRepository(StudentClassSubject)
        private readonly studentClassSubjectRepository: Repository<StudentClassSubject>,
    ) { }

    async showAll() {
        const schedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.classSubject', 'classSubject')
            .leftJoinAndSelect('schedule.room', 'room')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: schedule,
        };
    }

    async showAllByTeacherID(id: string) {
        IDisUUID.checking(id);

        const schedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.classSubject', 'classSubject', 'classSubject.teacherId = :id', { id })
            .leftJoinAndSelect('schedule.room', 'room')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: schedule,
        };
    }

    async showAllByTeacherByMonthAndWeek(id: string, params: IMonthOfWeek) {
        IDisUUID.checking(id);

        const schedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.classSubject', 'classSubject', 'classSubject.teacherId = :id', { id })
            .leftJoinAndSelect('schedule.room', 'room')
            .getMany();

        const data = [];
        await schedule.map(
            (item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                // tslint:disable-next-line:radix
                const valueMonth = date.month();
                // tslint:disable-next-line:radix
                const w = date.week();
                if (valueMonth === params.month && w === params.week) {
                    data.push(item);
                }
            },
        );

        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
        };
    }

    async showScheduleByClassSubjectNoParams(id: string) {
        IDisUUID.checking(id);
        const schedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .where('schedule.classSubjectId = :id', { id })
            .leftJoinAndSelect('schedule.classSubject', 'classSubject')
            .leftJoinAndSelect('schedule.room', 'room')
            .getMany();

        return {
            status: HttpStatus.OK,
            message: 'Success',
            schedule,
        };
    }

    async showScheduleByClassSubject(id: string, params: IDateOfWeek) {
        IDisUUID.checking(id);
        const schedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .where('schedule.classSubjectId = :id', { id })
            .leftJoinAndSelect('schedule.classSubject', 'classSubject')
            .leftJoinAndSelect('schedule.room', 'room')
            .getMany();

        const data = [];
        await schedule.map(
            (item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                const numberDay = date.toDate().valueOf();
                const numberDayStart = moment(params.dateStart, 'DD/MM/YYYY').toDate().valueOf();
                const numberDayEnd = moment(params.dateEnd, 'DD/MM/YYYY').toDate().valueOf();
                if (numberDay >= numberDayStart && numberDay <= numberDayEnd) {
                    data.push(item);
                }
            },
        );

        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
        };
    }

    async showScheduleRecent(id: string) {
        IDisUUID.checking(id);

        const schedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.classSubject', 'classSubject', 'classSubject.teacherId = :id', { id })
            .leftJoinAndSelect('schedule.room', 'room')
            .getMany();

        const data = [];
        await schedule.map(
            (item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                // tslint:disable-next-line:radix
                const valueMonth = date.month();
                // tslint:disable-next-line:radix
                const valueDay = date.date();

                const currentDay = moment('01/01/2019');
                const valueMonthCur = currentDay.month();
                const valueDayCur = currentDay.date();
                if (valueMonth === valueMonthCur && valueDay >= valueDayCur) {
                    data.push(valueDay);
                }
            },
        );
        const result = [];
        const minValue = await Math.min(...data);
        await schedule.map(
            (item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                // tslint:disable-next-line:radix
                const valueMonth = date.month();
                // tslint:disable-next-line:radix
                const valueDay = date.date();

                const currentDay = moment('01/01/2019');
                const valueMonthCur = currentDay.month();
                const valueDayCur = currentDay.date();
                if (valueMonth === valueMonthCur && valueDay === minValue) {
                    result.push(item);
                }
            },
        );
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: result,
        };
    }

    async create(data: ISchedule) {
        const findSchedule = await this.scheduleRepository
            .findOne({ where: { classSubjectId: data.classSubjectId, startTime: data.startTime, roomId: data.roomId, date: data.date } });
        if (findSchedule) {
            throw new HttpException('Class Subject existed ', HttpStatus.BAD_REQUEST);
        }
        const schedule = await this.scheduleRepository.create(data);
        await this.scheduleRepository.save(schedule);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: schedule,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findSchedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.classSubject', 'classSubject')
            .leftJoinAndSelect('schedule.room', 'room')
            .where('schedule.id = :id', { id })
            .getOne();
        if (!findSchedule) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSchedule,
        };
    }

    async update(id: string, data: Partial<ISchedule>) {
        IDisUUID.checking(id);
        let findSchedule = await this.scheduleRepository.findOne({ where: { id } });
        if (!findSchedule) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.scheduleRepository.update({ id }, data);
        findSchedule = await this.scheduleRepository
            .createQueryBuilder('schedule')
            .leftJoinAndSelect('schedule.classSubject', 'classSubject')
            .leftJoinAndSelect('schedule.room', 'room')
            .where('schedule.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSchedule,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findSchedule = await this.scheduleRepository.findOne({ where: { id } });
        if (!findSchedule) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.scheduleRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSchedule,
        };
    }
}
