import { IsString, IsNotEmpty, IsNumber, IsArray } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IArrString {
    value: string[];
}

export class IArrString implements IArrString {
    @IsArray()
    @ApiModelPropertyOptional()
    value: string[];
}
