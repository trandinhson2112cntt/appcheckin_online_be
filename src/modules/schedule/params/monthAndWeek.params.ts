import { IsString, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IMonthOfWeek {
    month: number;
    week: number;
}

export class MonthOfWeek implements IMonthOfWeek {
    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    month: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    week: number;
}
