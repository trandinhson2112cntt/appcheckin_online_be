import { IsString, IsNotEmpty, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IDateOfWeek {
    dateStart: string;
    dateEnd: string;
}

export class DateOfWeek implements IDateOfWeek {
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    dateStart: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    dateEnd: string;
}
