import { Entity, PrimaryGeneratedColumn, ManyToOne, Column, OneToMany } from 'typeorm';
import { Room } from '../../room/entities/room.entity';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
import { Absent } from '../../absent/entities/absent.entity';
import { Point } from '../../point/entities/point.entity';

@Entity()
export class Schedule {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => ClassSubject, (cs: ClassSubject) => cs.schedules) classSubject: ClassSubject;
    @Column('text') classSubjectId: string;
    @Column('text') classSubjectName: string;

    @ManyToOne(type => Room, (cs: Room) => cs.schedules) room: Room;
    @Column('text') roomId: string;

    @Column('text') date: string;

    @Column('text') startTime: string;

    @Column('text') endTime: string;

    @Column({ type: 'real', default: 0 }) dailyScore: number;
    // @OneToMany(type => Point, (p: Point) => p.schedule) points: Point[];
    @OneToMany(type => Absent, (abs: Absent) => abs.schedule) absents: Absent[];
    @OneToMany(type => Point, (abs: Point) => abs.schedule) points: Point[];
}
