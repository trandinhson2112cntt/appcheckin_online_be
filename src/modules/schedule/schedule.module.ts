import { Module } from '@nestjs/common';
import { ScheduleService } from './schedule.service';
import { ScheduleController } from './schedule.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Schedule } from './entities/schedule.entity';
import { StudentClassSubjectService } from '../student-class-subject/student-class-subject.service';
import { StudentClassSubjectModule } from '../student-class-subject/student-class-subject.module';
import { StudentClassSubject } from '../student-class-subject/entities/student-class-subject.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Schedule, StudentClassSubject]), StudentClassSubjectModule],
  providers: [ScheduleService],
  controllers: [ScheduleController],
})
export class ScheduleModule {}
