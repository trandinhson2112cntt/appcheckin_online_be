import { IsString, IsNotEmpty, IsUUID, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export interface ISchedule {
    classSubjectId: string;
    classSubjectName: string;
    roomId: string;
    date: string;
    startTime: string;
    endTime: string;
    dailyScore: number;
}

export class ScheduleDTO implements ISchedule {

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    classSubjectId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    classSubjectName: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    roomId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    date: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    startTime: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    endTime: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    dailyScore: number;
}
