import { IsString, IsNotEmpty, IsUUID, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export interface IRoom {
    roomName: string;
    floorName: string;
    buildingName: string;
}

export class RoomDTO implements IRoom {

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    roomName: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    floorName: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    buildingName: string;
}
