import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Schedule } from '../../schedule/entities/schedule.entity';

@Entity()
export class Room {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') roomName: string;
    @Column('text') floorName: string;
    @Column('text') buildingName: string;

    @OneToMany(type => Schedule, (s: Schedule) => s.classSubject) schedules: Schedule[];
}
