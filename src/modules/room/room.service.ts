import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { Room } from './entities/room.entity';
import { IRoom } from './interface/room.interface';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IDisUUID } from '../../common/IDisUUID';

@Injectable()
export class RoomService {
    constructor(
        @InjectRepository(Room)
        private roomRepository: Repository<Room>,
    ) { }

    async showAll() {
        const room = await this.roomRepository
            .find();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: room,
        };
    }

    async create(data: IRoom) {
        const findRoom = await this.roomRepository
            .findOne({ where: { roomName: data.roomName, buildingName: data.buildingName, floorName: data.floorName } });
        if (findRoom) {
            throw new HttpException('room existed ', HttpStatus.BAD_REQUEST);
        }
        const room = await this.roomRepository.create(data);
        await this.roomRepository.save(room);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: room,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findRoom = await this.roomRepository
            .createQueryBuilder('room')
            .where('room.id = :id', { id })
            .getOne();
        if (!findRoom) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findRoom,
        };
    }

    async update(id: string, data: Partial<IRoom>) {
        IDisUUID.checking(id);
        let findRoom = await this.roomRepository.findOne({ where: { id } });
        if (!findRoom) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.roomRepository.update({ id }, data);
        findRoom = await this.roomRepository
            .createQueryBuilder('room')
            .where('room.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findRoom,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findRoom = await this.roomRepository.findOne({ where: { id } });
        if (!findRoom) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.roomRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findRoom,
        };
    }
}
