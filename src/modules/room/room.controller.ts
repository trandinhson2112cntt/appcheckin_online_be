import { Controller, Get, UseGuards, Post, UsePipes,  Body, Put, Param, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { RoomService } from './room.service';
import { AuthGuard } from '@nestjs/passport';
import { RoomDTO } from './interface/room.interface';
import { ValidationPipe } from '../../pipes/validation.pipe';

@Controller('room')
@ApiUseTags('Room')
@ApiBearerAuth()
export class RoomController {
    constructor(
        private roomService: RoomService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAll() {
        return this.roomService.showAll();
    }
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: RoomDTO) {
        return this.roomService.create(data);
    }
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: RoomDTO) {
        return this.roomService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.roomService.destroy(id);
    }
}
