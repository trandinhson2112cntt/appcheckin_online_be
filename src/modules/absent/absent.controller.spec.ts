import { Test, TestingModule } from '@nestjs/testing';
import { AbsentController } from './absent.controller';

describe('Absent Controller', () => {
  let controller: AbsentController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AbsentController],
    }).compile();

    controller = module.get<AbsentController>(AbsentController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
