import { IsString, IsNotEmpty, IsUUID } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IAbsent {
    scheduleId: string;
    scheduleDate: string;
    studentClassSubjectId: string;
    studentClassSubjectName: string;
}

export class AbsentDTO implements IAbsent {  // DTO: Data transfer object
    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    scheduleId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    scheduleDate: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    studentClassSubjectId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    studentClassSubjectName: string;
}
