import { Module } from '@nestjs/common';
import { AbsentController } from './absent.controller';
import { AbsentService } from './absent.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Absent } from './entities/absent.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Absent])],
  controllers: [AbsentController],
  providers: [AbsentService],
})
export class AbsentModule {}
