import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Absent } from './entities/absent.entity';
import { Repository } from 'typeorm';
import { IAbsent, AbsentDTO } from './interface/absent.interface';
import { IDisUUID } from '../../common/IDisUUID';

@Injectable()
export class AbsentService {
    constructor(
        @InjectRepository(Absent)
        private absentRepository: Repository<Absent>,
    ) { }

    async showAll() {
        const absent = await this.absentRepository
            .createQueryBuilder('absent')
            .leftJoinAndSelect('absent.schedule', 'schedule')
            .leftJoinAndSelect('absent.studentClassSubject', 'studentClassSubject')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Succsess',
            data: absent,
        };
    }

    async create(data: IAbsent) {
        // tslint:disable-next-line:max-line-length
        const findAbsent = await this.absentRepository.findOne({ where: { scheduleId: data.scheduleId, scheduleDate: data.scheduleDate, studentClassSubjectId: data.studentClassSubjectId, studentClassSubjectName: data.studentClassSubjectName } });
        if (findAbsent) {
            throw new HttpException('Student absent existed ', HttpStatus.BAD_REQUEST);
        }
        const absent = await this.absentRepository.create(data);
        await this.absentRepository.save(absent);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: absent,
        };
    }

    async update(id: string, data: Partial<AbsentDTO>) {
        IDisUUID.checking(id);
        let findAbsent = await this.absentRepository.findOne({ where: { id } });
        if (!findAbsent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.absentRepository.update({ id }, data);
        findAbsent = await this.absentRepository
            .createQueryBuilder('absent')
            .leftJoinAndSelect('absent.schedule', 'schedule')
            .leftJoinAndSelect('absent.studentClassSubject', 'studentClassSubject')
            .where('absent.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findAbsent,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findAbsent = await this.absentRepository.findOne({ where: { id } });
        if (!findAbsent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.absentRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Succsess',
            data: findAbsent,
        };
    }

    // Vắng mặt theo Schedule ID
    async getListStudentAbsentByScheduleId(id: string) {
        IDisUUID.checking(id);
        const absent = await this.absentRepository
            .createQueryBuilder('absent')
            .where('absent.scheduleId = :id', { id })
            .leftJoinAndSelect('absent.schedule', 'schedule', 'schedule.Id = :id', { id }) // Param: { id: id(là id truyền vào) }
            .leftJoinAndSelect('absent.studentClassSubject', 'studentClassSubject')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: absent,
        };
    }

    async destroyByScheduleIdAndStudentClassSubjectId(data: any) {
        // tslint:disable-next-line:max-line-length
        const findAbsent = await this.absentRepository.findOne({ where: { scheduleId: data.scheduleId, studentClassSubjectId: data.studentClassSubjectId } });
        if (!findAbsent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.absentRepository.delete({ id: findAbsent.id });
        return {
            status: HttpStatus.OK,
            message: 'Succsess',
            data: findAbsent,
        };
    }
}
