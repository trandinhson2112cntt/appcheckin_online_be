import { Controller, Post, UseGuards, UsePipes, Body, Put, Param, Delete, Get } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AbsentService } from './absent.service';
import { AuthGuard } from '@nestjs/passport';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { AbsentDTO } from './interface/absent.interface'; // DTO: Data transfer object

@Controller('absent') // Name Controller
@ApiUseTags('Absent') // Postman
@ApiBearerAuth() // Use token bearer
export class AbsentController {
    constructor(
        private absentService: AbsentService,
    ) {}

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAll() {
        return this.absentService.showAll();
    }

    // Read the list of students by class
    @Post() // truyen duoc ca id va body
    @UseGuards(AuthGuard('jwt')) // Be used token
    @UsePipes(new ValidationPipe()) // validate gia tri truyen vao
    create(@Body() data: AbsentDTO) {
        return this.absentService.create(data);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: AbsentDTO) {
        return this.absentService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.absentService.destroy(id);
    }

    @Get('List-Student-Absent-By-ScheduleId:id')
    @UseGuards(AuthGuard('jwt'))
    getListStudentAbsentByScheduleId(@Param('id') id: string) {
        return this.absentService.getListStudentAbsentByScheduleId(id);
    }

    @Post('Schedule-Id-And-Student-Class-Subject-Id')
    @UseGuards(AuthGuard('jwt'))
    destroyByScheduleIdAndStudentClassSubjectId(@Body() data: any) {
        return this.absentService.destroyByScheduleIdAndStudentClassSubjectId(data);
    }
}
