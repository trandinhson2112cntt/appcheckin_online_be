import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Schedule } from '../../schedule/entities/schedule.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';

@Entity()
export class Absent {
    @PrimaryGeneratedColumn('uuid') id: string; // Khoa chinh

    @ManyToOne(type => Schedule, (m: Schedule) => m.absents) schedule: Schedule; // Noi bang
    @Column('uuid') scheduleId: string; // Khai bao khoa ngoai
    @Column('text') scheduleDate: string;

    @ManyToOne(type => StudentClassSubject, (m: StudentClassSubject) => m.absents) studentClassSubject: StudentClassSubject;
    @Column('uuid') studentClassSubjectId: string;
    @Column('text') studentClassSubjectName: string;

}
