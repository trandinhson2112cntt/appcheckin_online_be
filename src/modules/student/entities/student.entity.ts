import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToMany, OneToOne } from 'typeorm';
import { User } from '../../user/entities/user.entity';
import { Class } from '../../class/entities/class.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';

@Entity()
export class Student {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') education: string;

    @ManyToOne(type => Class, (c: Class) => c.students) class: Class;
    @Column('uuid') classId: string;
    @Column('text') className: string;

    @OneToMany(type => StudentClassSubject, (scs: StudentClassSubject) => scs.student) studentClasses: StudentClassSubject[];
    // @OneToMany(type => Point, (p: Point) => p.student) points: Point[];

    @Column('uuid') userId: string;
    @Column('text') userName: string;
    @ManyToOne(type => User)
    user?: User;
}
