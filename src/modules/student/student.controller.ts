import { Controller, Get, UseGuards, Post, UsePipes, Body, Param, Put, Delete } from '@nestjs/common';
import { StudentService } from './student.service';
import { AuthGuard } from '@nestjs/passport';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { StudentDTO } from './interface/student.interface';
import { Paging } from '../../common/ParamPaging';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@Controller('student')
@ApiUseTags('Student')
@ApiBearerAuth()
export class StudentController {
    constructor(
        private studentService: StudentService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAllStudent() {
        return this.studentService.showAll();
    }

    @Get('by-class:id')
    @UseGuards(AuthGuard('jwt'))
    showByClassId(@Param('id') id: string) {
        return this.studentService.showByClassId(id);
    }

    @Get('by-user:id')
    @UseGuards(AuthGuard('jwt'))
    showByUserId(@Param('id') id: string) {
        return this.studentService.showByUserId(id);
    }

    @Post('by-class-paging:id')
    @UseGuards(AuthGuard('jwt'))
    showByClassIdPaging(@Param('id') id: string, @Body() paging: Paging) {
        return this.studentService.showByClassIdPaging(id, paging);
    }
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: StudentDTO) {
        return this.studentService.create(data);
    }
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: StudentDTO) {
        return this.studentService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.studentService.destroy(id);
    }

}
