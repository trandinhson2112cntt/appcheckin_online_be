import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Student } from './entities/student.entity';
import { Repository } from 'typeorm';
import { IStudent } from './interface/student.interface';
import { IDisUUID } from '../../common/IDisUUID';
import { IPaging } from '../../common/ParamPaging';

@Injectable()
export class StudentService {
    constructor(
        @InjectRepository(Student)
        private studentRepository: Repository<Student>,
    ) { }

    async showAll() {
        const student = await this.studentRepository
            .createQueryBuilder('student')
            .leftJoinAndSelect('student.class', 'class')
            .leftJoinAndSelect('student.user', 'user')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: student,
        };
    }

    async showByClassId(id: string) {
        IDisUUID.checking(id);
        const data = await this.studentRepository
            .createQueryBuilder('student')
            .leftJoinAndSelect('student.class', 'class')
            .leftJoinAndSelect('student.user', 'user')
            .where('student.classId = :id', { id })
            .getMany();
        if (!data) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
        };
    }

    async showByUserId(id: string) {
        IDisUUID.checking(id);
        const data = await this.studentRepository
            .createQueryBuilder('student')
            .leftJoinAndSelect('student.class', 'class')
            .where('student.user.id = :id', { id })
            .getMany();
        if (!data) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
        };
    }

    async showByClassIdPaging(id: string, paging: IPaging) {
        IDisUUID.checking(id);
        const data = await this.studentRepository
            .createQueryBuilder('student')
            .leftJoinAndSelect('student.class', 'class')
            .leftJoinAndSelect('student.user', 'user')
            .where('student.classId = :id', { id })
            .skip(paging.skip)
            .take(paging.take)
            .getMany();
        if (!data) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            take: paging.take,
            skip: paging.skip,
            data,

        };
    }

    async create(data: IStudent) {
        const findStudent = await this.studentRepository.findOne({ where: { userId: data.userId } });
        if (findStudent) {
            throw new HttpException('Student existed ', HttpStatus.BAD_REQUEST);
        }
        const student = await this.studentRepository.create(data);
        await this.studentRepository.save(student);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: student,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findStudent = await this.studentRepository
            .createQueryBuilder('student')
            .leftJoinAndSelect('student.class', 'class')
            .leftJoinAndSelect('student.user', 'user')
            .where('student.id = :id', { id })
            .getOne();
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }

    async update(id: string, data: Partial<IStudent>) {
        IDisUUID.checking(id);
        let findStudent = await this.studentRepository.findOne({ where: { id } });
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.studentRepository.update({ id }, data);
        findStudent = await this.studentRepository
            .createQueryBuilder('student')
            .leftJoinAndSelect('student.class', 'class')
            .leftJoinAndSelect('student.user', 'user')
            .where('student.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findStudent = await this.studentRepository.findOne({ where: { id } });
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.studentRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }
}
