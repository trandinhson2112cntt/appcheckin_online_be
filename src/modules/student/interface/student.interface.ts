import { IsString, IsNotEmpty, IsUUID } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';
import { User } from '../../user/entities/user.entity';
import { UserRO } from '../../user/user.dto';

export interface IStudent {
    education: string;
    classId: string;
    className: string;
    userId: string;
    userName: string;
    user?: User;
}

export class StudentDTO implements IStudent {
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    education: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    classId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    className: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    userId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    userName: string;

    user?: User;
}
