import { IsString, IsNotEmpty, IsUUID, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export interface ISubject {
    name: string;
    numberOfCredit: number;
    scorePass: number;
    nameGroup: string;
    type: string;
}

export class SubjectDTO implements ISubject {

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    name: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    numberOfCredit: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    scorePass: number;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    nameGroup: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    type: string;
}
