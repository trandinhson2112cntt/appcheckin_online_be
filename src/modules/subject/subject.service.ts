import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Subject } from './entities/subject.entity';
import { Repository } from 'typeorm';
import { ISubject } from './interface/subject.interface';
import { IDisUUID } from '../../common/IDisUUID';

@Injectable()
export class SubjectService {
    constructor(
        @InjectRepository(Subject)
        private subjectRepository: Repository<Subject>,
    ) { }

    async showAll() {
        const subject = await this.subjectRepository
            .find();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: subject,
        };
    }

    async create(data: ISubject) {
        const findSubject = await this.subjectRepository.findOne({ where: { name: data.name, type: data.type } });
        if (findSubject) {
            throw new HttpException('Subject existed ', HttpStatus.BAD_REQUEST);
        }
        const subject = await this.subjectRepository.create(data);
        await this.subjectRepository.save(subject);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: subject,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findSubject = await this.subjectRepository
            .createQueryBuilder('subject')
            .where('subject.id = :id', { id })
            .getOne();
        if (!findSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSubject,
        };
    }

    async update(id: string, data: Partial<ISubject>) {
        IDisUUID.checking(id);
        let findSubject = await this.subjectRepository.findOne({ where: { id } });
        if (!findSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.subjectRepository.update({ id }, data);
        findSubject = await this.subjectRepository
            .createQueryBuilder('subject')
            .where('subject.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSubject,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findSubject = await this.subjectRepository.findOne({ where: { id } });
        if (!findSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.subjectRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findSubject,
        };
    }
}
