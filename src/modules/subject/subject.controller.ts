import { Controller, Get, UseGuards, Post, UsePipes, Body, Put, Param, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { SubjectDTO } from './interface/subject.interface';
import { SubjectService } from './subject.service';

@Controller('subject')
@ApiUseTags('Subject')
@ApiBearerAuth()
export class SubjectController {
    constructor(
        private subjectService: SubjectService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAll() {
        return this.subjectService.showAll();
    }
    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    read(@Param('id') id: string) {
        return this.subjectService.read(id);
    }
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: SubjectDTO) {
        return this.subjectService.create(data);
    }
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: SubjectDTO) {
        return this.subjectService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.subjectService.destroy(id);
    }
}
