import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Class } from './entities/class.entity';
import { Repository } from 'typeorm';
import { IPaging } from '../../common/ParamPaging';
import { IClass } from './interface/class.interface';
import { ClassParams } from './interface/class.params';
import { IDisUUID } from '../../common/IDisUUID';

@Injectable()
export class ClassService {
    constructor(
        @InjectRepository(Class)
        private classRepository: Repository<Class>,
    ) { }

    async showAll() {
        const data = await this.classRepository.find();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
        };
    }

    async showByMajorId(id: string) {
        IDisUUID.checking(id);
        const data = await this.classRepository.find({ where: { majorId: id } });
        if (data.length === 0) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
        };
    }

    async showClassPaging(paging: IPaging) {
        const data = await this.classRepository.find({ take: paging.take, skip: paging.skip });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data,
        };
    }

    async create(data: IClass) {
        const findClass = await this.classRepository.findOne({ where: { name: data.name } });
        if (findClass) {
            throw new HttpException('Class name existed ', HttpStatus.BAD_REQUEST);
        }
        const classRes = await this.classRepository.create(data);
        await this.classRepository.save(classRes);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: classRes,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findClass = await this.classRepository.findOne({ where: { id } });
        if (!findClass) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClass,
        };
    }

    async update(id: string, data: Partial<IClass>) {
        IDisUUID.checking(id);
        let findClass = await this.classRepository.findOne({ where: { id } });
        if (!findClass) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.classRepository.update({ id }, data);
        findClass = await this.classRepository.findOne({ where: { id } });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClass,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findClass = await this.classRepository.findOne({ where: { id } });
        if (!findClass) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.classRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClass,
        };
    }

}
