import { Controller, Param, Get, UseGuards, Post, UsePipes, ValidationPipe, Body, Put, Delete } from '@nestjs/common';
import { ClassService } from './class.service';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ClassDTO } from './interface/class.interface';
import { ClassParams } from './interface/class.params';
import { validate } from 'class-validator';
import { Paging } from '../../common/ParamPaging';

@Controller('class')
@ApiUseTags('Class')
@ApiBearerAuth()
export class ClassController {
    constructor(
        private classService: ClassService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAllClass() {
        return this.classService.showAll();
    }

    @Get('by-majorid:id')
    @UseGuards(AuthGuard('jwt'))
    showAllClassByMajorId(@Param('id') id: string) {
        return this.classService.showByMajorId(id);
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    read(@Param('id') id: string) {
        return this.classService.read(id);
    }

    @Post('class-paging')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showClassPaging(@Body() paging: Paging) {
        return this.classService.showClassPaging(paging);
    }

    @Post('create')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    createClass(@Body() data: ClassDTO) {
        return this.classService.create(data);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    updateClass(@Param('id') id: string, @Body() data: ClassDTO) {
        return this.classService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroyClass(@Param('id') id: string) {
        return this.classService.destroy(id);
    }
}
