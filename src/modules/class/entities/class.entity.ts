import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import Major from '../../major/entities/major.entity';
import { Student } from '../../student/entities/student.entity';

@Entity()
export class Class {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @ManyToOne(type => Major, (m: Major) => m.classes) major: Major;
    @Column('uuid') majorId: string;
    @Column('text') majorName: string;

    @OneToMany(type => Student, (s: Student) => s.class) students: Student[];
}
