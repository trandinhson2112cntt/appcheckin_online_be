import { IsString, IsNotEmpty, IsUUID } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IClass {
    name: string;
    majorId: string;
    majorName: string;
}

export class ClassDTO implements IClass {
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    name: string;
    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    majorId: string;
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    majorName: string;
}
