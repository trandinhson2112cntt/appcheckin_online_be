import { IsUUID } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export class ClassParams {
    @IsUUID()
    @ApiModelPropertyOptional()
    id: string;
}
