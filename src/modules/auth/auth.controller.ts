import { Controller, Get, UseGuards, Post, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { UserDTO } from '../user/user.dto';
import { ApiUseTags } from '@nestjs/swagger';
import { INotiFirebase } from './interfaces/notify.interface';

@Controller('auth')
@ApiUseTags('Auth')
export class AuthController {
  constructor(private readonly authService: AuthService) { }

  @Post('/login')
  login(@Body() data: UserDTO): Promise<any> {
    return this.authService.login(data);
  }

  @Post('/send-notification')
  sendNotificationa(@Body('') data: INotiFirebase) {
    return this.authService.sendNotification(data);
  }
}
