import { Injectable, Inject, forwardRef, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserService } from '../user/user.service';
import * as Admin from 'firebase-admin';
import { INotiFirebase } from './interfaces/notify.interface';
import { log } from 'util';
// tslint:disable-next-line:no-var-requires
const serviceAccount = require('../../../src/modules/auth/appmuster-c5b80-firebase-adminsdk-7tkh8-b9bcf23076.json');
Admin.initializeApp({
  credential: Admin.credential.cert(serviceAccount),
  databaseURL: 'https://appmuster-c5b80.firebaseio.com',
});
@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly userService: UserService,
  ) { }

  async createToken(payload: JwtPayload): Promise<any> {
    const accessToken = this.jwtService.sign(payload);
    return {
      expiresIn: process.env.EXPIRES_IN,
      accessToken,
    };
  }

  async login(payload: JwtPayload): Promise<any> {
    // put some validation logic here
    // for example query user by id/email/username
    const token = await this.createToken(payload);
    const user = await this.userService.login(payload);
    return {
      status: HttpStatus.OK,
      message: 'Success',
      data: {
        token,
        user,
      },
    };
  }

  async validateUser(payload: JwtPayload): Promise<any> {
    // put some validation logic here
    // for example query user by id/email/username
    const user = await this.userService.login(payload);
    return {
      user,
    };
  }

  async sendNotification(data: INotiFirebase) {
    const registrationTokens: string[] = data.listToken;
    // tslint:disable-next-line:max-line-length
    const token = 'cR4uSXJ-Xqk:APA91bEBoUrfTKUqw4KIv1GP2dNysi_1Wj9l9xxMbAqGPbWWtsKvt2LCLVHDaaq2q58JuU1yuFRc7NFfAnUE41PwTzxF0BxicsOpUKsPWbfD0Aq_FyzVGg-oX-srdbV60LZ-LcOGm70Q';
    registrationTokens.push(token);
    const message = {
      notification: {
        title: data.title,
        body: data.message,
      },
    };
    const options = {
    };

    return await Admin.messaging().sendToDevice(registrationTokens, message, options);
  }
}
