import { Controller, Get, Post, Body, UseGuards } from '@nestjs/common';

import { UserService } from './user.service';
import { UserDTO, UserRO } from './user.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
@Controller('user')
@ApiUseTags('User')
export class UserController {
    constructor(private userService: UserService) { }
    @Get()
    @UseGuards(AuthGuard('jwt'))
    @ApiBearerAuth()
    showAllUser(): Promise<UserRO[]> {
        return this.userService.showAll();
    }

    @Post('/register')
    register(@Body() data: UserRO): Promise<UserRO> {
        return this.userService.register(data);
    }
}
