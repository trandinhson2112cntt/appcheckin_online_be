import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

// import moduleName from 'class-va'
export class UserDTO {
    // @IsNotEmpty()
    @ApiModelProperty({ required: true })
    code: string;
    @ApiModelProperty({ required: true, minLength: 6, type: String, format: 'password' })
    password: string;
}

// tslint:disable-next-line:max-classes-per-file
export class UserRO {
    id: string;
    @ApiModelPropertyOptional()
    code: string;
    @ApiModelPropertyOptional()
    password: string;
    @ApiModelPropertyOptional()
    name: string;
    @ApiModelPropertyOptional()
    address: string;
    @ApiModelPropertyOptional()
    phonenumber: string;
    @ApiModelPropertyOptional({type: String})
    birthday: Date;
    @ApiModelPropertyOptional()
    typeUser: string;
    @ApiModelPropertyOptional()
    avatar: string;
    token?: string;
}
