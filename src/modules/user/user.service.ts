import { Injectable, HttpException, HttpCode, HttpStatus, Inject, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { UserDTO, UserRO } from './user.dto';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private userRepository: Repository<User>,
    ) { }

    async showAll() {
        const users = await this.userRepository.find();
        return users.map(user => user.toResponseObject(false));
    }
    async login(data: UserDTO) {
        const { code, password } = data;
        const user = await this.userRepository.findOne({ where: { code } });
        if (!user || !(await user.comparePassword(password))) {
            throw new HttpException('Invalid code/password', HttpStatus.BAD_REQUEST);
        }
        return user.toResponseObject();
    }
    async register(data: UserRO) {
        const { code } = data;
        let user = await this.userRepository.findOne({ where: { code } });
        if (user) {
            throw new HttpException('User already exists', HttpStatus.BAD_REQUEST);
        }
        user = await this.userRepository.create(data);
        await this.userRepository.save(user);
        return user.toResponseObject();
    }
}
