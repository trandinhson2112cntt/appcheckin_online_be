import { IsString, IsNotEmpty, IsUUID, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IPoint {
    scheduleId: string;
    scheduleDate: string;
    studentClassSubjectId: string;
    studentClassSubjectName: string;
    plusScore: number;
    totalDailyScore: number;
}

export class PointDTO implements IPoint {  // DTO: Data transfer object
    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    scheduleId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    scheduleDate: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    studentClassSubjectId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    studentClassSubjectName: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    plusScore: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    totalDailyScore: number;
}
