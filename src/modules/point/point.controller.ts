import { Controller, Post, UseGuards, UsePipes, Body, Get, Param, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { PointService } from './point.service';
import { AuthGuard } from '@nestjs/passport';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { create } from 'domain';
import { PointDTO } from './interface/point.interface';
import { validate } from 'class-validator';

@Controller('point') // Name Controller
@ApiUseTags('Point') // Postman
@ApiBearerAuth() // Use token bearer
export class PointController {
    constructor(
        private pointService: PointService,
    ) {}

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAll() {
        return this.pointService.showAll();
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: PointDTO) {
        return this.pointService.create(data);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: PointDTO) {
        return this.pointService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.pointService.destroy(id);
    }

    @Get('/showAllByScheduleId/:id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showAllByScheduleId(@Param('id') id: string) {
        return this.pointService.showAllByScheduleId(id);
    }

    @Post('/createAndUpdate')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    createAndUpdate(@Body() data: PointDTO) {
        return this.pointService.createAndUpdate(data);
    }

    @Post('/showByStudent')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showByStudent(@Body() data: PointDTO) {
        return this.pointService.showPointByScheduleIdAndStudentId(data);
    }
}
