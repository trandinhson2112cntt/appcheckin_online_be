import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Point } from './entities/point.entity';
import { Repository } from 'typeorm';
import { IPoint, PointDTO } from './interface/point.interface';
import { IDisUUID } from '../../common/IDisUUID';
import { log } from 'util';

@Injectable()
export class PointService {
    constructor(
        @InjectRepository(Point)
        private pointRepository: Repository<Point>,
    ) { }

    async showAll() {
        const point = await this.pointRepository
            .createQueryBuilder('point')
            .leftJoinAndSelect('point.schedule', 'schedule')
            .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: point,
        };
    }

    async create(data: IPoint) {
        // tslint:disable-next-line:max-line-length
        const findPoint = await this.pointRepository.findOne({ where: { scheduleId: data.scheduleId, scheduleDate: data.scheduleDate, studentClassSubjectId: data.studentClassSubjectId, studentClassSubjectName: data.studentClassSubjectName }});
        if (findPoint) {
            throw new HttpException('Student point existed', HttpStatus.BAD_REQUEST);
        }
        const point = await this.pointRepository.create(data);
        await this.pointRepository.save(point);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: point,
        };
    }

    async update(id: string, data: Partial<PointDTO>) {
        IDisUUID.checking(id);
        let findPoint = await this.pointRepository.findOne({ where: { id }});
        if (!findPoint) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.pointRepository.update({ id }, data);
        findPoint = await this.pointRepository
            .createQueryBuilder('point')
            .leftJoinAndSelect('point.schedule', 'schedule')
            .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
            .where('point.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findPoint,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findPoint = await this.pointRepository.findOne({ where: { id }});
        if (!findPoint) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.pointRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findPoint,
        };
    }

    async showAllByScheduleId(id: string) {
        const point = await this.pointRepository
            .createQueryBuilder('point')
            .leftJoinAndSelect('point.schedule', 'schedule', 'schedule.id = :id', { id })
            .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: point,
        };
    }

    async showPointByScheduleIdAndStudentId(data: PointDTO) {
        const point = await this.pointRepository
            .createQueryBuilder('point')
            // tslint:disable-next-line:max-line-length
            .where('point.scheduleId = :scheduleId and point.studentClassSubjectId = :studentId', {scheduleId: data.scheduleId, studentId: data.studentClassSubjectId })
            .leftJoinAndSelect('point.schedule', 'schedule')
            .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: point,
        };
    }

    async createAndUpdate(data: IPoint) {
        let findPoint = await this.pointRepository.findOne({
            where: {
                scheduleId: data.scheduleId,
                scheduleDate: data.scheduleDate,
                studentClassSubjectId: data.studentClassSubjectId,
                studentClassSubjectName: data.studentClassSubjectName,
            },
        });
        if (findPoint) {
            const id = await findPoint.id;
            await this.pointRepository.update({ id }, data);
            findPoint = await this.pointRepository
                .createQueryBuilder('point')
                .leftJoinAndSelect('point.schedule', 'schedule')
                .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
                .where('point.id = :id', { id })
                .getOne();
            return {
                status: HttpStatus.OK,
                message: 'Success',
                data: findPoint,
            };
        }
        const point = await this.pointRepository.create(data);
        await this.pointRepository.save(point);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: point,
        };
    }
}
