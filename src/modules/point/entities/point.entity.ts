import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { Schedule } from '../../schedule/entities/schedule.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';

@Entity()
export class Point {
    @PrimaryGeneratedColumn('uuid') id: string; // Khoa chinh

    @ManyToOne(type => Schedule, (m: Schedule) => m.points) schedule: Schedule; // Noi bang
    @Column('uuid') scheduleId: string; // Khai bao khoa ngoai
    @Column('text') scheduleDate: string;

    @ManyToOne(type => StudentClassSubject, (m: StudentClassSubject) => m.points) studentClassSubject: StudentClassSubject;
    @Column('uuid') studentClassSubjectId: string;
    @Column('text') studentClassSubjectName: string;

    @Column('text') plusScore: number;
    @Column('text') totalDailyScore: number;

}
