import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import Major from './entities/major.entity';
import { MajorController } from './major.controller';
import { MajorService } from './major.service';
import { MajorRepository } from './major.repository';

@Module({
  imports: [TypeOrmModule.forFeature([MajorRepository])],
  controllers: [MajorController],
  providers: [MajorService],
})
export class MajorModule { }
