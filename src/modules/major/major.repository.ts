import { Repository, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { EntityRepository } from 'typeorm/decorator/EntityRepository';
import Major from './entities/major.entity';
import { Class } from '../class/entities/class.entity';
import { Teacher } from '../teacher/entities/teacher.entity';

@EntityRepository(Major)
export class MajorRepository extends Repository<Major> {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @OneToMany(type => Class, (c: Class) => c.major) classes: Class[];

    @OneToMany(type => Teacher, (t: Teacher) => t.major) teachers: Teacher[];
}
