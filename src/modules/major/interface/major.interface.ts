import { IsString, IsNotEmpty } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IMajor {
    name: string;
}

export class MajorDTO implements IMajor {
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    name: string;
}
