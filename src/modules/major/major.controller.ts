import { Controller, Get, Post, Put, Delete, Body, Param, UsePipes, Logger, UseGuards, HttpCode, Res } from '@nestjs/common';
import { MajorService } from './major.service';
import { IMajor, MajorDTO } from './interface/major.interface';
import Major from './entities/major.entity';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags, ApiBearerAuth, ApiOkResponse } from '@nestjs/swagger';
import { IPaging, Paging } from '../../common/ParamPaging';
import { Response } from 'express';

@Controller('major')
@ApiUseTags('Major')
@ApiBearerAuth()
export class MajorController {

    private logger = new Logger('MajorController');
    constructor(private majorService: MajorService) {

    }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAllMajor() {
        return this.majorService.showAll();
    }
    @Post('/majorpaging')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    showMajorPaging(@Body() paging: Paging) {
        return this.majorService.showMajorPaging(paging);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    createMajor(@Body() data: MajorDTO) {
        this.logger.log(JSON.stringify(data));
        return this.majorService.create(data);
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    readMajor(@Param('id') id: string) {
        return this.majorService.read(id);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    updateMajor(@Param('id') id: string, @Body() data: MajorDTO) {
        return this.majorService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroyMajor(@Param('id') id: string) {
        return this.majorService.destroy(id);
    }
}
