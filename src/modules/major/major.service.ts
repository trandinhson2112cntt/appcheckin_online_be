import { Injectable, HttpException, HttpStatus, ArgumentsHost, Inject, HttpService, HttpCode } from '@nestjs/common';
import { Repository } from 'typeorm';
import Major from './entities/major.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { IMajor } from './interface/major.interface';
import { IPaging } from '../../common/ParamPaging';

@Injectable()
export class MajorService {
    constructor(
        @InjectRepository(Major)
        private majorRepository: Repository<Major>,
    ) { }

    async showAll() {
        const major = await this.majorRepository.find();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: major,
        };
    }

    async showMajorPaging(paging: IPaging) {
        const major = await this.majorRepository.find({ take: paging.take, skip: paging.skip });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: major,
        };
    }

    async create(data: IMajor) {
        const major = await this.majorRepository.create(data);
        await this.majorRepository.save(major);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: major,
        };
    }

    async read(id: string) {
        const major = await this.majorRepository.findOne({ where: { id } });
        if (!major) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: major,
        };
    }

    async update(id: string, data: Partial<IMajor>) {
        let major = await this.majorRepository.findOne({ where: { id } });
        if (!major) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.majorRepository.update({ id }, data);
        major = await this.majorRepository.findOne({ where: { id } });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: major,
        };
    }

    async destroy(id: string) {
        const major = await this.majorRepository.findOne({ where: { id } });
        if (!major) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.majorRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: major,
        };
    }
}
