import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Class } from '../../class/entities/class.entity';
import { Teacher } from '../../teacher/entities/teacher.entity';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';

@Entity()
export default class Major {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @OneToMany(type => Class, (c: Class) => c.major) classes: Class[];

    @OneToMany(type => Teacher, (t: Teacher) => t.major) teachers: Teacher[];

    @OneToMany(type => ClassSubject, (t: ClassSubject) => t.major) classSubjects: ClassSubject[];

}
