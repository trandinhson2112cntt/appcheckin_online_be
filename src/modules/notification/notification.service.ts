import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { IDisUUID } from '../../common/IDisUUID';
import Notification from './entities/notification.entity';
import { INotification } from './interface/notification.interface';
import moment = require('moment');

@Injectable()
export class NotificationService {
    constructor(
        @InjectRepository(Notification)
        private notificationRepository: Repository<Notification>,
    ) { }

    async showAll() {
        const notification = await this.notificationRepository
            .createQueryBuilder('notification')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: notification,
        };
    }

    async showListByClassSubjectId(id: string) {
        const notification = await this.notificationRepository
            .createQueryBuilder('notification')
            .where('notification.classSubjectId = :id', { id })
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: notification,
        };
    }

    async showListByStudentClassSubjectId(id: string) {
        const notification = await this.notificationRepository
            .createQueryBuilder('notification')
            .where('notification.studentClassSubjectId = :id', { id })
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: notification,
        };
    }

    async create(data: INotification) {
        const findnotification = await this.notificationRepository
            // tslint:disable-next-line:max-line-length
            .findOne({ where: { message: data.message, title: data.title } });
        if (findnotification) {
            throw new HttpException('Notifi existed ', HttpStatus.BAD_REQUEST);
        }
        data.createdDate = moment.now();
        const notification = await this.notificationRepository.create(data);
        await this.notificationRepository.save(notification);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: notification,
        };
    }
    async read(id: string) {
        IDisUUID.checking(id);
        const findnotification = await this.notificationRepository
            .createQueryBuilder('notification')
            .leftJoinAndSelect('notification.classSubject', 'classSubject')
            .leftJoinAndSelect('notification.studentClassSubject', 'studentClassSubject')
            .where('notification.id = :id', { id })
            .getOne();
        if (!findnotification) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findnotification,
        };
    }

    async update(id: string, data: Partial<INotification>) {
        IDisUUID.checking(id);
        let findClassSubject = await this.notificationRepository.findOne({ where: { id } });
        if (!findClassSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.notificationRepository.update({ id }, data);
        findClassSubject = await this.notificationRepository
            .createQueryBuilder('notification')
            .leftJoinAndSelect('notification.classSubject', 'classSubject')
            .leftJoinAndSelect('notification.studentClassSubject', 'studentClassSubject')
            .where('notification.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClassSubject,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findClassSubject = await this.notificationRepository.findOne({ where: { id } });
        if (!findClassSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.notificationRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClassSubject,
        };
    }
}
