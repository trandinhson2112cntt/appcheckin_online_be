import { IsString, IsNotEmpty, IsNumber, IsUUID } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface INotification {
    title: string;
    message: string;
    createdDate: number;
    studentClassSubjectId: string;
    studentName: string;
    classSubjectId: string;
    classSubjectName: string;
}

export class NotificationDTO implements INotification {
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    title: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    message: string;

    @ApiModelPropertyOptional()
    createdDate: number;

    @ApiModelPropertyOptional()
    studentClassSubjectId: string;

    @IsString()
    @ApiModelPropertyOptional()
    studentName: string;

    @ApiModelPropertyOptional()
    classSubjectId: string;

    @IsString()
    @ApiModelPropertyOptional()
    classSubjectName: string;
}
