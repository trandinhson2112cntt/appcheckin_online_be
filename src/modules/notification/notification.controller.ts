import { Controller, Get, UseGuards, Param, Post, UsePipes, ValidationPipe, Body, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { NotificationService } from './notification.service';
import { NotificationDTO } from './interface/notification.interface';

@Controller('notification')
@ApiUseTags('Notification')
@ApiBearerAuth()
@Controller('notification')
export class NotificationController {

    constructor(
        private notificationService: NotificationService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAll() {
        return this.notificationService.showAll();
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    read(@Param('id') id: string) {
        return this.notificationService.read(id);
    }

    @Get('class-subject/:id')
    @UseGuards(AuthGuard('jwt'))
    showListByClassSubjectId(@Param('id') id: string) {
        return this.notificationService.showListByClassSubjectId(id);
    }
    @Get('student-class-subject/:id')
    @UseGuards(AuthGuard('jwt'))
    showListByStudentClassSubjectId(@Param('id') id: string) {
        return this.notificationService.showListByStudentClassSubjectId(id);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: NotificationDTO) {
        return this.notificationService.create(data);
    }

    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: NotificationDTO) {
        return this.notificationService.update(id, data);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.notificationService.destroy(id);
    }
}
