import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from 'typeorm';
import { Class } from '../../class/entities/class.entity';
import { Teacher } from '../../teacher/entities/teacher.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';

@Entity()
export default class Notification {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') title: string;
    @Column('text') message: string;
    @Column({type: 'text', nullable: true}) createdDate: number;

    // @ManyToOne(type => StudentClassSubject, (t: StudentClassSubject) => t.notifications) studentClassSubject: StudentClassSubject;
    @Column({type: 'text', nullable: true}) studentClassSubjectId: string;
    @Column({type: 'text', nullable: true}) studentName: string;

    // @ManyToOne(type => ClassSubject, (t: ClassSubject) => t.notifications) classSubject: ClassSubject;
    @Column({type: 'text', nullable: true}) classSubjectId: string;
    @Column({type: 'text', nullable: true}) classSubjectName: string;

}
