import { Test, TestingModule } from '@nestjs/testing';
import { StudentClassSubjectController } from './student-class-subject.controller';

describe('StudentClassSubject Controller', () => {
  let controller: StudentClassSubjectController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StudentClassSubjectController],
    }).compile();

    controller = module.get<StudentClassSubjectController>(StudentClassSubjectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
