import { IsString, IsNotEmpty, IsUUID, IsNumber } from 'class-validator';
import { ApiModelPropertyOptional, ApiModelProperty } from '@nestjs/swagger';

export class IBodyToken {
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    token: string;
}
export interface IStudentClassSubject {
    classSubjectId: string;
    classSubjectName: string;
    studentId: string;
    studentName: string;
    studentCode: string;
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
    tokenDevice: string;

}

export interface IOneStudentClassSubjectInfo {
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
}

// tslint:disable-next-line:max-classes-per-file
export class StudentClassSubjectDTO implements IStudentClassSubject {
    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    classSubjectId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    classSubjectName: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    studentId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    studentName: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    studentCode: string;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    attendanceScore: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    totalDailyScore: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    midTermScore: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    totalScore: number;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    tokenDevice: string;
}

// Student Score
// tslint:disable-next-line:max-classes-per-file
export class OneStudentClassSubjectScoreDTO implements IOneStudentClassSubjectInfo {
    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    attendanceScore: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    totalDailyScore: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    midTermScore: number;

    @IsNumber()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    totalScore: number;
}
