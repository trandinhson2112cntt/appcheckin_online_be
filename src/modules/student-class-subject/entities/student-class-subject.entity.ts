import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany } from 'typeorm';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
import { Student } from '../../student/entities/student.entity';
import Notification from '../../notification/entities/notification.entity';
import { Absent } from '../../absent/entities/absent.entity';
import { Point } from '../../point/entities/point.entity';

@Entity()
export class StudentClassSubject {
    @PrimaryGeneratedColumn('uuid') id: string;

    @ManyToOne(type => ClassSubject, (cs: ClassSubject) => cs.studentClasses) classSubject: ClassSubject;
    @Column('uuid') classSubjectId: string;
    @Column('text') classSubjectName: string;

    @ManyToOne(type => Student, (t: Student) => t.studentClasses) student: Student;
    @Column('uuid') studentId: string;
    @Column('text') studentName: string;
    @Column('text') studentCode: string;

    @Column('real') attendanceScore: number;

    @Column('real') totalDailyScore: number;

    @Column('real') midTermScore: number;

    @Column('real') totalScore: number;
    
    @Column({ type: 'text', default: ''}) tokenDevice: string;
    // @OneToMany(type => Notification, (n: Notification) => n.studentClassSubject) notifications: Notification[];
    @OneToMany(type => Absent, (n: Absent) => n.studentClassSubject) absents: Absent[];
    @OneToMany(type => Point, (n: Point) => n.studentClassSubject) points: Point[];

}
