import { Controller, Get, UseGuards, Post, UsePipes, Body, Put, Param, Delete, Patch } from '@nestjs/common';
import { StudentClassSubjectService } from './student-class-subject.service';
import { AuthGuard } from '@nestjs/passport';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { StudentClassSubjectDTO, IBodyToken, OneStudentClassSubjectScoreDTO, IStudentClassSubject } from './interface/student-class-subjetc.interface';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';

@Controller('student-class-subject')
@ApiUseTags('Student Class Subject')
@ApiBearerAuth()
export class StudentClassSubjectController {
    constructor(
        private studentClassSubjectService: StudentClassSubjectService,
    ) { }

    @Get()
    @UseGuards(AuthGuard('jwt'))
    showAllStudent() {
        return this.studentClassSubjectService.showAll();
    }

    @Get('all-student-in-class-subject:id')
    @UseGuards(AuthGuard('jwt'))
    showAllStudentInClassSubject(@Param('id') id: string) {
        return this.studentClassSubjectService.showAllStudentWithClassSubjectID(id);
    }

    @Get('all-class-with-student:id')
    @UseGuards(AuthGuard('jwt'))
    showAllClassForStudent(@Param('id') id: string) {
        return this.studentClassSubjectService.showAllClassSubjectFromStudentID(id);
    }

    @Get('all-student-class-with-student:id')
    @UseGuards(AuthGuard('jwt'))
    showAllStudentClassForStudent(@Param('id') id: string) {
        return this.studentClassSubjectService.showAllStudentClassSubjectIdFromStudentID(id);
    }

    @Get('all-class-subject-detail-with-student:id')
    @UseGuards(AuthGuard('jwt'))
    showAllClassSubjectDetailFromStudentID(@Param('id') id: string) {
        return this.studentClassSubjectService.showAllClassSubjectDetailFromStudentID(id);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    create(@Body() data: StudentClassSubjectDTO) {
        return this.studentClassSubjectService.create(data);
    }
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: StudentClassSubjectDTO) {
        return this.studentClassSubjectService.update(id, data);
    }

    @Post('token:id')
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    updateToken(@Param('id') id: string, @Body() token: IBodyToken) {
        return this.studentClassSubjectService.updateTokenDevices(id, token);
    }

    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.studentClassSubjectService.destroy(id);
    }

    @Get('/student-class-subject-info/:id')
    @UseGuards(AuthGuard('jwt'))
    showStudentClassSubjectInfoById(@Param('id') id: string) {
        return this.studentClassSubjectService.studentClassSubjectInfoById(id);
    }

    @Post('/student-class-subject-Score/:id')
    @UseGuards(AuthGuard('jwt'))
    studentClassSubjectScoreById(@Param('id') id: string, @Body() data: OneStudentClassSubjectScoreDTO) {
        return this.studentClassSubjectService.studentClassSubjectScoreById(id, data);
    }

    @Post('/getInfo')
    @UseGuards(AuthGuard('jwt'))
    getstudentClassSubjectScoreById( @Body() data: IStudentClassSubject) {
        return this.studentClassSubjectService.getStudentWithClassSubjectID(data);
    }
}
