import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { StudentClassSubject } from './entities/student-class-subject.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { IDisUUID } from '../../common/IDisUUID';
import { IStudentClassSubject, IBodyToken, IOneStudentClassSubjectInfo } from './interface/student-class-subjetc.interface';
import { IsString, IsNotEmpty } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

// tslint:disable-next-line:max-classes-per-file
@Injectable()
export class StudentClassSubjectService {
    constructor(
        @InjectRepository(StudentClassSubject)
        private studentClassSubjectRepository: Repository<StudentClassSubject>,
    ) { }

    async showAll() {
        const studentClass = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: studentClass,
        };
    }
    async getStudentWithClassSubjectID(data: IStudentClassSubject) {
        // IDisUUID.checking(id);

        const allStudentClass = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            // tslint:disable-next-line:max-line-length
            .where('StudentClassSubject.classSubjectId = :id and StudentClassSubject.studentId = :idStudent', { id: data.classSubjectId, idStudent: data.studentId })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: allStudentClass,
        };
    }
    
    async showAllStudentWithClassSubjectID(id: string) {
        IDisUUID.checking(id);

        const allStudentClass = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .where('StudentClassSubject.classSubjectId = :id', { id })
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: allStudentClass,
        };
    }

    async showAllClassSubjectFromStudentID(id: string) {
        IDisUUID.checking(id);
        const res = [];
        const allClass = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.studentId = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getMany();
        allClass.map(
            item => {
                res.push(item.classSubjectId);
            },
        );
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: res,
        };
    }

    async showAllClassSubjectDetailFromStudentID(id: string) {
        IDisUUID.checking(id);
        const allClass = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.studentId = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getMany();

        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: allClass,
        };
    }

    async showAllStudentClassSubjectIdFromStudentID(id: string) {
        IDisUUID.checking(id);
        const res = [];
        const allClass = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.studentId = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getMany();
        allClass.map(
            item => {
                res.push(item.id);
            },
        );
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: res,
        };
    }
    async create(data: IStudentClassSubject) {
        const findStudent = await this.studentClassSubjectRepository
            .findOne({ where: { classSubjectId: data.classSubjectId, studentId: data.studentId } });
        if (findStudent) {
            throw new HttpException('Student existed ', HttpStatus.BAD_REQUEST);
        }
        const student = await this.studentClassSubjectRepository.create(data);
        await this.studentClassSubjectRepository.save(student);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: student,
        };
    }

    async updateTokenDevices(id: string, tokenDevice: IBodyToken) {
        IDisUUID.checking(id);
        let findStudent = await this.studentClassSubjectRepository.findOne({ where: { id } });
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        findStudent.tokenDevice = tokenDevice.token;
        await this.studentClassSubjectRepository.update({ id }, findStudent);
        findStudent = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.id = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }

    async read(id: string) {
        IDisUUID.checking(id);
        const findStudent = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.id = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getOne();
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }

    async update(id: string, data: Partial<IStudentClassSubject>) {
        IDisUUID.checking(id);
        let findStudent = await this.studentClassSubjectRepository.findOne({ where: { id } });
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.studentClassSubjectRepository.update({ id }, data);
        findStudent = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.id = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findStudent = await this.studentClassSubjectRepository.findOne({ where: { id } });
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.studentClassSubjectRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }

    async studentClassSubjectInfoById(id: string) {
        IDisUUID.checking(id);
        const studentInfo = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.id = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getOne();
        if (!studentInfo) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: studentInfo,
        };
    }

    async studentClassSubjectScoreById(id: string, data: Partial<IOneStudentClassSubjectInfo>) {
        IDisUUID.checking(id);
        let findStudent = await this.studentClassSubjectRepository.findOne({ where: { id } });
        if (!findStudent) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.studentClassSubjectRepository.update({ id }, data);
        findStudent = await this.studentClassSubjectRepository
            .createQueryBuilder('StudentClassSubject')
            .where('StudentClassSubject.id = :id', { id })
            .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
            .leftJoinAndSelect('StudentClassSubject.student', 'student')
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findStudent,
        };
    }
}
