import { Module } from '@nestjs/common';
import { StudentClassSubjectService } from './student-class-subject.service';
import { StudentClassSubjectController } from './student-class-subject.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentClassSubject } from './entities/student-class-subject.entity';

@Module({
  imports: [TypeOrmModule.forFeature([StudentClassSubject])],
  providers: [StudentClassSubjectService],
  controllers: [StudentClassSubjectController],
  exports: [StudentClassSubjectService],
})
export class StudentClassSubjectModule { }
