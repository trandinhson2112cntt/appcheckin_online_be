import { Test, TestingModule } from '@nestjs/testing';
import { StudentClassSubjectService } from './student-class-subject.service';

describe('StudentClassSubjectService', () => {
  let service: StudentClassSubjectService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [StudentClassSubjectService],
    }).compile();

    service = module.get<StudentClassSubjectService>(StudentClassSubjectService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
