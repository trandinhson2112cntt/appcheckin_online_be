import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { ClassSubject } from './entities/class-subject.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { IClassSubject } from './interface/class-subject.interface';
import { IDisUUID } from '../../common/IDisUUID';

@Injectable()
export class ClassSubjectService {
    constructor(
        @InjectRepository(ClassSubject)
        private classSubjectRepository: Repository<ClassSubject>,
    ) { }

    async showAll() {
        const classSubject = await this.classSubjectRepository
            .createQueryBuilder('classsubject') // tao query tren cai ban classSubject
            .leftJoinAndSelect('classsubject.teacher', 'teacher') // lay tat ca danh muc theo khoa ngoai
            .leftJoinAndSelect('classsubject.semester', 'semester') // lay tat ca danh muc theo khoa
            .leftJoinAndSelect('classsubject.subject', 'subject')
            .leftJoinAndSelect('classsubject.major', 'major')
            .getMany(); // lay nhieu dong
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: classSubject,
        };
    }

    // Read the list of students by class
    async showAllClassSubjectByTeacherId(id: string) {
        IDisUUID.checking(id);
        const classSubjectByTeacherId = await this.classSubjectRepository
            .createQueryBuilder('classsubject')
            .leftJoinAndSelect('classsubject.teacher', 'teacher', 'teacher.id = :id', { id }) // condition, param(:id la id truyen vao)
            .leftJoinAndSelect('classsubject.semester', 'semester')
            .leftJoinAndSelect('classsubject.subject', 'subject')
            .leftJoinAndSelect('classsubject.major', 'major')
            .getMany();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: classSubjectByTeacherId,
        };
    }

    async create(data: IClassSubject) {
        const findClassSubject = await this.classSubjectRepository
            .findOne({ where: { name: data.name, semesterName: data.semesterName, semesterYear: data.semesterYear, teacherName: data.teacherName } });
        if (findClassSubject) {
            throw new HttpException('Class Subject existed ', HttpStatus.BAD_REQUEST);
        }
        const classSubject = await this.classSubjectRepository.create(data);
        await this.classSubjectRepository.save(classSubject);
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: classSubject,
        };
    }
    async read(id: string) {
        IDisUUID.checking(id);
        const findClassSubject = await this.classSubjectRepository
            .createQueryBuilder('classsubject')
            .leftJoinAndSelect('classsubject.teacher', 'teacher')
            .leftJoinAndSelect('classsubject.semester', 'semester')
            .leftJoinAndSelect('classsubject.subject', 'subject')
            .leftJoinAndSelect('classsubject.major', 'major')
            .where('classsubject.id = :id', { id })
            .getOne();
        if (!findClassSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClassSubject,
        };
    }

    async update(id: string, data: Partial<IClassSubject>) {
        IDisUUID.checking(id);
        let findClassSubject = await this.classSubjectRepository.findOne({ where: { id } });
        if (!findClassSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.classSubjectRepository.update({ id }, data);
        findClassSubject = await this.classSubjectRepository
            .createQueryBuilder('classsubject')
            .leftJoinAndSelect('classsubject.teacher', 'teacher')
            .leftJoinAndSelect('classsubject.semester', 'semester')
            .leftJoinAndSelect('classsubject.subject', 'subject')
            .leftJoinAndSelect('classsubject.major', 'major')
            .where('classsubject.id = :id', { id })
            .getOne();
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClassSubject,
        };
    }

    async destroy(id: string) {
        IDisUUID.checking(id);
        const findClassSubject = await this.classSubjectRepository.findOne({ where: { id } });
        if (!findClassSubject) {
            throw new HttpException('Not found', HttpStatus.NOT_FOUND);
        }
        await this.classSubjectRepository.delete({ id });
        return {
            status: HttpStatus.OK,
            message: 'Success',
            data: findClassSubject,
        };
    }
}
