import { Controller, Get, UseGuards, Post, Param, Body, UsePipes, Put, Delete } from '@nestjs/common';
import { ApiUseTags, ApiBearerAuth } from '@nestjs/swagger';
import { ClassSubjectService } from './class-subject.service';
import { AuthGuard } from '@nestjs/passport';
import { ValidationPipe } from '../../pipes/validation.pipe';
import { ClassSubjectDTO } from './interface/class-subject.interface';

@Controller('class-subject')
@ApiUseTags('Class Subject')
@ApiBearerAuth()
export class ClassSubjectController {
    constructor(
        private classSubjecttService: ClassSubjectService,
    ) { }

    @Get() // chi truyen dc 1 id
    @UseGuards(AuthGuard('jwt')) // token?
    showAll() {
        return this.classSubjecttService.showAll();
    }

    @Get(':id')
    @UseGuards(AuthGuard('jwt'))
    read(@Param('id') id: string) {
        return this.classSubjecttService.read(id);
    }

    @Get('by-classsubject-teacher/:id')
    @UseGuards(AuthGuard('jwt'))
    showAllClassSubjectTeacherById(@Param('id') id: string) {
        return this.classSubjecttService.showAllClassSubjectByTeacherId(id);
    }

    // Read the list of students by class
    @Post() // truyen duoc ca id va body
    @UseGuards(AuthGuard('jwt')) // Be used token
    @UsePipes(new ValidationPipe()) // validate gia tri truyen vao
    create(@Body() data: ClassSubjectDTO) {
        return this.classSubjecttService.create(data);
    }

    @Put(':id') // edit
    @UseGuards(AuthGuard('jwt'))
    @UsePipes(new ValidationPipe())
    update(@Param('id') id: string, @Body() data: ClassSubjectDTO) {
        return this.classSubjecttService.update(id, data);
    }

    @Delete(':id') // Delete
    @UseGuards(AuthGuard('jwt'))
    destroy(@Param('id') id: string) {
        return this.classSubjecttService.destroy(id);
    }
}
