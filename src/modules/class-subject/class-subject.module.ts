import { Module } from '@nestjs/common';
import { ClassSubjectService } from './class-subject.service';
import { ClassSubjectController } from './class-subject.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClassSubject } from './entities/class-subject.entity';

@Module({
    imports: [TypeOrmModule.forFeature([ClassSubject])],
    providers: [ClassSubjectService],
    controllers: [ClassSubjectController],
})
export class ClassSubjectModule { }
