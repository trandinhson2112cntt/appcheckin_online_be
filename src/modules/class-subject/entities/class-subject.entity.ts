import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne, OneToMany } from 'typeorm';
import { Teacher } from '../../teacher/entities/teacher.entity';
import { Semester } from '../../semester/entities/semester.entity';
import { Subject } from '../../subject/entities/subject.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';
import { Schedule } from '../../schedule/entities/schedule.entity';
import Notification from '../../notification/entities/notification.entity';
import Major from '../../major/entities/major.entity';
@Entity()
export class ClassSubject {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column('text') name: string;

    @ManyToOne(type => Subject, (s: Subject) => s.classSubjects) subject: Subject;
    @Column('uuid') subjectId: string;
    @Column('text') subjectName: string;

    @ManyToOne(type => Teacher, (t: Teacher) => t.classSubjects) teacher: Teacher;
    @Column('uuid') teacherId: string;
    @Column('text') teacherName: string;

    @ManyToOne(type => Semester, (se: Semester) => se.classSubjects) semester: Semester;
    @Column('uuid') semesterId: string;
    @Column('text') semesterName: string;
    @Column('text') semesterYear: string;

    @Column('date') startDate: string;

    @Column('date') endDate: string;
    @OneToMany(type => StudentClassSubject, (scs: StudentClassSubject) => scs.classSubject) studentClasses: StudentClassSubject[];
    @OneToMany(type => Schedule, (s: Schedule) => s.classSubject) schedules: Schedule[];

    @ManyToOne(type => Major, (se: Major) => se.classSubjects) major: Major;
    @Column({type: 'uuid', default: '77c80598-353d-4ac4-b8b9-de99cab01154'}) majorId: string;
    @Column({type: 'text', default: 'Công nghệ thông tin'}) majorName: string;
}
