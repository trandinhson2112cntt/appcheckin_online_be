import { IsString, IsNotEmpty, IsUUID } from 'class-validator';
import { ApiModelPropertyOptional } from '@nestjs/swagger';

export interface IClassSubject {
    name: string;
    subjectId: string;
    subjectName: string;
    semesterYear: string;
    teacherId: string;
    teacherName: string;
    semesterId: string;
    semesterName: string;
    startDate: string;
    endDate: string;
    majorId: string;
    majorName: string;
}

export class ClassSubjectDTO implements IClassSubject {
    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    name: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    subjectId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    subjectName: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    teacherId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    teacherName: string;

    @IsUUID()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    semesterId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    semesterName: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    semesterYear: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    startDate: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    endDate: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    majorId: string;

    @IsString()
    @IsNotEmpty()
    @ApiModelPropertyOptional()
    majorName: string;
}
