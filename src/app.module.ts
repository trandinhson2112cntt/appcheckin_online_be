import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from '../config/database.module';
import { Connection } from 'typeorm';
import { UserModule } from './modules/user/user.module';
import { MajorModule } from './modules/major/major.module';
import { AuthModule } from './modules/auth/auth.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SharedModule } from './shared.module';
import { ConfigService } from './shared-modules/config.service';
import { ScheduleModule } from './modules/schedule/schedule.module';
import { ClassModule } from './modules/class/class.module';
import { StudentModule } from './modules/student/student.module';
import { TeacherController } from './modules/teacher/teacher.controller';
import { TeacherModule } from './modules/teacher/teacher.module';
import { SubjectModule } from './modules/subject/subject.module';
import { SemesterModule } from './modules/semester/semester.module';
import { ClassSubjectModule } from './modules/class-subject/class-subject.module';
import { StudentClassSubjectModule } from './modules/student-class-subject/student-class-subject.module';
import { RoomModule } from './modules/room/room.module';
import { NotificationModule } from './modules/notification/notification.module';
import { AbsentModule } from './modules/absent/absent.module';
import { PointModule } from './modules/point/point.module';

@Module({
  imports: [
    MajorModule,
    UserModule,
    AuthModule,
    SharedModule,
    DatabaseModule,
    ScheduleModule,
    ClassModule,
    StudentModule,
    TeacherModule,
    SubjectModule,
    SemesterModule,
    ClassSubjectModule,
    StudentClassSubjectModule,
    RoomModule,
    NotificationModule,
    AbsentModule,
    PointModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
  ],
})
export class AppModule {
  constructor(private readonly connection: Connection) { }
}
