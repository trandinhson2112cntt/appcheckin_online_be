"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const config_service_1 = require("../src/shared-modules/config.service");
const major_entity_1 = require("../src/modules/major/entities/major.entity");
const user_entity_1 = require("../src/modules/user/entities/user.entity");
const teacher_entity_1 = require("../src/modules/teacher/entities/teacher.entity");
const student_entity_1 = require("../src/modules/student/entities/student.entity");
const subject_entity_1 = require("../src/modules/subject/entities/subject.entity");
const student_class_subject_entity_1 = require("../src/modules/student-class-subject/entities/student-class-subject.entity");
const semester_entity_1 = require("../src/modules/semester/entities/semester.entity");
const schedule_entity_1 = require("../src/modules/schedule/entities/schedule.entity");
const room_entity_1 = require("../src/modules/room/entities/room.entity");
const class_entity_1 = require("../src/modules/class/entities/class.entity");
const class_subject_entity_1 = require("../src/modules/class-subject/entities/class-subject.entity");
const notification_entity_1 = require("../src/modules/notification/entities/notification.entity");
const absent_entity_1 = require("../src/modules/absent/entities/absent.entity");
const point_entity_1 = require("../src/modules/point/entities/point.entity");
const config = new config_service_1.ConfigService();
exports.TypeOrmConfig = {
    type: 'postgres',
    host: config.get('POSTGRES_HOST'),
    port: config.getNumber('POSTGRES_PORT'),
    username: config.get('POSTGRES_USERNAME'),
    password: config.get('POSTGRES_PASSWORD'),
    database: config.get('POSTGRES_DATABASE'),
    synchronize: true,
    logging: true,
    entities: [major_entity_1.default, user_entity_1.User, teacher_entity_1.Teacher, student_entity_1.Student, subject_entity_1.Subject, student_class_subject_entity_1.StudentClassSubject, semester_entity_1.Semester, schedule_entity_1.Schedule, room_entity_1.Room, class_entity_1.Class, class_subject_entity_1.ClassSubject, absent_entity_1.Absent, point_entity_1.Point, notification_entity_1.default],
};
//# sourceMappingURL=database.js.map