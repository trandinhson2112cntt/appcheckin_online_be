export interface IPaging {
    take: number;
    skip: number;
}
export declare class Paging implements IPaging {
    take: number;
    skip: number;
}
