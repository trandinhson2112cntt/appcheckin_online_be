"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
class IDisUUID {
    static checking(id) {
        const var1 = id.split('-');
        if (var1.length !== 5) {
            throw new common_1.HttpException('Invalid UUID string: ' + id, common_1.HttpStatus.BAD_REQUEST);
        }
    }
}
exports.IDisUUID = IDisUUID;
//# sourceMappingURL=IDisUUID.js.map