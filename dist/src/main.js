"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
const http_error_filter_1 = require("./filters/http-error.filter");
const logging_interceptor_1 = require("./interceptors/logging.interceptor");
const swagger_1 = require("@nestjs/swagger");
const shared_module_1 = require("./shared.module");
const config_service_1 = require("./shared-modules/config.service");
function bootstrap() {
    return __awaiter(this, void 0, void 0, function* () {
        const app = yield core_1.NestFactory.create(app_module_1.AppModule);
        app.useGlobalFilters(new http_error_filter_1.HttpErrorFilter());
        app.useGlobalInterceptors(new logging_interceptor_1.LoggingInterceptor());
        const configService = app.select(shared_module_1.SharedModule).get(config_service_1.ConfigService);
        const options = new swagger_1.DocumentBuilder()
            .setTitle('App muster API list')
            .setDescription('API Documentation')
            .setVersion('1.0')
            .addBearerAuth('Authorization', 'header')
            .build();
        const document = swagger_1.SwaggerModule.createDocument(app, options);
        swagger_1.SwaggerModule.setup('api', app, document);
        yield app.listen(3000);
    });
}
bootstrap();
//# sourceMappingURL=main.js.map