"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const database_module_1 = require("../config/database.module");
const typeorm_1 = require("typeorm");
const user_module_1 = require("./modules/user/user.module");
const major_module_1 = require("./modules/major/major.module");
const auth_module_1 = require("./modules/auth/auth.module");
const shared_module_1 = require("./shared.module");
const schedule_module_1 = require("./modules/schedule/schedule.module");
const class_module_1 = require("./modules/class/class.module");
const student_module_1 = require("./modules/student/student.module");
const teacher_module_1 = require("./modules/teacher/teacher.module");
const subject_module_1 = require("./modules/subject/subject.module");
const semester_module_1 = require("./modules/semester/semester.module");
const class_subject_module_1 = require("./modules/class-subject/class-subject.module");
const student_class_subject_module_1 = require("./modules/student-class-subject/student-class-subject.module");
const room_module_1 = require("./modules/room/room.module");
const notification_module_1 = require("./modules/notification/notification.module");
const absent_module_1 = require("./modules/absent/absent.module");
const point_module_1 = require("./modules/point/point.module");
let AppModule = class AppModule {
    constructor(connection) {
        this.connection = connection;
    }
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            major_module_1.MajorModule,
            user_module_1.UserModule,
            auth_module_1.AuthModule,
            shared_module_1.SharedModule,
            database_module_1.DatabaseModule,
            schedule_module_1.ScheduleModule,
            class_module_1.ClassModule,
            student_module_1.StudentModule,
            teacher_module_1.TeacherModule,
            subject_module_1.SubjectModule,
            semester_module_1.SemesterModule,
            class_subject_module_1.ClassSubjectModule,
            student_class_subject_module_1.StudentClassSubjectModule,
            room_module_1.RoomModule,
            notification_module_1.NotificationModule,
            absent_module_1.AbsentModule,
            point_module_1.PointModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [
            app_service_1.AppService,
        ],
    }),
    __metadata("design:paramtypes", [typeorm_1.Connection])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map