import { Building } from './building.entity';
import { Floor } from './floor.entity';
import Schedule from './schedule.entity';
export declare class Room {
    id: string;
    name: string;
    building: Building;
    buildingName: string;
    room: Floor;
    floorName: string;
    schedules: Schedule[];
}
