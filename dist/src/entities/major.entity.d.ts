import { Class } from './class.entity';
import Teacher from './teacher.entity';
export declare class Major {
    id: string;
    name: string;
    classes: Class[];
    teachers: Teacher[];
}
