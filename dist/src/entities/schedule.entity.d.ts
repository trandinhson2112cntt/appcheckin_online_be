import ClassSubject from './class-subject.entity';
import { Room } from './room.entity';
import Point from './point.entity';
export default class Schedule {
    id: string;
    classSubject: ClassSubject;
    classSubjectName: string;
    room: Room;
    roomName: string;
    date: Date;
    startTime: string;
    endTime: string;
    points: Point[];
}
