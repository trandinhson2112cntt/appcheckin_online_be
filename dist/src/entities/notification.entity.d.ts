import ClassSubject from './class-subject.entity';
import StudentClassSubject from './studentClassSubject.entity';
export default class Notification {
    id: string;
    title: string;
    content: string;
    createDate: Date;
    classSubject: ClassSubject;
    classSubjectName: string;
    studentClassSubject: StudentClassSubject;
    studentName: string;
    status: boolean;
}
