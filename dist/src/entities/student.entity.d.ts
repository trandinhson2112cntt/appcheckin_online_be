import { Class } from './class.entity';
import Point from './point.entity';
import StudentClassSubject from './studentClassSubject.entity';
import { User } from './user.entity';
export declare class Student {
    id: string;
    education: string;
    class: Class;
    className: string;
    studentClasses: StudentClassSubject[];
    points: Point[];
    user: User;
}
