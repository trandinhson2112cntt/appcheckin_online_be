"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const schedule_entity_1 = require("./schedule.entity");
const student_entity_1 = require("./student.entity");
let Point = class Point {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Point.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => schedule_entity_1.default, (sc) => sc.points),
    __metadata("design:type", schedule_entity_1.default)
], Point.prototype, "schedule", void 0);
__decorate([
    typeorm_1.ManyToOne(type => student_entity_1.Student, (s) => s.points),
    __metadata("design:type", student_entity_1.Student)
], Point.prototype, "student", void 0);
__decorate([
    typeorm_1.Column('real'),
    __metadata("design:type", Number)
], Point.prototype, "point", void 0);
Point = __decorate([
    typeorm_1.Entity()
], Point);
exports.default = Point;
//# sourceMappingURL=point.entity.js.map