import ClassSubject from './class-subject.entity';
export default class Semester {
    id: string;
    name: string;
    classSubjects: ClassSubject[];
}
