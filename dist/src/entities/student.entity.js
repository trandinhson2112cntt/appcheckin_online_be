"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_entity_1 = require("./class.entity");
const point_entity_1 = require("./point.entity");
const studentClassSubject_entity_1 = require("./studentClassSubject.entity");
const user_entity_1 = require("./user.entity");
let Student = class Student {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Student.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Student.prototype, "education", void 0);
__decorate([
    typeorm_1.ManyToOne(type => class_entity_1.Class, (c) => c.students),
    __metadata("design:type", class_entity_1.Class)
], Student.prototype, "class", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Student.prototype, "className", void 0);
__decorate([
    typeorm_1.OneToMany(type => studentClassSubject_entity_1.default, (scs) => scs.student),
    __metadata("design:type", Array)
], Student.prototype, "studentClasses", void 0);
__decorate([
    typeorm_1.OneToMany(type => point_entity_1.default, (p) => p.student),
    __metadata("design:type", Array)
], Student.prototype, "points", void 0);
__decorate([
    typeorm_1.OneToOne(type => user_entity_1.User),
    typeorm_1.JoinColumn(),
    __metadata("design:type", user_entity_1.User)
], Student.prototype, "user", void 0);
Student = __decorate([
    typeorm_1.Entity()
], Student);
exports.Student = Student;
//# sourceMappingURL=student.entity.js.map