import ClassSubject from './class-subject.entity';
export default class RecipeSubject {
    id: string;
    classSubject: ClassSubject;
    classSubjectName: string;
    coefficientDailyScore: number;
    coefficientAttendanceScore: number;
    coefficientMidTermScore: number;
}
