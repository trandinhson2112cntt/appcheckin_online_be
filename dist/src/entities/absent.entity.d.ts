import Schedule from './schedule.entity';
import { Student } from './student.entity';
export default class Absent {
    id: string;
    schedule: Schedule;
    student: Student;
}
