import Schedule from './schedule.entity';
import { Student } from './student.entity';
export default class Point {
    id: string;
    schedule: Schedule;
    student: Student;
    point: number;
}
