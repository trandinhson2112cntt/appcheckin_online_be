import Subject from './subject.entity';
import Teacher from './teacher.entity';
import Semester from './semester.entity';
import StudentClassSubject from './studentClassSubject.entity';
import Schedule from './schedule.entity';
import Notification from './notification.entity';
export default class ClassSubject {
    id: string;
    name: string;
    subject: Subject;
    subjectName: string;
    teacher: Teacher;
    teacherName: string;
    startDate: string;
    endDate: string;
    semester: Semester;
    semesterName: string;
    studentClasses: StudentClassSubject[];
    schedules: Schedule[];
    notifications: Notification[];
}
