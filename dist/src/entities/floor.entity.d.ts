import { Room } from './room.entity';
export declare class Floor {
    id: string;
    name: string;
    rooms: Room[];
}
