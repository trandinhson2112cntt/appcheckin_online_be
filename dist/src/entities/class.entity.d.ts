import { Major } from './major.entity';
import { Student } from './student.entity';
export declare class Class {
    id: string;
    name: string;
    major: Major;
    majorName: string;
    students: Student[];
}
