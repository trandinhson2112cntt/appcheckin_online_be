import { Major } from './major.entity';
import ClassSubject from './class-subject.entity';
import { User } from './user.entity';
export default class Teacher {
    id: string;
    major: Major;
    majorName: string;
    classSubjects: ClassSubject[];
    user: User;
}
