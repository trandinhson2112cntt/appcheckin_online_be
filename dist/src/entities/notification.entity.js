"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_subject_entity_1 = require("./class-subject.entity");
const studentClassSubject_entity_1 = require("./studentClassSubject.entity");
let Notification = class Notification {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Notification.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Notification.prototype, "title", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Notification.prototype, "content", void 0);
__decorate([
    typeorm_1.Column('timestamp'),
    __metadata("design:type", Date)
], Notification.prototype, "createDate", void 0);
__decorate([
    typeorm_1.ManyToOne(type => class_subject_entity_1.default, (cs) => cs.notifications),
    __metadata("design:type", class_subject_entity_1.default)
], Notification.prototype, "classSubject", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Notification.prototype, "classSubjectName", void 0);
__decorate([
    typeorm_1.ManyToOne(type => studentClassSubject_entity_1.default, (cs) => cs.notifications),
    __metadata("design:type", studentClassSubject_entity_1.default)
], Notification.prototype, "studentClassSubject", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Notification.prototype, "studentName", void 0);
__decorate([
    typeorm_1.Column('boolean'),
    __metadata("design:type", Boolean)
], Notification.prototype, "status", void 0);
Notification = __decorate([
    typeorm_1.Entity()
], Notification);
exports.default = Notification;
//# sourceMappingURL=notification.entity.js.map