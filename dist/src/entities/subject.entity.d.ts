import ClassSubject from './class-subject.entity';
export default class Subject {
    id: string;
    name: string;
    numberOfCredit: string;
    scorePass: number;
    nameGroup: string;
    type: string;
    classSubjects: ClassSubject[];
}
