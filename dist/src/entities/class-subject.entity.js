"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const subject_entity_1 = require("./subject.entity");
const teacher_entity_1 = require("./teacher.entity");
const semester_entity_1 = require("./semester.entity");
const studentClassSubject_entity_1 = require("./studentClassSubject.entity");
const schedule_entity_1 = require("./schedule.entity");
const notification_entity_1 = require("./notification.entity");
let ClassSubject = class ClassSubject {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], ClassSubject.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "name", void 0);
__decorate([
    typeorm_1.ManyToOne(type => subject_entity_1.default, (s) => s.classSubjects),
    __metadata("design:type", subject_entity_1.default)
], ClassSubject.prototype, "subject", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "subjectName", void 0);
__decorate([
    typeorm_1.ManyToOne(type => teacher_entity_1.default, (t) => t.classSubjects),
    __metadata("design:type", teacher_entity_1.default)
], ClassSubject.prototype, "teacher", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "teacherName", void 0);
__decorate([
    typeorm_1.Column('date'),
    __metadata("design:type", String)
], ClassSubject.prototype, "startDate", void 0);
__decorate([
    typeorm_1.Column('date'),
    __metadata("design:type", String)
], ClassSubject.prototype, "endDate", void 0);
__decorate([
    typeorm_1.ManyToOne(type => semester_entity_1.default, (se) => se.classSubjects),
    __metadata("design:type", semester_entity_1.default)
], ClassSubject.prototype, "semester", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "semesterName", void 0);
__decorate([
    typeorm_1.OneToMany(type => studentClassSubject_entity_1.default, (scs) => scs.classSubject),
    __metadata("design:type", Array)
], ClassSubject.prototype, "studentClasses", void 0);
__decorate([
    typeorm_1.OneToMany(type => schedule_entity_1.default, (s) => s.classSubject),
    __metadata("design:type", Array)
], ClassSubject.prototype, "schedules", void 0);
__decorate([
    typeorm_1.OneToMany(type => notification_entity_1.default, (s) => s.classSubject),
    __metadata("design:type", Array)
], ClassSubject.prototype, "notifications", void 0);
ClassSubject = __decorate([
    typeorm_1.Entity()
], ClassSubject);
exports.default = ClassSubject;
//# sourceMappingURL=class-subject.entity.js.map