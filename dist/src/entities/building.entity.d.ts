import { Room } from './room.entity';
export declare class Building {
    id: string;
    name: string;
    rooms: Room[];
}
