import ClassSubject from './class-subject.entity';
import Notification from './notification.entity';
import { Student } from './student.entity';
export default class StudentClassSubject {
    id: string;
    classSubject: ClassSubject;
    classSubjectName: string;
    student: Student;
    studentName: string;
    studentCode: string;
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
    notifications: Notification[];
}
