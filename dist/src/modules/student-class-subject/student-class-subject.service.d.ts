import { HttpStatus } from '@nestjs/common';
import { StudentClassSubject } from './entities/student-class-subject.entity';
import { Repository } from 'typeorm';
import { IStudentClassSubject, IBodyToken, IOneStudentClassSubjectInfo } from './interface/student-class-subjetc.interface';
export declare class StudentClassSubjectService {
    private studentClassSubjectRepository;
    constructor(studentClassSubjectRepository: Repository<StudentClassSubject>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject[];
    }>;
    getStudentWithClassSubjectID(data: IStudentClassSubject): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
    showAllStudentWithClassSubjectID(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject[];
    }>;
    showAllClassSubjectFromStudentID(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: any[];
    }>;
    showAllClassSubjectDetailFromStudentID(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject[];
    }>;
    showAllStudentClassSubjectIdFromStudentID(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: any[];
    }>;
    create(data: IStudentClassSubject): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
    updateTokenDevices(id: string, tokenDevice: IBodyToken): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
    update(id: string, data: Partial<IStudentClassSubject>): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
    studentClassSubjectInfoById(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
    studentClassSubjectScoreById(id: string, data: Partial<IOneStudentClassSubjectInfo>): Promise<{
        status: HttpStatus;
        message: string;
        data: StudentClassSubject;
    }>;
}
