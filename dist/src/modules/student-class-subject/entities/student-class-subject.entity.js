"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_subject_entity_1 = require("../../class-subject/entities/class-subject.entity");
const student_entity_1 = require("../../student/entities/student.entity");
const absent_entity_1 = require("../../absent/entities/absent.entity");
const point_entity_1 = require("../../point/entities/point.entity");
let StudentClassSubject = class StudentClassSubject {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], StudentClassSubject.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => class_subject_entity_1.ClassSubject, (cs) => cs.studentClasses),
    __metadata("design:type", class_subject_entity_1.ClassSubject)
], StudentClassSubject.prototype, "classSubject", void 0);
__decorate([
    typeorm_1.Column('uuid'),
    __metadata("design:type", String)
], StudentClassSubject.prototype, "classSubjectId", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], StudentClassSubject.prototype, "classSubjectName", void 0);
__decorate([
    typeorm_1.ManyToOne(type => student_entity_1.Student, (t) => t.studentClasses),
    __metadata("design:type", student_entity_1.Student)
], StudentClassSubject.prototype, "student", void 0);
__decorate([
    typeorm_1.Column('uuid'),
    __metadata("design:type", String)
], StudentClassSubject.prototype, "studentId", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], StudentClassSubject.prototype, "studentName", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], StudentClassSubject.prototype, "studentCode", void 0);
__decorate([
    typeorm_1.Column('real'),
    __metadata("design:type", Number)
], StudentClassSubject.prototype, "attendanceScore", void 0);
__decorate([
    typeorm_1.Column('real'),
    __metadata("design:type", Number)
], StudentClassSubject.prototype, "totalDailyScore", void 0);
__decorate([
    typeorm_1.Column('real'),
    __metadata("design:type", Number)
], StudentClassSubject.prototype, "midTermScore", void 0);
__decorate([
    typeorm_1.Column('real'),
    __metadata("design:type", Number)
], StudentClassSubject.prototype, "totalScore", void 0);
__decorate([
    typeorm_1.Column({ type: 'text', default: '' }),
    __metadata("design:type", String)
], StudentClassSubject.prototype, "tokenDevice", void 0);
__decorate([
    typeorm_1.OneToMany(type => absent_entity_1.Absent, (n) => n.studentClassSubject),
    __metadata("design:type", Array)
], StudentClassSubject.prototype, "absents", void 0);
__decorate([
    typeorm_1.OneToMany(type => point_entity_1.Point, (n) => n.studentClassSubject),
    __metadata("design:type", Array)
], StudentClassSubject.prototype, "points", void 0);
StudentClassSubject = __decorate([
    typeorm_1.Entity()
], StudentClassSubject);
exports.StudentClassSubject = StudentClassSubject;
//# sourceMappingURL=student-class-subject.entity.js.map