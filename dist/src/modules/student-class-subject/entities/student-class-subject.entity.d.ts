import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
import { Student } from '../../student/entities/student.entity';
import { Absent } from '../../absent/entities/absent.entity';
import { Point } from '../../point/entities/point.entity';
export declare class StudentClassSubject {
    id: string;
    classSubject: ClassSubject;
    classSubjectId: string;
    classSubjectName: string;
    student: Student;
    studentId: string;
    studentName: string;
    studentCode: string;
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
    tokenDevice: string;
    absents: Absent[];
    points: Point[];
}
