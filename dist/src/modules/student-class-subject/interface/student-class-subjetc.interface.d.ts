export declare class IBodyToken {
    token: string;
}
export interface IStudentClassSubject {
    classSubjectId: string;
    classSubjectName: string;
    studentId: string;
    studentName: string;
    studentCode: string;
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
    tokenDevice: string;
}
export interface IOneStudentClassSubjectInfo {
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
}
export declare class StudentClassSubjectDTO implements IStudentClassSubject {
    classSubjectId: string;
    classSubjectName: string;
    studentId: string;
    studentName: string;
    studentCode: string;
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
    tokenDevice: string;
}
export declare class OneStudentClassSubjectScoreDTO implements IOneStudentClassSubjectInfo {
    attendanceScore: number;
    totalDailyScore: number;
    midTermScore: number;
    totalScore: number;
}
