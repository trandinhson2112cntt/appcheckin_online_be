import { StudentClassSubjectService } from './student-class-subject.service';
import { StudentClassSubjectDTO, IBodyToken, OneStudentClassSubjectScoreDTO, IStudentClassSubject } from './interface/student-class-subjetc.interface';
export declare class StudentClassSubjectController {
    private studentClassSubjectService;
    constructor(studentClassSubjectService: StudentClassSubjectService);
    showAllStudent(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject[];
    }>;
    showAllStudentInClassSubject(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject[];
    }>;
    showAllClassForStudent(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: any[];
    }>;
    showAllStudentClassForStudent(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: any[];
    }>;
    showAllClassSubjectDetailFromStudentID(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject[];
    }>;
    create(data: StudentClassSubjectDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject;
    }>;
    update(id: string, data: StudentClassSubjectDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject;
    }>;
    updateToken(id: string, token: IBodyToken): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject;
    }>;
    showStudentClassSubjectInfoById(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject;
    }>;
    studentClassSubjectScoreById(id: string, data: OneStudentClassSubjectScoreDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject;
    }>;
    getstudentClassSubjectScoreById(data: IStudentClassSubject): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student-class-subject.entity").StudentClassSubject;
    }>;
}
