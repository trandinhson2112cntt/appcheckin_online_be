"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const student_class_subject_entity_1 = require("./entities/student-class-subject.entity");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let StudentClassSubjectService = class StudentClassSubjectService {
    constructor(studentClassSubjectRepository) {
        this.studentClassSubjectRepository = studentClassSubjectRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const studentClass = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: studentClass,
            };
        });
    }
    getStudentWithClassSubjectID(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const allStudentClass = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .where('StudentClassSubject.classSubjectId = :id and StudentClassSubject.studentId = :idStudent', { id: data.classSubjectId, idStudent: data.studentId })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: allStudentClass,
            };
        });
    }
    showAllStudentWithClassSubjectID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const allStudentClass = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .where('StudentClassSubject.classSubjectId = :id', { id })
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: allStudentClass,
            };
        });
    }
    showAllClassSubjectFromStudentID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const res = [];
            const allClass = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.studentId = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getMany();
            allClass.map(item => {
                res.push(item.classSubjectId);
            });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: res,
            };
        });
    }
    showAllClassSubjectDetailFromStudentID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const allClass = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.studentId = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: allClass,
            };
        });
    }
    showAllStudentClassSubjectIdFromStudentID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const res = [];
            const allClass = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.studentId = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getMany();
            allClass.map(item => {
                res.push(item.id);
            });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: res,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findStudent = yield this.studentClassSubjectRepository
                .findOne({ where: { classSubjectId: data.classSubjectId, studentId: data.studentId } });
            if (findStudent) {
                throw new common_1.HttpException('Student existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const student = yield this.studentClassSubjectRepository.create(data);
            yield this.studentClassSubjectRepository.save(student);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: student,
            };
        });
    }
    updateTokenDevices(id, tokenDevice) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findStudent = yield this.studentClassSubjectRepository.findOne({ where: { id } });
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            findStudent.tokenDevice = tokenDevice.token;
            yield this.studentClassSubjectRepository.update({ id }, findStudent);
            findStudent = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.id = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findStudent = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.id = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getOne();
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findStudent = yield this.studentClassSubjectRepository.findOne({ where: { id } });
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.studentClassSubjectRepository.update({ id }, data);
            findStudent = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.id = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findStudent = yield this.studentClassSubjectRepository.findOne({ where: { id } });
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.studentClassSubjectRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
    studentClassSubjectInfoById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const studentInfo = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.id = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getOne();
            if (!studentInfo) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: studentInfo,
            };
        });
    }
    studentClassSubjectScoreById(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findStudent = yield this.studentClassSubjectRepository.findOne({ where: { id } });
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.studentClassSubjectRepository.update({ id }, data);
            findStudent = yield this.studentClassSubjectRepository
                .createQueryBuilder('StudentClassSubject')
                .where('StudentClassSubject.id = :id', { id })
                .leftJoinAndSelect('StudentClassSubject.classSubject', 'classSubject')
                .leftJoinAndSelect('StudentClassSubject.student', 'student')
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
};
StudentClassSubjectService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(student_class_subject_entity_1.StudentClassSubject)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], StudentClassSubjectService);
exports.StudentClassSubjectService = StudentClassSubjectService;
//# sourceMappingURL=student-class-subject.service.js.map