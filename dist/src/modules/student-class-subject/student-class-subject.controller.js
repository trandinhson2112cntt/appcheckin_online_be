"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const student_class_subject_service_1 = require("./student-class-subject.service");
const passport_1 = require("@nestjs/passport");
const validation_pipe_1 = require("../../pipes/validation.pipe");
const student_class_subjetc_interface_1 = require("./interface/student-class-subjetc.interface");
const swagger_1 = require("@nestjs/swagger");
let StudentClassSubjectController = class StudentClassSubjectController {
    constructor(studentClassSubjectService) {
        this.studentClassSubjectService = studentClassSubjectService;
    }
    showAllStudent() {
        return this.studentClassSubjectService.showAll();
    }
    showAllStudentInClassSubject(id) {
        return this.studentClassSubjectService.showAllStudentWithClassSubjectID(id);
    }
    showAllClassForStudent(id) {
        return this.studentClassSubjectService.showAllClassSubjectFromStudentID(id);
    }
    showAllStudentClassForStudent(id) {
        return this.studentClassSubjectService.showAllStudentClassSubjectIdFromStudentID(id);
    }
    showAllClassSubjectDetailFromStudentID(id) {
        return this.studentClassSubjectService.showAllClassSubjectDetailFromStudentID(id);
    }
    create(data) {
        return this.studentClassSubjectService.create(data);
    }
    update(id, data) {
        return this.studentClassSubjectService.update(id, data);
    }
    updateToken(id, token) {
        return this.studentClassSubjectService.updateTokenDevices(id, token);
    }
    destroy(id) {
        return this.studentClassSubjectService.destroy(id);
    }
    showStudentClassSubjectInfoById(id) {
        return this.studentClassSubjectService.studentClassSubjectInfoById(id);
    }
    studentClassSubjectScoreById(id, data) {
        return this.studentClassSubjectService.studentClassSubjectScoreById(id, data);
    }
    getstudentClassSubjectScoreById(data) {
        return this.studentClassSubjectService.getStudentWithClassSubjectID(data);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "showAllStudent", null);
__decorate([
    common_1.Get('all-student-in-class-subject:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "showAllStudentInClassSubject", null);
__decorate([
    common_1.Get('all-class-with-student:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "showAllClassForStudent", null);
__decorate([
    common_1.Get('all-student-class-with-student:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "showAllStudentClassForStudent", null);
__decorate([
    common_1.Get('all-class-subject-detail-with-student:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "showAllClassSubjectDetailFromStudentID", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [student_class_subjetc_interface_1.StudentClassSubjectDTO]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, student_class_subjetc_interface_1.StudentClassSubjectDTO]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "update", null);
__decorate([
    common_1.Post('token:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, student_class_subjetc_interface_1.IBodyToken]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "updateToken", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "destroy", null);
__decorate([
    common_1.Get('/student-class-subject-info/:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "showStudentClassSubjectInfoById", null);
__decorate([
    common_1.Post('/student-class-subject-Score/:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, student_class_subjetc_interface_1.OneStudentClassSubjectScoreDTO]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "studentClassSubjectScoreById", null);
__decorate([
    common_1.Post('/getInfo'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], StudentClassSubjectController.prototype, "getstudentClassSubjectScoreById", null);
StudentClassSubjectController = __decorate([
    common_1.Controller('student-class-subject'),
    swagger_1.ApiUseTags('Student Class Subject'),
    swagger_1.ApiBearerAuth(),
    __metadata("design:paramtypes", [student_class_subject_service_1.StudentClassSubjectService])
], StudentClassSubjectController);
exports.StudentClassSubjectController = StudentClassSubjectController;
//# sourceMappingURL=student-class-subject.controller.js.map