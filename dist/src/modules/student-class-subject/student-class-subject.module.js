"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const student_class_subject_service_1 = require("./student-class-subject.service");
const student_class_subject_controller_1 = require("./student-class-subject.controller");
const typeorm_1 = require("@nestjs/typeorm");
const student_class_subject_entity_1 = require("./entities/student-class-subject.entity");
let StudentClassSubjectModule = class StudentClassSubjectModule {
};
StudentClassSubjectModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([student_class_subject_entity_1.StudentClassSubject])],
        providers: [student_class_subject_service_1.StudentClassSubjectService],
        controllers: [student_class_subject_controller_1.StudentClassSubjectController],
        exports: [student_class_subject_service_1.StudentClassSubjectService],
    })
], StudentClassSubjectModule);
exports.StudentClassSubjectModule = StudentClassSubjectModule;
//# sourceMappingURL=student-class-subject.module.js.map