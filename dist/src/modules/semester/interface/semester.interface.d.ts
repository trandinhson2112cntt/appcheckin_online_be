export interface ISemester {
    name: string;
    year: string;
}
export declare class SemesterDTO implements ISemester {
    name: string;
    year: string;
}
