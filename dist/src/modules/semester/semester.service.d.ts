import { HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import { ISemester } from './interface/semester.interface';
import { Semester } from './entities/semester.entity';
export declare class SemesterService {
    private semesterRepository;
    constructor(semesterRepository: Repository<Semester>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Semester[];
    }>;
    create(data: ISemester): Promise<{
        status: HttpStatus;
        message: string;
        data: Semester;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Semester;
    }>;
    update(id: string, data: Partial<ISemester>): Promise<{
        status: HttpStatus;
        message: string;
        data: Semester;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Semester;
    }>;
}
