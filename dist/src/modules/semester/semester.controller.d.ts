import { SemesterService } from './semester.service';
import { SemesterDTO } from './interface/semester.interface';
export declare class SemesterController {
    private semesterService;
    constructor(semesterService: SemesterService);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/semester.entity").Semester[];
    }>;
    create(data: SemesterDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/semester.entity").Semester;
    }>;
    update(id: string, data: SemesterDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/semester.entity").Semester;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/semester.entity").Semester;
    }>;
}
