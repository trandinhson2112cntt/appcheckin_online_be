import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
export declare class Semester {
    id: string;
    name: string;
    year: string;
    classSubjects: ClassSubject[];
}
