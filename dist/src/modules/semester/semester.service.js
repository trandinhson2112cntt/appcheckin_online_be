"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
const typeorm_2 = require("typeorm");
const semester_entity_1 = require("./entities/semester.entity");
let SemesterService = class SemesterService {
    constructor(semesterRepository) {
        this.semesterRepository = semesterRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const semester = yield this.semesterRepository
                .find();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: semester,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findSemester = yield this.semesterRepository.findOne({ where: { name: data.name, year: data.year } });
            if (findSemester) {
                throw new common_1.HttpException('semester existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const semester = yield this.semesterRepository.create(data);
            yield this.semesterRepository.save(semester);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: semester,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findSemester = yield this.semesterRepository
                .createQueryBuilder('semester')
                .where('semester.id = :id', { id })
                .getOne();
            if (!findSemester) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSemester,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findSemester = yield this.semesterRepository.findOne({ where: { id } });
            if (!findSemester) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.semesterRepository.update({ id }, data);
            findSemester = yield this.semesterRepository
                .createQueryBuilder('semester')
                .where('semester.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSemester,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findSemester = yield this.semesterRepository.findOne({ where: { id } });
            if (!findSemester) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.semesterRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSemester,
            };
        });
    }
};
SemesterService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(semester_entity_1.Semester)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SemesterService);
exports.SemesterService = SemesterService;
//# sourceMappingURL=semester.service.js.map