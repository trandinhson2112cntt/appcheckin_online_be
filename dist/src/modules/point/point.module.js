"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const point_service_1 = require("./point.service");
const point_controller_1 = require("./point.controller");
const typeorm_1 = require("@nestjs/typeorm");
const point_entity_1 = require("./entities/point.entity");
let PointModule = class PointModule {
};
PointModule = __decorate([
    common_1.Module({
        imports: [typeorm_1.TypeOrmModule.forFeature([point_entity_1.Point])],
        controllers: [point_controller_1.PointController],
        providers: [point_service_1.PointService],
    })
], PointModule);
exports.PointModule = PointModule;
//# sourceMappingURL=point.module.js.map