"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const point_service_1 = require("./point.service");
const passport_1 = require("@nestjs/passport");
const validation_pipe_1 = require("../../pipes/validation.pipe");
const point_interface_1 = require("./interface/point.interface");
let PointController = class PointController {
    constructor(pointService) {
        this.pointService = pointService;
    }
    showAll() {
        return this.pointService.showAll();
    }
    create(data) {
        return this.pointService.create(data);
    }
    update(id, data) {
        return this.pointService.update(id, data);
    }
    destroy(id) {
        return this.pointService.destroy(id);
    }
    showAllByScheduleId(id) {
        return this.pointService.showAllByScheduleId(id);
    }
    createAndUpdate(data) {
        return this.pointService.createAndUpdate(data);
    }
    showByStudent(data) {
        return this.pointService.showPointByScheduleIdAndStudentId(data);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], PointController.prototype, "showAll", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [point_interface_1.PointDTO]),
    __metadata("design:returntype", void 0)
], PointController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, point_interface_1.PointDTO]),
    __metadata("design:returntype", void 0)
], PointController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PointController.prototype, "destroy", null);
__decorate([
    common_1.Get('/showAllByScheduleId/:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], PointController.prototype, "showAllByScheduleId", null);
__decorate([
    common_1.Post('/createAndUpdate'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [point_interface_1.PointDTO]),
    __metadata("design:returntype", void 0)
], PointController.prototype, "createAndUpdate", null);
__decorate([
    common_1.Post('/showByStudent'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [point_interface_1.PointDTO]),
    __metadata("design:returntype", void 0)
], PointController.prototype, "showByStudent", null);
PointController = __decorate([
    common_1.Controller('point'),
    swagger_1.ApiUseTags('Point'),
    swagger_1.ApiBearerAuth(),
    __metadata("design:paramtypes", [point_service_1.PointService])
], PointController);
exports.PointController = PointController;
//# sourceMappingURL=point.controller.js.map