import { PointService } from './point.service';
import { PointDTO } from './interface/point.interface';
export declare class PointController {
    private pointService;
    constructor(pointService: PointService);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/point.entity").Point[];
    }>;
    create(data: PointDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/point.entity").Point;
    }>;
    update(id: string, data: PointDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/point.entity").Point;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/point.entity").Point;
    }>;
    showAllByScheduleId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/point.entity").Point[];
    }>;
    createAndUpdate(data: PointDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/point.entity").Point;
    }>;
    showByStudent(data: PointDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/point.entity").Point;
    }>;
}
