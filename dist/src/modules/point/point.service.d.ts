import { HttpStatus } from '@nestjs/common';
import { Point } from './entities/point.entity';
import { Repository } from 'typeorm';
import { IPoint, PointDTO } from './interface/point.interface';
export declare class PointService {
    private pointRepository;
    constructor(pointRepository: Repository<Point>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Point[];
    }>;
    create(data: IPoint): Promise<{
        status: HttpStatus;
        message: string;
        data: Point;
    }>;
    update(id: string, data: Partial<PointDTO>): Promise<{
        status: HttpStatus;
        message: string;
        data: Point;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Point;
    }>;
    showAllByScheduleId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Point[];
    }>;
    showPointByScheduleIdAndStudentId(data: PointDTO): Promise<{
        status: HttpStatus;
        message: string;
        data: Point;
    }>;
    createAndUpdate(data: IPoint): Promise<{
        status: HttpStatus;
        message: string;
        data: Point;
    }>;
}
