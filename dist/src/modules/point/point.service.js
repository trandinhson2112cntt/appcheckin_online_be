"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const point_entity_1 = require("./entities/point.entity");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let PointService = class PointService {
    constructor(pointRepository) {
        this.pointRepository = pointRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const point = yield this.pointRepository
                .createQueryBuilder('point')
                .leftJoinAndSelect('point.schedule', 'schedule')
                .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: point,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findPoint = yield this.pointRepository.findOne({ where: { scheduleId: data.scheduleId, scheduleDate: data.scheduleDate, studentClassSubjectId: data.studentClassSubjectId, studentClassSubjectName: data.studentClassSubjectName } });
            if (findPoint) {
                throw new common_1.HttpException('Student point existed', common_1.HttpStatus.BAD_REQUEST);
            }
            const point = yield this.pointRepository.create(data);
            yield this.pointRepository.save(point);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: point,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findPoint = yield this.pointRepository.findOne({ where: { id } });
            if (!findPoint) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.pointRepository.update({ id }, data);
            findPoint = yield this.pointRepository
                .createQueryBuilder('point')
                .leftJoinAndSelect('point.schedule', 'schedule')
                .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
                .where('point.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findPoint,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findPoint = yield this.pointRepository.findOne({ where: { id } });
            if (!findPoint) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.pointRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findPoint,
            };
        });
    }
    showAllByScheduleId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const point = yield this.pointRepository
                .createQueryBuilder('point')
                .leftJoinAndSelect('point.schedule', 'schedule', 'schedule.id = :id', { id })
                .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: point,
            };
        });
    }
    showPointByScheduleIdAndStudentId(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const point = yield this.pointRepository
                .createQueryBuilder('point')
                .where('point.scheduleId = :scheduleId and point.studentClassSubjectId = :studentId', { scheduleId: data.scheduleId, studentId: data.studentClassSubjectId })
                .leftJoinAndSelect('point.schedule', 'schedule')
                .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: point,
            };
        });
    }
    createAndUpdate(data) {
        return __awaiter(this, void 0, void 0, function* () {
            let findPoint = yield this.pointRepository.findOne({
                where: {
                    scheduleId: data.scheduleId,
                    scheduleDate: data.scheduleDate,
                    studentClassSubjectId: data.studentClassSubjectId,
                    studentClassSubjectName: data.studentClassSubjectName,
                },
            });
            if (findPoint) {
                const id = yield findPoint.id;
                yield this.pointRepository.update({ id }, data);
                findPoint = yield this.pointRepository
                    .createQueryBuilder('point')
                    .leftJoinAndSelect('point.schedule', 'schedule')
                    .leftJoinAndSelect('point.studentClassSubject', 'studentClassSubject')
                    .where('point.id = :id', { id })
                    .getOne();
                return {
                    status: common_1.HttpStatus.OK,
                    message: 'Success',
                    data: findPoint,
                };
            }
            const point = yield this.pointRepository.create(data);
            yield this.pointRepository.save(point);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: point,
            };
        });
    }
};
PointService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(point_entity_1.Point)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], PointService);
exports.PointService = PointService;
//# sourceMappingURL=point.service.js.map