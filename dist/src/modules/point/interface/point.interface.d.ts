export interface IPoint {
    scheduleId: string;
    scheduleDate: string;
    studentClassSubjectId: string;
    studentClassSubjectName: string;
    plusScore: number;
    totalDailyScore: number;
}
export declare class PointDTO implements IPoint {
    scheduleId: string;
    scheduleDate: string;
    studentClassSubjectId: string;
    studentClassSubjectName: string;
    plusScore: number;
    totalDailyScore: number;
}
