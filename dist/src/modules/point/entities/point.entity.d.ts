import { Schedule } from '../../schedule/entities/schedule.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';
export declare class Point {
    id: string;
    schedule: Schedule;
    scheduleId: string;
    scheduleDate: string;
    studentClassSubject: StudentClassSubject;
    studentClassSubjectId: string;
    studentClassSubjectName: string;
    plusScore: number;
    totalDailyScore: number;
}
