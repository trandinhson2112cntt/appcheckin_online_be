"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class ClassSubjectDTO {
}
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "name", void 0);
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "subjectId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "subjectName", void 0);
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "teacherId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "teacherName", void 0);
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "semesterId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "semesterName", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "semesterYear", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "startDate", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "endDate", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "majorId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ClassSubjectDTO.prototype, "majorName", void 0);
exports.ClassSubjectDTO = ClassSubjectDTO;
//# sourceMappingURL=class-subject.interface.js.map