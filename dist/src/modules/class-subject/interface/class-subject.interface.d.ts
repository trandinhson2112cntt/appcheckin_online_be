export interface IClassSubject {
    name: string;
    subjectId: string;
    subjectName: string;
    semesterYear: string;
    teacherId: string;
    teacherName: string;
    semesterId: string;
    semesterName: string;
    startDate: string;
    endDate: string;
    majorId: string;
    majorName: string;
}
export declare class ClassSubjectDTO implements IClassSubject {
    name: string;
    subjectId: string;
    subjectName: string;
    teacherId: string;
    teacherName: string;
    semesterId: string;
    semesterName: string;
    semesterYear: string;
    startDate: string;
    endDate: string;
    majorId: string;
    majorName: string;
}
