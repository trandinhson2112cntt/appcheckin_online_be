import { ClassSubjectService } from './class-subject.service';
import { ClassSubjectDTO } from './interface/class-subject.interface';
export declare class ClassSubjectController {
    private classSubjecttService;
    constructor(classSubjecttService: ClassSubjectService);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class-subject.entity").ClassSubject[];
    }>;
    read(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class-subject.entity").ClassSubject;
    }>;
    showAllClassSubjectTeacherById(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class-subject.entity").ClassSubject[];
    }>;
    create(data: ClassSubjectDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class-subject.entity").ClassSubject;
    }>;
    update(id: string, data: ClassSubjectDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class-subject.entity").ClassSubject;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class-subject.entity").ClassSubject;
    }>;
}
