"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const class_subject_service_1 = require("./class-subject.service");
const passport_1 = require("@nestjs/passport");
const validation_pipe_1 = require("../../pipes/validation.pipe");
const class_subject_interface_1 = require("./interface/class-subject.interface");
let ClassSubjectController = class ClassSubjectController {
    constructor(classSubjecttService) {
        this.classSubjecttService = classSubjecttService;
    }
    showAll() {
        return this.classSubjecttService.showAll();
    }
    read(id) {
        return this.classSubjecttService.read(id);
    }
    showAllClassSubjectTeacherById(id) {
        return this.classSubjecttService.showAllClassSubjectByTeacherId(id);
    }
    create(data) {
        return this.classSubjecttService.create(data);
    }
    update(id, data) {
        return this.classSubjecttService.update(id, data);
    }
    destroy(id) {
        return this.classSubjecttService.destroy(id);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ClassSubjectController.prototype, "showAll", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ClassSubjectController.prototype, "read", null);
__decorate([
    common_1.Get('by-classsubject-teacher/:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ClassSubjectController.prototype, "showAllClassSubjectTeacherById", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [class_subject_interface_1.ClassSubjectDTO]),
    __metadata("design:returntype", void 0)
], ClassSubjectController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, class_subject_interface_1.ClassSubjectDTO]),
    __metadata("design:returntype", void 0)
], ClassSubjectController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ClassSubjectController.prototype, "destroy", null);
ClassSubjectController = __decorate([
    common_1.Controller('class-subject'),
    swagger_1.ApiUseTags('Class Subject'),
    swagger_1.ApiBearerAuth(),
    __metadata("design:paramtypes", [class_subject_service_1.ClassSubjectService])
], ClassSubjectController);
exports.ClassSubjectController = ClassSubjectController;
//# sourceMappingURL=class-subject.controller.js.map