"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_subject_entity_1 = require("./entities/class-subject.entity");
const typeorm_1 = require("typeorm");
const typeorm_2 = require("@nestjs/typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let ClassSubjectService = class ClassSubjectService {
    constructor(classSubjectRepository) {
        this.classSubjectRepository = classSubjectRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const classSubject = yield this.classSubjectRepository
                .createQueryBuilder('classsubject')
                .leftJoinAndSelect('classsubject.teacher', 'teacher')
                .leftJoinAndSelect('classsubject.semester', 'semester')
                .leftJoinAndSelect('classsubject.subject', 'subject')
                .leftJoinAndSelect('classsubject.major', 'major')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: classSubject,
            };
        });
    }
    showAllClassSubjectByTeacherId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const classSubjectByTeacherId = yield this.classSubjectRepository
                .createQueryBuilder('classsubject')
                .leftJoinAndSelect('classsubject.teacher', 'teacher', 'teacher.id = :id', { id })
                .leftJoinAndSelect('classsubject.semester', 'semester')
                .leftJoinAndSelect('classsubject.subject', 'subject')
                .leftJoinAndSelect('classsubject.major', 'major')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: classSubjectByTeacherId,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findClassSubject = yield this.classSubjectRepository
                .findOne({ where: { name: data.name, semesterName: data.semesterName, semesterYear: data.semesterYear, teacherName: data.teacherName } });
            if (findClassSubject) {
                throw new common_1.HttpException('Class Subject existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const classSubject = yield this.classSubjectRepository.create(data);
            yield this.classSubjectRepository.save(classSubject);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: classSubject,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findClassSubject = yield this.classSubjectRepository
                .createQueryBuilder('classsubject')
                .leftJoinAndSelect('classsubject.teacher', 'teacher')
                .leftJoinAndSelect('classsubject.semester', 'semester')
                .leftJoinAndSelect('classsubject.subject', 'subject')
                .leftJoinAndSelect('classsubject.major', 'major')
                .where('classsubject.id = :id', { id })
                .getOne();
            if (!findClassSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClassSubject,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findClassSubject = yield this.classSubjectRepository.findOne({ where: { id } });
            if (!findClassSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.classSubjectRepository.update({ id }, data);
            findClassSubject = yield this.classSubjectRepository
                .createQueryBuilder('classsubject')
                .leftJoinAndSelect('classsubject.teacher', 'teacher')
                .leftJoinAndSelect('classsubject.semester', 'semester')
                .leftJoinAndSelect('classsubject.subject', 'subject')
                .leftJoinAndSelect('classsubject.major', 'major')
                .where('classsubject.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClassSubject,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findClassSubject = yield this.classSubjectRepository.findOne({ where: { id } });
            if (!findClassSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.classSubjectRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClassSubject,
            };
        });
    }
};
ClassSubjectService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(class_subject_entity_1.ClassSubject)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], ClassSubjectService);
exports.ClassSubjectService = ClassSubjectService;
//# sourceMappingURL=class-subject.service.js.map