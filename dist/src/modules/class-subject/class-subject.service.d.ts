import { HttpStatus } from '@nestjs/common';
import { ClassSubject } from './entities/class-subject.entity';
import { Repository } from 'typeorm';
import { IClassSubject } from './interface/class-subject.interface';
export declare class ClassSubjectService {
    private classSubjectRepository;
    constructor(classSubjectRepository: Repository<ClassSubject>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: ClassSubject[];
    }>;
    showAllClassSubjectByTeacherId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: ClassSubject[];
    }>;
    create(data: IClassSubject): Promise<{
        status: HttpStatus;
        message: string;
        data: ClassSubject;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: ClassSubject;
    }>;
    update(id: string, data: Partial<IClassSubject>): Promise<{
        status: HttpStatus;
        message: string;
        data: ClassSubject;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: ClassSubject;
    }>;
}
