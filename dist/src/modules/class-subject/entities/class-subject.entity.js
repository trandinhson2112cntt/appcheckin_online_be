"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const teacher_entity_1 = require("../../teacher/entities/teacher.entity");
const semester_entity_1 = require("../../semester/entities/semester.entity");
const subject_entity_1 = require("../../subject/entities/subject.entity");
const student_class_subject_entity_1 = require("../../student-class-subject/entities/student-class-subject.entity");
const schedule_entity_1 = require("../../schedule/entities/schedule.entity");
const major_entity_1 = require("../../major/entities/major.entity");
let ClassSubject = class ClassSubject {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], ClassSubject.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "name", void 0);
__decorate([
    typeorm_1.ManyToOne(type => subject_entity_1.Subject, (s) => s.classSubjects),
    __metadata("design:type", subject_entity_1.Subject)
], ClassSubject.prototype, "subject", void 0);
__decorate([
    typeorm_1.Column('uuid'),
    __metadata("design:type", String)
], ClassSubject.prototype, "subjectId", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "subjectName", void 0);
__decorate([
    typeorm_1.ManyToOne(type => teacher_entity_1.Teacher, (t) => t.classSubjects),
    __metadata("design:type", teacher_entity_1.Teacher)
], ClassSubject.prototype, "teacher", void 0);
__decorate([
    typeorm_1.Column('uuid'),
    __metadata("design:type", String)
], ClassSubject.prototype, "teacherId", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "teacherName", void 0);
__decorate([
    typeorm_1.ManyToOne(type => semester_entity_1.Semester, (se) => se.classSubjects),
    __metadata("design:type", semester_entity_1.Semester)
], ClassSubject.prototype, "semester", void 0);
__decorate([
    typeorm_1.Column('uuid'),
    __metadata("design:type", String)
], ClassSubject.prototype, "semesterId", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "semesterName", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], ClassSubject.prototype, "semesterYear", void 0);
__decorate([
    typeorm_1.Column('date'),
    __metadata("design:type", String)
], ClassSubject.prototype, "startDate", void 0);
__decorate([
    typeorm_1.Column('date'),
    __metadata("design:type", String)
], ClassSubject.prototype, "endDate", void 0);
__decorate([
    typeorm_1.OneToMany(type => student_class_subject_entity_1.StudentClassSubject, (scs) => scs.classSubject),
    __metadata("design:type", Array)
], ClassSubject.prototype, "studentClasses", void 0);
__decorate([
    typeorm_1.OneToMany(type => schedule_entity_1.Schedule, (s) => s.classSubject),
    __metadata("design:type", Array)
], ClassSubject.prototype, "schedules", void 0);
__decorate([
    typeorm_1.ManyToOne(type => major_entity_1.default, (se) => se.classSubjects),
    __metadata("design:type", major_entity_1.default)
], ClassSubject.prototype, "major", void 0);
__decorate([
    typeorm_1.Column({ type: 'uuid', default: '77c80598-353d-4ac4-b8b9-de99cab01154' }),
    __metadata("design:type", String)
], ClassSubject.prototype, "majorId", void 0);
__decorate([
    typeorm_1.Column({ type: 'text', default: 'Công nghệ thông tin' }),
    __metadata("design:type", String)
], ClassSubject.prototype, "majorName", void 0);
ClassSubject = __decorate([
    typeorm_1.Entity()
], ClassSubject);
exports.ClassSubject = ClassSubject;
//# sourceMappingURL=class-subject.entity.js.map