import { Teacher } from '../../teacher/entities/teacher.entity';
import { Semester } from '../../semester/entities/semester.entity';
import { Subject } from '../../subject/entities/subject.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';
import { Schedule } from '../../schedule/entities/schedule.entity';
import Major from '../../major/entities/major.entity';
export declare class ClassSubject {
    id: string;
    name: string;
    subject: Subject;
    subjectId: string;
    subjectName: string;
    teacher: Teacher;
    teacherId: string;
    teacherName: string;
    semester: Semester;
    semesterId: string;
    semesterName: string;
    semesterYear: string;
    startDate: string;
    endDate: string;
    studentClasses: StudentClassSubject[];
    schedules: Schedule[];
    major: Major;
    majorId: string;
    majorName: string;
}
