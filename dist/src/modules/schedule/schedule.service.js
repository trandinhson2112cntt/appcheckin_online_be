"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const schedule_entity_1 = require("./entities/schedule.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
const moment = require("moment");
const student_class_subject_entity_1 = require("../student-class-subject/entities/student-class-subject.entity");
let ScheduleService = class ScheduleService {
    constructor(scheduleRepository, studentClassSubjectRepository) {
        this.scheduleRepository = scheduleRepository;
        this.studentClassSubjectRepository = studentClassSubjectRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const schedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .leftJoinAndSelect('schedule.classSubject', 'classSubject')
                .leftJoinAndSelect('schedule.room', 'room')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: schedule,
            };
        });
    }
    showAllByTeacherID(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const schedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .leftJoinAndSelect('schedule.classSubject', 'classSubject', 'classSubject.teacherId = :id', { id })
                .leftJoinAndSelect('schedule.room', 'room')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: schedule,
            };
        });
    }
    showAllByTeacherByMonthAndWeek(id, params) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const schedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .leftJoinAndSelect('schedule.classSubject', 'classSubject', 'classSubject.teacherId = :id', { id })
                .leftJoinAndSelect('schedule.room', 'room')
                .getMany();
            const data = [];
            yield schedule.map((item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                const valueMonth = date.month();
                const w = date.week();
                if (valueMonth === params.month && w === params.week) {
                    data.push(item);
                }
            });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data,
            };
        });
    }
    showScheduleByClassSubjectNoParams(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const schedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .where('schedule.classSubjectId = :id', { id })
                .leftJoinAndSelect('schedule.classSubject', 'classSubject')
                .leftJoinAndSelect('schedule.room', 'room')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                schedule,
            };
        });
    }
    showScheduleByClassSubject(id, params) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const schedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .where('schedule.classSubjectId = :id', { id })
                .leftJoinAndSelect('schedule.classSubject', 'classSubject')
                .leftJoinAndSelect('schedule.room', 'room')
                .getMany();
            const data = [];
            yield schedule.map((item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                const numberDay = date.toDate().valueOf();
                const numberDayStart = moment(params.dateStart, 'DD/MM/YYYY').toDate().valueOf();
                const numberDayEnd = moment(params.dateEnd, 'DD/MM/YYYY').toDate().valueOf();
                if (numberDay >= numberDayStart && numberDay <= numberDayEnd) {
                    data.push(item);
                }
            });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data,
            };
        });
    }
    showScheduleRecent(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const schedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .leftJoinAndSelect('schedule.classSubject', 'classSubject', 'classSubject.teacherId = :id', { id })
                .leftJoinAndSelect('schedule.room', 'room')
                .getMany();
            const data = [];
            yield schedule.map((item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                const valueMonth = date.month();
                const valueDay = date.date();
                const currentDay = moment('01/01/2019');
                const valueMonthCur = currentDay.month();
                const valueDayCur = currentDay.date();
                if (valueMonth === valueMonthCur && valueDay >= valueDayCur) {
                    data.push(valueDay);
                }
            });
            const result = [];
            const minValue = yield Math.min(...data);
            yield schedule.map((item) => {
                const date = moment(item.date, 'DD/MM/YYYY');
                const valueMonth = date.month();
                const valueDay = date.date();
                const currentDay = moment('01/01/2019');
                const valueMonthCur = currentDay.month();
                const valueDayCur = currentDay.date();
                if (valueMonth === valueMonthCur && valueDay === minValue) {
                    result.push(item);
                }
            });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: result,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findSchedule = yield this.scheduleRepository
                .findOne({ where: { classSubjectId: data.classSubjectId, startTime: data.startTime, roomId: data.roomId, date: data.date } });
            if (findSchedule) {
                throw new common_1.HttpException('Class Subject existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const schedule = yield this.scheduleRepository.create(data);
            yield this.scheduleRepository.save(schedule);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: schedule,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findSchedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .leftJoinAndSelect('schedule.classSubject', 'classSubject')
                .leftJoinAndSelect('schedule.room', 'room')
                .where('schedule.id = :id', { id })
                .getOne();
            if (!findSchedule) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSchedule,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findSchedule = yield this.scheduleRepository.findOne({ where: { id } });
            if (!findSchedule) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.scheduleRepository.update({ id }, data);
            findSchedule = yield this.scheduleRepository
                .createQueryBuilder('schedule')
                .leftJoinAndSelect('schedule.classSubject', 'classSubject')
                .leftJoinAndSelect('schedule.room', 'room')
                .where('schedule.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSchedule,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findSchedule = yield this.scheduleRepository.findOne({ where: { id } });
            if (!findSchedule) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.scheduleRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSchedule,
            };
        });
    }
};
ScheduleService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(schedule_entity_1.Schedule)),
    __param(1, typeorm_1.InjectRepository(student_class_subject_entity_1.StudentClassSubject)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository])
], ScheduleService);
exports.ScheduleService = ScheduleService;
//# sourceMappingURL=schedule.service.js.map