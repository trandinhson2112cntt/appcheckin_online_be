import { Room } from '../../room/entities/room.entity';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
import { Absent } from '../../absent/entities/absent.entity';
import { Point } from '../../point/entities/point.entity';
export declare class Schedule {
    id: string;
    classSubject: ClassSubject;
    classSubjectId: string;
    classSubjectName: string;
    room: Room;
    roomId: string;
    date: string;
    startTime: string;
    endTime: string;
    dailyScore: number;
    absents: Absent[];
    points: Point[];
}
