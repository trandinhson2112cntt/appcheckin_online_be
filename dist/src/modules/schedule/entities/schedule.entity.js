"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const room_entity_1 = require("../../room/entities/room.entity");
const class_subject_entity_1 = require("../../class-subject/entities/class-subject.entity");
const absent_entity_1 = require("../../absent/entities/absent.entity");
const point_entity_1 = require("../../point/entities/point.entity");
let Schedule = class Schedule {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Schedule.prototype, "id", void 0);
__decorate([
    typeorm_1.ManyToOne(type => class_subject_entity_1.ClassSubject, (cs) => cs.schedules),
    __metadata("design:type", class_subject_entity_1.ClassSubject)
], Schedule.prototype, "classSubject", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Schedule.prototype, "classSubjectId", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Schedule.prototype, "classSubjectName", void 0);
__decorate([
    typeorm_1.ManyToOne(type => room_entity_1.Room, (cs) => cs.schedules),
    __metadata("design:type", room_entity_1.Room)
], Schedule.prototype, "room", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Schedule.prototype, "roomId", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Schedule.prototype, "date", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Schedule.prototype, "startTime", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Schedule.prototype, "endTime", void 0);
__decorate([
    typeorm_1.Column({ type: 'real', default: 0 }),
    __metadata("design:type", Number)
], Schedule.prototype, "dailyScore", void 0);
__decorate([
    typeorm_1.OneToMany(type => absent_entity_1.Absent, (abs) => abs.schedule),
    __metadata("design:type", Array)
], Schedule.prototype, "absents", void 0);
__decorate([
    typeorm_1.OneToMany(type => point_entity_1.Point, (abs) => abs.schedule),
    __metadata("design:type", Array)
], Schedule.prototype, "points", void 0);
Schedule = __decorate([
    typeorm_1.Entity()
], Schedule);
exports.Schedule = Schedule;
//# sourceMappingURL=schedule.entity.js.map