export interface IDateOfWeek {
    dateStart: string;
    dateEnd: string;
}
export declare class DateOfWeek implements IDateOfWeek {
    dateStart: string;
    dateEnd: string;
}
