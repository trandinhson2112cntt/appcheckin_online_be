export interface IMonthOfWeek {
    month: number;
    week: number;
}
export declare class MonthOfWeek implements IMonthOfWeek {
    month: number;
    week: number;
}
