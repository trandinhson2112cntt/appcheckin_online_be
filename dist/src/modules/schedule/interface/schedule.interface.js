"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
const swagger_1 = require("@nestjs/swagger");
class ScheduleDTO {
}
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ScheduleDTO.prototype, "classSubjectId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ScheduleDTO.prototype, "classSubjectName", void 0);
__decorate([
    class_validator_1.IsUUID(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ScheduleDTO.prototype, "roomId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ScheduleDTO.prototype, "date", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ScheduleDTO.prototype, "startTime", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], ScheduleDTO.prototype, "endTime", void 0);
__decorate([
    class_validator_1.IsNumber(),
    class_validator_1.IsNotEmpty(),
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", Number)
], ScheduleDTO.prototype, "dailyScore", void 0);
exports.ScheduleDTO = ScheduleDTO;
//# sourceMappingURL=schedule.interface.js.map