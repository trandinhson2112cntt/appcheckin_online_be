export interface ISchedule {
    classSubjectId: string;
    classSubjectName: string;
    roomId: string;
    date: string;
    startTime: string;
    endTime: string;
    dailyScore: number;
}
export declare class ScheduleDTO implements ISchedule {
    classSubjectId: string;
    classSubjectName: string;
    roomId: string;
    date: string;
    startTime: string;
    endTime: string;
    dailyScore: number;
}
