import { ScheduleService } from './schedule.service';
import { ScheduleDTO } from './interface/schedule.interface';
import { MonthOfWeek } from './params/monthAndWeek.params';
import { StudentClassSubjectService } from '../student-class-subject/student-class-subject.service';
import { Schedule } from './entities/schedule.entity';
import { Repository } from 'typeorm';
import { StudentClassSubject } from '../student-class-subject/entities/student-class-subject.entity';
import { DateOfWeek } from './params/dateOfWeek.params';
export declare class ScheduleController {
    private scheduleService;
    private studentClassSubjectService;
    private readonly scheduleRepository;
    private readonly studentClassSubjectRepository;
    constructor(scheduleService: ScheduleService, studentClassSubjectService: StudentClassSubjectService, scheduleRepository: Repository<Schedule>, studentClassSubjectRepository: Repository<StudentClassSubject>);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Schedule[];
    }>;
    showAllByClassSubject(id: string, params: DateOfWeek): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: any[];
    }>;
    showAllByClassSubjectNoParam(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        schedule: Schedule[];
    }>;
    showAllByTeacher(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Schedule[];
    }>;
    showAllByTeacherByMonthAndWeek(id: string, params: MonthOfWeek): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: any[];
    }>;
    showScheduleRecent(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: any[];
    }>;
    create(data: ScheduleDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Schedule;
    }>;
    update(id: string, data: ScheduleDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Schedule;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Schedule;
    }>;
}
