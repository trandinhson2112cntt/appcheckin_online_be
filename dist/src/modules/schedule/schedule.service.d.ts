import { HttpStatus } from '@nestjs/common';
import { Schedule } from './entities/schedule.entity';
import { Repository } from 'typeorm';
import { ISchedule } from './interface/schedule.interface';
import { IMonthOfWeek } from './params/monthAndWeek.params';
import { StudentClassSubject } from '../student-class-subject/entities/student-class-subject.entity';
import { IDateOfWeek } from './params/dateOfWeek.params';
export declare class ScheduleService {
    private readonly scheduleRepository;
    private readonly studentClassSubjectRepository;
    constructor(scheduleRepository: Repository<Schedule>, studentClassSubjectRepository: Repository<StudentClassSubject>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Schedule[];
    }>;
    showAllByTeacherID(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Schedule[];
    }>;
    showAllByTeacherByMonthAndWeek(id: string, params: IMonthOfWeek): Promise<{
        status: HttpStatus;
        message: string;
        data: any[];
    }>;
    showScheduleByClassSubjectNoParams(id: string): Promise<{
        status: HttpStatus;
        message: string;
        schedule: Schedule[];
    }>;
    showScheduleByClassSubject(id: string, params: IDateOfWeek): Promise<{
        status: HttpStatus;
        message: string;
        data: any[];
    }>;
    showScheduleRecent(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: any[];
    }>;
    create(data: ISchedule): Promise<{
        status: HttpStatus;
        message: string;
        data: Schedule;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Schedule;
    }>;
    update(id: string, data: Partial<ISchedule>): Promise<{
        status: HttpStatus;
        message: string;
        data: Schedule;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Schedule;
    }>;
}
