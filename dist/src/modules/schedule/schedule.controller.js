"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const validation_pipe_1 = require("../../pipes/validation.pipe");
const swagger_1 = require("@nestjs/swagger");
const schedule_service_1 = require("./schedule.service");
const passport_1 = require("@nestjs/passport");
const schedule_interface_1 = require("./interface/schedule.interface");
const monthAndWeek_params_1 = require("./params/monthAndWeek.params");
const student_class_subject_service_1 = require("../student-class-subject/student-class-subject.service");
const typeorm_1 = require("@nestjs/typeorm");
const schedule_entity_1 = require("./entities/schedule.entity");
const typeorm_2 = require("typeorm");
const student_class_subject_entity_1 = require("../student-class-subject/entities/student-class-subject.entity");
const dateOfWeek_params_1 = require("./params/dateOfWeek.params");
let ScheduleController = class ScheduleController {
    constructor(scheduleService, studentClassSubjectService, scheduleRepository, studentClassSubjectRepository) {
        this.scheduleService = scheduleService;
        this.studentClassSubjectService = studentClassSubjectService;
        this.scheduleRepository = scheduleRepository;
        this.studentClassSubjectRepository = studentClassSubjectRepository;
    }
    showAll() {
        return this.scheduleService.showAll();
    }
    showAllByClassSubject(id, params) {
        return this.scheduleService.showScheduleByClassSubject(id, params);
    }
    showAllByClassSubjectNoParam(id) {
        return this.scheduleService.showScheduleByClassSubjectNoParams(id);
    }
    showAllByTeacher(id) {
        return this.scheduleService.showAllByTeacherID(id);
    }
    showAllByTeacherByMonthAndWeek(id, params) {
        return this.scheduleService.showAllByTeacherByMonthAndWeek(id, params);
    }
    showScheduleRecent(id) {
        return this.scheduleService.showScheduleRecent(id);
    }
    create(data) {
        return this.scheduleService.create(data);
    }
    update(id, data) {
        return this.scheduleService.update(id, data);
    }
    destroy(id) {
        return this.scheduleService.destroy(id);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "showAll", null);
__decorate([
    common_1.Post('by-class-subject-student:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, dateOfWeek_params_1.DateOfWeek]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "showAllByClassSubject", null);
__decorate([
    common_1.Get('by-class-subject:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "showAllByClassSubjectNoParam", null);
__decorate([
    common_1.Get('by-teacher:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "showAllByTeacher", null);
__decorate([
    common_1.Post('by-teacher-month-week:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, monthAndWeek_params_1.MonthOfWeek]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "showAllByTeacherByMonthAndWeek", null);
__decorate([
    common_1.Get('get-recent-schedule:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "showScheduleRecent", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [schedule_interface_1.ScheduleDTO]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, schedule_interface_1.ScheduleDTO]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ScheduleController.prototype, "destroy", null);
ScheduleController = __decorate([
    common_1.Controller('schedule'),
    swagger_1.ApiUseTags('Schedule'),
    swagger_1.ApiBearerAuth(),
    __param(2, typeorm_1.InjectRepository(schedule_entity_1.Schedule)),
    __param(3, typeorm_1.InjectRepository(student_class_subject_entity_1.StudentClassSubject)),
    __metadata("design:paramtypes", [schedule_service_1.ScheduleService,
        student_class_subject_service_1.StudentClassSubjectService,
        typeorm_2.Repository,
        typeorm_2.Repository])
], ScheduleController);
exports.ScheduleController = ScheduleController;
//# sourceMappingURL=schedule.controller.js.map