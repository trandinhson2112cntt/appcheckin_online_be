import { HttpStatus } from '@nestjs/common';
import { Absent } from './entities/absent.entity';
import { Repository } from 'typeorm';
import { IAbsent, AbsentDTO } from './interface/absent.interface';
export declare class AbsentService {
    private absentRepository;
    constructor(absentRepository: Repository<Absent>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Absent[];
    }>;
    create(data: IAbsent): Promise<{
        status: HttpStatus;
        message: string;
        data: Absent;
    }>;
    update(id: string, data: Partial<AbsentDTO>): Promise<{
        status: HttpStatus;
        message: string;
        data: Absent;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Absent;
    }>;
    getListStudentAbsentByScheduleId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Absent[];
    }>;
    destroyByScheduleIdAndStudentClassSubjectId(data: any): Promise<{
        status: HttpStatus;
        message: string;
        data: Absent;
    }>;
}
