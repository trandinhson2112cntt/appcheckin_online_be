import { AbsentService } from './absent.service';
import { AbsentDTO } from './interface/absent.interface';
export declare class AbsentController {
    private absentService;
    constructor(absentService: AbsentService);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/absent.entity").Absent[];
    }>;
    create(data: AbsentDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/absent.entity").Absent;
    }>;
    update(id: string, data: AbsentDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/absent.entity").Absent;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/absent.entity").Absent;
    }>;
    getListStudentAbsentByScheduleId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/absent.entity").Absent[];
    }>;
    destroyByScheduleIdAndStudentClassSubjectId(data: any): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/absent.entity").Absent;
    }>;
}
