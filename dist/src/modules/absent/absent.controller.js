"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const absent_service_1 = require("./absent.service");
const passport_1 = require("@nestjs/passport");
const validation_pipe_1 = require("../../pipes/validation.pipe");
const absent_interface_1 = require("./interface/absent.interface");
let AbsentController = class AbsentController {
    constructor(absentService) {
        this.absentService = absentService;
    }
    showAll() {
        return this.absentService.showAll();
    }
    create(data) {
        return this.absentService.create(data);
    }
    update(id, data) {
        return this.absentService.update(id, data);
    }
    destroy(id) {
        return this.absentService.destroy(id);
    }
    getListStudentAbsentByScheduleId(id) {
        return this.absentService.getListStudentAbsentByScheduleId(id);
    }
    destroyByScheduleIdAndStudentClassSubjectId(data) {
        return this.absentService.destroyByScheduleIdAndStudentClassSubjectId(data);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], AbsentController.prototype, "showAll", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [absent_interface_1.AbsentDTO]),
    __metadata("design:returntype", void 0)
], AbsentController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, absent_interface_1.AbsentDTO]),
    __metadata("design:returntype", void 0)
], AbsentController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AbsentController.prototype, "destroy", null);
__decorate([
    common_1.Get('List-Student-Absent-By-ScheduleId:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], AbsentController.prototype, "getListStudentAbsentByScheduleId", null);
__decorate([
    common_1.Post('Schedule-Id-And-Student-Class-Subject-Id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], AbsentController.prototype, "destroyByScheduleIdAndStudentClassSubjectId", null);
AbsentController = __decorate([
    common_1.Controller('absent'),
    swagger_1.ApiUseTags('Absent'),
    swagger_1.ApiBearerAuth(),
    __metadata("design:paramtypes", [absent_service_1.AbsentService])
], AbsentController);
exports.AbsentController = AbsentController;
//# sourceMappingURL=absent.controller.js.map