"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const absent_entity_1 = require("./entities/absent.entity");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let AbsentService = class AbsentService {
    constructor(absentRepository) {
        this.absentRepository = absentRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const absent = yield this.absentRepository
                .createQueryBuilder('absent')
                .leftJoinAndSelect('absent.schedule', 'schedule')
                .leftJoinAndSelect('absent.studentClassSubject', 'studentClassSubject')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Succsess',
                data: absent,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findAbsent = yield this.absentRepository.findOne({ where: { scheduleId: data.scheduleId, scheduleDate: data.scheduleDate, studentClassSubjectId: data.studentClassSubjectId, studentClassSubjectName: data.studentClassSubjectName } });
            if (findAbsent) {
                throw new common_1.HttpException('Student absent existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const absent = yield this.absentRepository.create(data);
            yield this.absentRepository.save(absent);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: absent,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findAbsent = yield this.absentRepository.findOne({ where: { id } });
            if (!findAbsent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.absentRepository.update({ id }, data);
            findAbsent = yield this.absentRepository
                .createQueryBuilder('absent')
                .leftJoinAndSelect('absent.schedule', 'schedule')
                .leftJoinAndSelect('absent.studentClassSubject', 'studentClassSubject')
                .where('absent.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findAbsent,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findAbsent = yield this.absentRepository.findOne({ where: { id } });
            if (!findAbsent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.absentRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Succsess',
                data: findAbsent,
            };
        });
    }
    getListStudentAbsentByScheduleId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const absent = yield this.absentRepository
                .createQueryBuilder('absent')
                .where('absent.scheduleId = :id', { id })
                .leftJoinAndSelect('absent.schedule', 'schedule', 'schedule.Id = :id', { id })
                .leftJoinAndSelect('absent.studentClassSubject', 'studentClassSubject')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: absent,
            };
        });
    }
    destroyByScheduleIdAndStudentClassSubjectId(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findAbsent = yield this.absentRepository.findOne({ where: { scheduleId: data.scheduleId, studentClassSubjectId: data.studentClassSubjectId } });
            if (!findAbsent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.absentRepository.delete({ id: findAbsent.id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Succsess',
                data: findAbsent,
            };
        });
    }
};
AbsentService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(absent_entity_1.Absent)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], AbsentService);
exports.AbsentService = AbsentService;
//# sourceMappingURL=absent.service.js.map