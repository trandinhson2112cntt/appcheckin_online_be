export interface IAbsent {
    scheduleId: string;
    scheduleDate: string;
    studentClassSubjectId: string;
    studentClassSubjectName: string;
}
export declare class AbsentDTO implements IAbsent {
    scheduleId: string;
    scheduleDate: string;
    studentClassSubjectId: string;
    studentClassSubjectName: string;
}
