import { HttpStatus } from '@nestjs/common';
import { Class } from './entities/class.entity';
import { Repository } from 'typeorm';
import { IPaging } from '../../common/ParamPaging';
import { IClass } from './interface/class.interface';
export declare class ClassService {
    private classRepository;
    constructor(classRepository: Repository<Class>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Class[];
    }>;
    showByMajorId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Class[];
    }>;
    showClassPaging(paging: IPaging): Promise<{
        status: HttpStatus;
        message: string;
        data: Class[];
    }>;
    create(data: IClass): Promise<{
        status: HttpStatus;
        message: string;
        data: Class;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Class;
    }>;
    update(id: string, data: Partial<IClass>): Promise<{
        status: HttpStatus;
        message: string;
        data: Class;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Class;
    }>;
}
