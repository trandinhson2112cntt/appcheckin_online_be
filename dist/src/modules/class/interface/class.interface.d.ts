export interface IClass {
    name: string;
    majorId: string;
    majorName: string;
}
export declare class ClassDTO implements IClass {
    name: string;
    majorId: string;
    majorName: string;
}
