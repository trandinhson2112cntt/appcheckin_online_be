"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const class_service_1 = require("./class.service");
const swagger_1 = require("@nestjs/swagger");
const passport_1 = require("@nestjs/passport");
const class_interface_1 = require("./interface/class.interface");
const ParamPaging_1 = require("../../common/ParamPaging");
let ClassController = class ClassController {
    constructor(classService) {
        this.classService = classService;
    }
    showAllClass() {
        return this.classService.showAll();
    }
    showAllClassByMajorId(id) {
        return this.classService.showByMajorId(id);
    }
    read(id) {
        return this.classService.read(id);
    }
    showClassPaging(paging) {
        return this.classService.showClassPaging(paging);
    }
    createClass(data) {
        return this.classService.create(data);
    }
    updateClass(id, data) {
        return this.classService.update(id, data);
    }
    destroyClass(id) {
        return this.classService.destroy(id);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ClassController.prototype, "showAllClass", null);
__decorate([
    common_1.Get('by-majorid:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ClassController.prototype, "showAllClassByMajorId", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ClassController.prototype, "read", null);
__decorate([
    common_1.Post('class-paging'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [ParamPaging_1.Paging]),
    __metadata("design:returntype", void 0)
], ClassController.prototype, "showClassPaging", null);
__decorate([
    common_1.Post('create'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [class_interface_1.ClassDTO]),
    __metadata("design:returntype", void 0)
], ClassController.prototype, "createClass", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new common_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, class_interface_1.ClassDTO]),
    __metadata("design:returntype", void 0)
], ClassController.prototype, "updateClass", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], ClassController.prototype, "destroyClass", null);
ClassController = __decorate([
    common_1.Controller('class'),
    swagger_1.ApiUseTags('Class'),
    swagger_1.ApiBearerAuth(),
    __metadata("design:paramtypes", [class_service_1.ClassService])
], ClassController);
exports.ClassController = ClassController;
//# sourceMappingURL=class.controller.js.map