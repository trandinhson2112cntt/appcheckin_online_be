import Major from '../../major/entities/major.entity';
import { Student } from '../../student/entities/student.entity';
export declare class Class {
    id: string;
    name: string;
    major: Major;
    majorId: string;
    majorName: string;
    students: Student[];
}
