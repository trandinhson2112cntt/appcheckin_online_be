import { ClassService } from './class.service';
import { ClassDTO } from './interface/class.interface';
import { Paging } from '../../common/ParamPaging';
export declare class ClassController {
    private classService;
    constructor(classService: ClassService);
    showAllClass(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class.entity").Class[];
    }>;
    showAllClassByMajorId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class.entity").Class[];
    }>;
    read(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class.entity").Class;
    }>;
    showClassPaging(paging: Paging): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class.entity").Class[];
    }>;
    createClass(data: ClassDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class.entity").Class;
    }>;
    updateClass(id: string, data: ClassDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class.entity").Class;
    }>;
    destroyClass(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/class.entity").Class;
    }>;
}
