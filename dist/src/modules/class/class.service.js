"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const class_entity_1 = require("./entities/class.entity");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let ClassService = class ClassService {
    constructor(classRepository) {
        this.classRepository = classRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.classRepository.find();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data,
            };
        });
    }
    showByMajorId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const data = yield this.classRepository.find({ where: { majorId: id } });
            if (data.length === 0) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data,
            };
        });
    }
    showClassPaging(paging) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield this.classRepository.find({ take: paging.take, skip: paging.skip });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findClass = yield this.classRepository.findOne({ where: { name: data.name } });
            if (findClass) {
                throw new common_1.HttpException('Class name existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const classRes = yield this.classRepository.create(data);
            yield this.classRepository.save(classRes);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: classRes,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findClass = yield this.classRepository.findOne({ where: { id } });
            if (!findClass) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClass,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findClass = yield this.classRepository.findOne({ where: { id } });
            if (!findClass) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.classRepository.update({ id }, data);
            findClass = yield this.classRepository.findOne({ where: { id } });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClass,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findClass = yield this.classRepository.findOne({ where: { id } });
            if (!findClass) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.classRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClass,
            };
        });
    }
};
ClassService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(class_entity_1.Class)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], ClassService);
exports.ClassService = ClassService;
//# sourceMappingURL=class.service.js.map