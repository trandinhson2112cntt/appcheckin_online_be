"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
const notification_entity_1 = require("./entities/notification.entity");
const moment = require("moment");
let NotificationService = class NotificationService {
    constructor(notificationRepository) {
        this.notificationRepository = notificationRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const notification = yield this.notificationRepository
                .createQueryBuilder('notification')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: notification,
            };
        });
    }
    showListByClassSubjectId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const notification = yield this.notificationRepository
                .createQueryBuilder('notification')
                .where('notification.classSubjectId = :id', { id })
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: notification,
            };
        });
    }
    showListByStudentClassSubjectId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const notification = yield this.notificationRepository
                .createQueryBuilder('notification')
                .where('notification.studentClassSubjectId = :id', { id })
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: notification,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findnotification = yield this.notificationRepository
                .findOne({ where: { message: data.message, title: data.title } });
            if (findnotification) {
                throw new common_1.HttpException('Notifi existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            data.createdDate = moment.now();
            const notification = yield this.notificationRepository.create(data);
            yield this.notificationRepository.save(notification);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: notification,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findnotification = yield this.notificationRepository
                .createQueryBuilder('notification')
                .leftJoinAndSelect('notification.classSubject', 'classSubject')
                .leftJoinAndSelect('notification.studentClassSubject', 'studentClassSubject')
                .where('notification.id = :id', { id })
                .getOne();
            if (!findnotification) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findnotification,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findClassSubject = yield this.notificationRepository.findOne({ where: { id } });
            if (!findClassSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.notificationRepository.update({ id }, data);
            findClassSubject = yield this.notificationRepository
                .createQueryBuilder('notification')
                .leftJoinAndSelect('notification.classSubject', 'classSubject')
                .leftJoinAndSelect('notification.studentClassSubject', 'studentClassSubject')
                .where('notification.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClassSubject,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findClassSubject = yield this.notificationRepository.findOne({ where: { id } });
            if (!findClassSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.notificationRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findClassSubject,
            };
        });
    }
};
NotificationService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(notification_entity_1.default)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], NotificationService);
exports.NotificationService = NotificationService;
//# sourceMappingURL=notification.service.js.map