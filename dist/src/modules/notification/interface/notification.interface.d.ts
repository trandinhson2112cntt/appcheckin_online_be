export interface INotification {
    title: string;
    message: string;
    createdDate: number;
    studentClassSubjectId: string;
    studentName: string;
    classSubjectId: string;
    classSubjectName: string;
}
export declare class NotificationDTO implements INotification {
    title: string;
    message: string;
    createdDate: number;
    studentClassSubjectId: string;
    studentName: string;
    classSubjectId: string;
    classSubjectName: string;
}
