import { NotificationService } from './notification.service';
import { NotificationDTO } from './interface/notification.interface';
export declare class NotificationController {
    private notificationService;
    constructor(notificationService: NotificationService);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/notification.entity").default[];
    }>;
    read(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/notification.entity").default;
    }>;
    showListByClassSubjectId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/notification.entity").default[];
    }>;
    showListByStudentClassSubjectId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/notification.entity").default[];
    }>;
    create(data: NotificationDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/notification.entity").default;
    }>;
    update(id: string, data: NotificationDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/notification.entity").default;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/notification.entity").default;
    }>;
}
