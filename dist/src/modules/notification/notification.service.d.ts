import { HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import Notification from './entities/notification.entity';
import { INotification } from './interface/notification.interface';
export declare class NotificationService {
    private notificationRepository;
    constructor(notificationRepository: Repository<Notification>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Notification[];
    }>;
    showListByClassSubjectId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Notification[];
    }>;
    showListByStudentClassSubjectId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Notification[];
    }>;
    create(data: INotification): Promise<{
        status: HttpStatus;
        message: string;
        data: Notification;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Notification;
    }>;
    update(id: string, data: Partial<INotification>): Promise<{
        status: HttpStatus;
        message: string;
        data: Notification;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Notification;
    }>;
}
