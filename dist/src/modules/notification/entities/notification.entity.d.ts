export default class Notification {
    id: string;
    title: string;
    message: string;
    createdDate: number;
    studentClassSubjectId: string;
    studentName: string;
    classSubjectId: string;
    classSubjectName: string;
}
