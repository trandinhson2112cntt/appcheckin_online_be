import { RoomService } from './room.service';
import { RoomDTO } from './interface/room.interface';
export declare class RoomController {
    private roomService;
    constructor(roomService: RoomService);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/room.entity").Room[];
    }>;
    create(data: RoomDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/room.entity").Room;
    }>;
    update(id: string, data: RoomDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/room.entity").Room;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/room.entity").Room;
    }>;
}
