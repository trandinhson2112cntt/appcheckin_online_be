export interface IRoom {
    roomName: string;
    floorName: string;
    buildingName: string;
}
export declare class RoomDTO implements IRoom {
    roomName: string;
    floorName: string;
    buildingName: string;
}
