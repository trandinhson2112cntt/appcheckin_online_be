import { HttpStatus } from '@nestjs/common';
import { Room } from './entities/room.entity';
import { IRoom } from './interface/room.interface';
import { Repository } from 'typeorm';
export declare class RoomService {
    private roomRepository;
    constructor(roomRepository: Repository<Room>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Room[];
    }>;
    create(data: IRoom): Promise<{
        status: HttpStatus;
        message: string;
        data: Room;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Room;
    }>;
    update(id: string, data: Partial<IRoom>): Promise<{
        status: HttpStatus;
        message: string;
        data: Room;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Room;
    }>;
}
