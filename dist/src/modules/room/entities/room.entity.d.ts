import { Schedule } from '../../schedule/entities/schedule.entity';
export declare class Room {
    id: string;
    roomName: string;
    floorName: string;
    buildingName: string;
    schedules: Schedule[];
}
