import { SubjectDTO } from './interface/subject.interface';
import { SubjectService } from './subject.service';
export declare class SubjectController {
    private subjectService;
    constructor(subjectService: SubjectService);
    showAll(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/subject.entity").Subject[];
    }>;
    read(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/subject.entity").Subject;
    }>;
    create(data: SubjectDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/subject.entity").Subject;
    }>;
    update(id: string, data: SubjectDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/subject.entity").Subject;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/subject.entity").Subject;
    }>;
}
