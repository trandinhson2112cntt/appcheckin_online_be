"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_subject_entity_1 = require("../../class-subject/entities/class-subject.entity");
let Subject = class Subject {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Subject.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Subject.prototype, "name", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", Number)
], Subject.prototype, "numberOfCredit", void 0);
__decorate([
    typeorm_1.Column('real'),
    __metadata("design:type", Number)
], Subject.prototype, "scorePass", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Subject.prototype, "nameGroup", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Subject.prototype, "type", void 0);
__decorate([
    typeorm_1.OneToMany(type => class_subject_entity_1.ClassSubject, (c) => c.teacher),
    __metadata("design:type", Array)
], Subject.prototype, "classSubjects", void 0);
Subject = __decorate([
    typeorm_1.Entity()
], Subject);
exports.Subject = Subject;
//# sourceMappingURL=subject.entity.js.map