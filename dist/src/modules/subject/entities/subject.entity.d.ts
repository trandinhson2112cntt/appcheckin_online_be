import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
export declare class Subject {
    id: string;
    name: string;
    numberOfCredit: number;
    scorePass: number;
    nameGroup: string;
    type: string;
    classSubjects: ClassSubject[];
}
