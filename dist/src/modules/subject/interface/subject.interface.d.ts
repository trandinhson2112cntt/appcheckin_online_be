export interface ISubject {
    name: string;
    numberOfCredit: number;
    scorePass: number;
    nameGroup: string;
    type: string;
}
export declare class SubjectDTO implements ISubject {
    name: string;
    numberOfCredit: number;
    scorePass: number;
    nameGroup: string;
    type: string;
}
