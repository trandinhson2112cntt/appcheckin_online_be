"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const subject_entity_1 = require("./entities/subject.entity");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let SubjectService = class SubjectService {
    constructor(subjectRepository) {
        this.subjectRepository = subjectRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const subject = yield this.subjectRepository
                .find();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: subject,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findSubject = yield this.subjectRepository.findOne({ where: { name: data.name, type: data.type } });
            if (findSubject) {
                throw new common_1.HttpException('Subject existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const subject = yield this.subjectRepository.create(data);
            yield this.subjectRepository.save(subject);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: subject,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findSubject = yield this.subjectRepository
                .createQueryBuilder('subject')
                .where('subject.id = :id', { id })
                .getOne();
            if (!findSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSubject,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findSubject = yield this.subjectRepository.findOne({ where: { id } });
            if (!findSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.subjectRepository.update({ id }, data);
            findSubject = yield this.subjectRepository
                .createQueryBuilder('subject')
                .where('subject.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSubject,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findSubject = yield this.subjectRepository.findOne({ where: { id } });
            if (!findSubject) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.subjectRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findSubject,
            };
        });
    }
};
SubjectService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(subject_entity_1.Subject)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], SubjectService);
exports.SubjectService = SubjectService;
//# sourceMappingURL=subject.service.js.map