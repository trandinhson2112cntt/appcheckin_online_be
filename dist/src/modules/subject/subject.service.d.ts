import { HttpStatus } from '@nestjs/common';
import { Subject } from './entities/subject.entity';
import { Repository } from 'typeorm';
import { ISubject } from './interface/subject.interface';
export declare class SubjectService {
    private subjectRepository;
    constructor(subjectRepository: Repository<Subject>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Subject[];
    }>;
    create(data: ISubject): Promise<{
        status: HttpStatus;
        message: string;
        data: Subject;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Subject;
    }>;
    update(id: string, data: Partial<ISubject>): Promise<{
        status: HttpStatus;
        message: string;
        data: Subject;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Subject;
    }>;
}
