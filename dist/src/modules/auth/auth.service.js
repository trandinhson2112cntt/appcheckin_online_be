"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const jwt_1 = require("@nestjs/jwt");
const user_service_1 = require("../user/user.service");
const Admin = require("firebase-admin");
const serviceAccount = require('../../../appmuster-c5b80-firebase-adminsdk-7tkh8-b9bcf23076.json');
Admin.initializeApp({
    credential: Admin.credential.cert(serviceAccount),
    databaseURL: 'https://appmuster-c5b80.firebaseio.com',
});
let AuthService = class AuthService {
    constructor(jwtService, userService) {
        this.jwtService = jwtService;
        this.userService = userService;
    }
    createToken(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const accessToken = this.jwtService.sign(payload);
            return {
                expiresIn: process.env.EXPIRES_IN,
                accessToken,
            };
        });
    }
    login(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const token = yield this.createToken(payload);
            const user = yield this.userService.login(payload);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: {
                    token,
                    user,
                },
            };
        });
    }
    validateUser(payload) {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.userService.login(payload);
            return {
                user,
            };
        });
    }
    sendNotification(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const registrationTokens = data.listToken;
            const token = 'cR4uSXJ-Xqk:APA91bEBoUrfTKUqw4KIv1GP2dNysi_1Wj9l9xxMbAqGPbWWtsKvt2LCLVHDaaq2q58JuU1yuFRc7NFfAnUE41PwTzxF0BxicsOpUKsPWbfD0Aq_FyzVGg-oX-srdbV60LZ-LcOGm70Q';
            registrationTokens.push(token);
            const message = {
                notification: {
                    title: data.title,
                    body: data.message,
                },
            };
            const options = {};
            return yield Admin.messaging().sendToDevice(registrationTokens, message, options);
        });
    }
};
AuthService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        user_service_1.UserService])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map