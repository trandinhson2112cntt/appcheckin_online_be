import { AuthService } from './auth.service';
import { UserDTO } from '../user/user.dto';
import { INotiFirebase } from './interfaces/notify.interface';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    login(data: UserDTO): Promise<any>;
    sendNotificationa(data: INotiFirebase): Promise<import("firebase-admin").messaging.MessagingDevicesResponse>;
}
