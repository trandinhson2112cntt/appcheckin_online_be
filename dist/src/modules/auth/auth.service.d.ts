import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { UserService } from '../user/user.service';
import * as Admin from 'firebase-admin';
import { INotiFirebase } from './interfaces/notify.interface';
export declare class AuthService {
    private readonly jwtService;
    private readonly userService;
    constructor(jwtService: JwtService, userService: UserService);
    createToken(payload: JwtPayload): Promise<any>;
    login(payload: JwtPayload): Promise<any>;
    validateUser(payload: JwtPayload): Promise<any>;
    sendNotification(data: INotiFirebase): Promise<Admin.messaging.MessagingDevicesResponse>;
}
