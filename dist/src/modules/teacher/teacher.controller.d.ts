import { TeacherService } from './teacher.service';
import { Paging } from '../../common/ParamPaging';
import { TeacherDTO } from './interface/teacher.interface';
export declare class TeacherController {
    private teachertService;
    constructor(teachertService: TeacherService);
    showAllTeachert(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/teacher.entity").Teacher[];
    }>;
    showByMajorId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/teacher.entity").Teacher[];
    }>;
    showByMajorIdPaging(id: string, paging: Paging): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        take: number;
        skip: number;
        data: import("./entities/teacher.entity").Teacher[];
    }>;
    create(data: TeacherDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/teacher.entity").Teacher;
    }>;
    update(id: string, data: TeacherDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/teacher.entity").Teacher;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/teacher.entity").Teacher;
    }>;
}
