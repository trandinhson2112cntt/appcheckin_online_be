import { User } from '../../user/entities/user.entity';
export interface ITeacher {
    level: string;
    majorId: string;
    majorName: string;
    userId: string;
    userName: string;
    user?: User;
}
export declare class TeacherDTO implements ITeacher {
    level: string;
    majorId: string;
    majorName: string;
    userId: string;
    userName: string;
    user?: User;
}
