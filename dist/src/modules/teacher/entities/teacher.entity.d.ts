import { User } from '../../user/entities/user.entity';
import Major from '../../major/entities/major.entity';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
export declare class Teacher {
    id: string;
    level: string;
    major: Major;
    majorId: string;
    majorName: string;
    userId: string;
    userName: string;
    classSubjects: ClassSubject[];
    user: User;
}
