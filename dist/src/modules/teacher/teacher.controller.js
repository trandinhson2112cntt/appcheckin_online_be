"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const swagger_1 = require("@nestjs/swagger");
const teacher_service_1 = require("./teacher.service");
const passport_1 = require("@nestjs/passport");
const ParamPaging_1 = require("../../common/ParamPaging");
const validation_pipe_1 = require("../../pipes/validation.pipe");
const teacher_interface_1 = require("./interface/teacher.interface");
let TeacherController = class TeacherController {
    constructor(teachertService) {
        this.teachertService = teachertService;
    }
    showAllTeachert() {
        return this.teachertService.showAll();
    }
    showByMajorId(id) {
        return this.teachertService.showByMajorId(id);
    }
    showByMajorIdPaging(id, paging) {
        return this.teachertService.showByMajorIdPaging(id, paging);
    }
    create(data) {
        return this.teachertService.create(data);
    }
    update(id, data) {
        return this.teachertService.update(id, data);
    }
    destroy(id) {
        return this.teachertService.destroy(id);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], TeacherController.prototype, "showAllTeachert", null);
__decorate([
    common_1.Get('by-major:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], TeacherController.prototype, "showByMajorId", null);
__decorate([
    common_1.Post('by-major-paging:id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, ParamPaging_1.Paging]),
    __metadata("design:returntype", void 0)
], TeacherController.prototype, "showByMajorIdPaging", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [teacher_interface_1.TeacherDTO]),
    __metadata("design:returntype", void 0)
], TeacherController.prototype, "create", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, teacher_interface_1.TeacherDTO]),
    __metadata("design:returntype", void 0)
], TeacherController.prototype, "update", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], TeacherController.prototype, "destroy", null);
TeacherController = __decorate([
    common_1.Controller('teacher'),
    swagger_1.ApiUseTags('Teacher'),
    swagger_1.ApiBearerAuth(),
    __metadata("design:paramtypes", [teacher_service_1.TeacherService])
], TeacherController);
exports.TeacherController = TeacherController;
//# sourceMappingURL=teacher.controller.js.map