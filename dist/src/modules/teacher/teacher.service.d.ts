import { HttpStatus } from '@nestjs/common';
import { Teacher } from './entities/teacher.entity';
import { Repository } from 'typeorm';
import { IPaging } from '../../common/ParamPaging';
import { ITeacher } from './interface/teacher.interface';
export declare class TeacherService {
    private teacherRepository;
    constructor(teacherRepository: Repository<Teacher>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Teacher[];
    }>;
    showByMajorId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Teacher[];
    }>;
    showByMajorIdPaging(id: string, paging: IPaging): Promise<{
        status: HttpStatus;
        message: string;
        take: number;
        skip: number;
        data: Teacher[];
    }>;
    create(data: ITeacher): Promise<{
        status: HttpStatus;
        message: string;
        data: Teacher;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Teacher;
    }>;
    update(id: string, data: Partial<ITeacher>): Promise<{
        status: HttpStatus;
        message: string;
        data: Teacher;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Teacher;
    }>;
}
