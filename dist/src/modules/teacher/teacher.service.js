"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const teacher_entity_1 = require("./entities/teacher.entity");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let TeacherService = class TeacherService {
    constructor(teacherRepository) {
        this.teacherRepository = teacherRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const teacher = yield this.teacherRepository
                .createQueryBuilder('teacher')
                .leftJoinAndSelect('teacher.major', 'major')
                .leftJoinAndSelect('teacher.user', 'user')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: teacher,
            };
        });
    }
    showByMajorId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const teacher = yield this.teacherRepository
                .createQueryBuilder('teacher')
                .leftJoinAndSelect('teacher.major', 'major')
                .leftJoinAndSelect('teacher.user', 'user')
                .where('teacher.majorId = :id', { id })
                .getMany();
            if (teacher.length === 0 || !teacher) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: teacher,
            };
        });
    }
    showByMajorIdPaging(id, paging) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const data = yield this.teacherRepository
                .createQueryBuilder('teacher')
                .leftJoinAndSelect('teacher.major', 'major')
                .leftJoinAndSelect('teacher.user', 'user')
                .where('teacher.majorId = :id', { id })
                .skip(paging.skip)
                .take(paging.take)
                .getMany();
            if (data.length === 0 || !data) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                take: paging.take,
                skip: paging.skip,
                data,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findTeacher = yield this.teacherRepository.findOne({ where: { userId: data.userId } });
            if (findTeacher) {
                throw new common_1.HttpException('Teacher existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const teacher = yield this.teacherRepository.create(data);
            yield this.teacherRepository.save(teacher);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: teacher,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findTeacher = yield this.teacherRepository
                .createQueryBuilder('teacher')
                .leftJoinAndSelect('teacher.major', 'major')
                .leftJoinAndSelect('teacher.user', 'user')
                .where('teacher.id = :id', { id })
                .getOne();
            if (!findTeacher) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findTeacher,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findTeacher = yield this.teacherRepository.findOne({ where: { id } });
            if (!findTeacher) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.teacherRepository.update({ id }, data);
            findTeacher = yield this.teacherRepository
                .createQueryBuilder('teacher')
                .leftJoinAndSelect('teacher.major', 'major')
                .leftJoinAndSelect('teacher.user', 'user')
                .where('teacher.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findTeacher,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findTeacher = yield this.teacherRepository.findOne({ where: { id } });
            if (!findTeacher) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.teacherRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findTeacher,
            };
        });
    }
};
TeacherService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(teacher_entity_1.Teacher)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], TeacherService);
exports.TeacherService = TeacherService;
//# sourceMappingURL=teacher.service.js.map