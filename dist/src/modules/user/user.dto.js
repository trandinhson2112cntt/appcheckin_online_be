"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const swagger_1 = require("@nestjs/swagger");
class UserDTO {
}
__decorate([
    swagger_1.ApiModelProperty({ required: true }),
    __metadata("design:type", String)
], UserDTO.prototype, "code", void 0);
__decorate([
    swagger_1.ApiModelProperty({ required: true, minLength: 6, type: String, format: 'password' }),
    __metadata("design:type", String)
], UserDTO.prototype, "password", void 0);
exports.UserDTO = UserDTO;
class UserRO {
}
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], UserRO.prototype, "code", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], UserRO.prototype, "password", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], UserRO.prototype, "name", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], UserRO.prototype, "address", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], UserRO.prototype, "phonenumber", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional({ type: String }),
    __metadata("design:type", Date)
], UserRO.prototype, "birthday", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], UserRO.prototype, "typeUser", void 0);
__decorate([
    swagger_1.ApiModelPropertyOptional(),
    __metadata("design:type", String)
], UserRO.prototype, "avatar", void 0);
exports.UserRO = UserRO;
//# sourceMappingURL=user.dto.js.map