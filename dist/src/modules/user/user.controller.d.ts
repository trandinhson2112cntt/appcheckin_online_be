import { UserService } from './user.service';
import { UserRO } from './user.dto';
export declare class UserController {
    private userService;
    constructor(userService: UserService);
    showAllUser(): Promise<UserRO[]>;
    register(data: UserRO): Promise<UserRO>;
}
