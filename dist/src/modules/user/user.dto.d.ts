export declare class UserDTO {
    code: string;
    password: string;
}
export declare class UserRO {
    id: string;
    code: string;
    password: string;
    name: string;
    address: string;
    phonenumber: string;
    birthday: Date;
    typeUser: string;
    avatar: string;
    token?: string;
}
