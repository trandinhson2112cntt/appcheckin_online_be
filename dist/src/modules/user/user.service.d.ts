import { User } from './entities/user.entity';
import { Repository } from 'typeorm';
import { UserDTO, UserRO } from './user.dto';
export declare class UserService {
    private userRepository;
    constructor(userRepository: Repository<User>);
    showAll(): Promise<UserRO[]>;
    login(data: UserDTO): Promise<UserRO>;
    register(data: UserRO): Promise<UserRO>;
}
