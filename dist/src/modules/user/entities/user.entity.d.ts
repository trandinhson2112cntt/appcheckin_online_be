import { UserRO } from '../user.dto';
export declare class User {
    id: string;
    name: string;
    address: string;
    phonenumber: string;
    birthday: Date;
    code: string;
    password: string;
    avatar: string;
    typeUser: string;
    hashPassword(): Promise<void>;
    toResponseObject(showToken?: boolean): UserRO;
    comparePassword(attemp: string): Promise<boolean>;
}
