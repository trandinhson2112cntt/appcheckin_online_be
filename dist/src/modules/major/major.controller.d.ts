import { MajorService } from './major.service';
import { MajorDTO } from './interface/major.interface';
import Major from './entities/major.entity';
import { Paging } from '../../common/ParamPaging';
export declare class MajorController {
    private majorService;
    private logger;
    constructor(majorService: MajorService);
    showAllMajor(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Major[];
    }>;
    showMajorPaging(paging: Paging): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Major[];
    }>;
    createMajor(data: MajorDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Major;
    }>;
    readMajor(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Major;
    }>;
    updateMajor(id: string, data: MajorDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Major;
    }>;
    destroyMajor(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: Major;
    }>;
}
