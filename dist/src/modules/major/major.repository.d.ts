import { Repository } from 'typeorm';
import Major from './entities/major.entity';
import { Class } from '../class/entities/class.entity';
import { Teacher } from '../teacher/entities/teacher.entity';
export declare class MajorRepository extends Repository<Major> {
    id: string;
    name: string;
    classes: Class[];
    teachers: Teacher[];
}
