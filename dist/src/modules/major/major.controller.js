"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const major_service_1 = require("./major.service");
const major_interface_1 = require("./interface/major.interface");
const validation_pipe_1 = require("../../pipes/validation.pipe");
const passport_1 = require("@nestjs/passport");
const swagger_1 = require("@nestjs/swagger");
const ParamPaging_1 = require("../../common/ParamPaging");
let MajorController = class MajorController {
    constructor(majorService) {
        this.majorService = majorService;
        this.logger = new common_1.Logger('MajorController');
    }
    showAllMajor() {
        return this.majorService.showAll();
    }
    showMajorPaging(paging) {
        return this.majorService.showMajorPaging(paging);
    }
    createMajor(data) {
        this.logger.log(JSON.stringify(data));
        return this.majorService.create(data);
    }
    readMajor(id) {
        return this.majorService.read(id);
    }
    updateMajor(id, data) {
        return this.majorService.update(id, data);
    }
    destroyMajor(id) {
        return this.majorService.destroy(id);
    }
};
__decorate([
    common_1.Get(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], MajorController.prototype, "showAllMajor", null);
__decorate([
    common_1.Post('/majorpaging'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [ParamPaging_1.Paging]),
    __metadata("design:returntype", void 0)
], MajorController.prototype, "showMajorPaging", null);
__decorate([
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [major_interface_1.MajorDTO]),
    __metadata("design:returntype", void 0)
], MajorController.prototype, "createMajor", null);
__decorate([
    common_1.Get(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], MajorController.prototype, "readMajor", null);
__decorate([
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    common_1.UsePipes(new validation_pipe_1.ValidationPipe()),
    __param(0, common_1.Param('id')), __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, major_interface_1.MajorDTO]),
    __metadata("design:returntype", void 0)
], MajorController.prototype, "updateMajor", null);
__decorate([
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], MajorController.prototype, "destroyMajor", null);
MajorController = __decorate([
    common_1.Controller('major'),
    swagger_1.ApiUseTags('Major'),
    swagger_1.ApiBearerAuth(),
    __metadata("design:paramtypes", [major_service_1.MajorService])
], MajorController);
exports.MajorController = MajorController;
//# sourceMappingURL=major.controller.js.map