"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("typeorm");
const major_entity_1 = require("./entities/major.entity");
const typeorm_2 = require("@nestjs/typeorm");
let MajorService = class MajorService {
    constructor(majorRepository) {
        this.majorRepository = majorRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const major = yield this.majorRepository.find();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: major,
            };
        });
    }
    showMajorPaging(paging) {
        return __awaiter(this, void 0, void 0, function* () {
            const major = yield this.majorRepository.find({ take: paging.take, skip: paging.skip });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: major,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const major = yield this.majorRepository.create(data);
            yield this.majorRepository.save(major);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: major,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const major = yield this.majorRepository.findOne({ where: { id } });
            if (!major) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: major,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            let major = yield this.majorRepository.findOne({ where: { id } });
            if (!major) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.majorRepository.update({ id }, data);
            major = yield this.majorRepository.findOne({ where: { id } });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: major,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const major = yield this.majorRepository.findOne({ where: { id } });
            if (!major) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.majorRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: major,
            };
        });
    }
};
MajorService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_2.InjectRepository(major_entity_1.default)),
    __metadata("design:paramtypes", [typeorm_1.Repository])
], MajorService);
exports.MajorService = MajorService;
//# sourceMappingURL=major.service.js.map