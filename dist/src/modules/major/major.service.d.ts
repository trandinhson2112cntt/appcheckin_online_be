import { HttpStatus } from '@nestjs/common';
import { Repository } from 'typeorm';
import Major from './entities/major.entity';
import { IMajor } from './interface/major.interface';
import { IPaging } from '../../common/ParamPaging';
export declare class MajorService {
    private majorRepository;
    constructor(majorRepository: Repository<Major>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Major[];
    }>;
    showMajorPaging(paging: IPaging): Promise<{
        status: HttpStatus;
        message: string;
        data: Major[];
    }>;
    create(data: IMajor): Promise<{
        status: HttpStatus;
        message: string;
        data: Major;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Major;
    }>;
    update(id: string, data: Partial<IMajor>): Promise<{
        status: HttpStatus;
        message: string;
        data: Major;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Major;
    }>;
}
