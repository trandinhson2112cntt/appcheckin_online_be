export interface IMajor {
    name: string;
}
export declare class MajorDTO implements IMajor {
    name: string;
}
