"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const class_entity_1 = require("../../class/entities/class.entity");
const teacher_entity_1 = require("../../teacher/entities/teacher.entity");
const class_subject_entity_1 = require("../../class-subject/entities/class-subject.entity");
let Major = class Major {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn('uuid'),
    __metadata("design:type", String)
], Major.prototype, "id", void 0);
__decorate([
    typeorm_1.Column('text'),
    __metadata("design:type", String)
], Major.prototype, "name", void 0);
__decorate([
    typeorm_1.OneToMany(type => class_entity_1.Class, (c) => c.major),
    __metadata("design:type", Array)
], Major.prototype, "classes", void 0);
__decorate([
    typeorm_1.OneToMany(type => teacher_entity_1.Teacher, (t) => t.major),
    __metadata("design:type", Array)
], Major.prototype, "teachers", void 0);
__decorate([
    typeorm_1.OneToMany(type => class_subject_entity_1.ClassSubject, (t) => t.major),
    __metadata("design:type", Array)
], Major.prototype, "classSubjects", void 0);
Major = __decorate([
    typeorm_1.Entity()
], Major);
exports.default = Major;
//# sourceMappingURL=major.entity.js.map