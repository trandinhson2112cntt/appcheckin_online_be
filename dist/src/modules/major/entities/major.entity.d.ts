import { Class } from '../../class/entities/class.entity';
import { Teacher } from '../../teacher/entities/teacher.entity';
import { ClassSubject } from '../../class-subject/entities/class-subject.entity';
export default class Major {
    id: string;
    name: string;
    classes: Class[];
    teachers: Teacher[];
    classSubjects: ClassSubject[];
}
