"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const student_entity_1 = require("./entities/student.entity");
const typeorm_2 = require("typeorm");
const IDisUUID_1 = require("../../common/IDisUUID");
let StudentService = class StudentService {
    constructor(studentRepository) {
        this.studentRepository = studentRepository;
    }
    showAll() {
        return __awaiter(this, void 0, void 0, function* () {
            const student = yield this.studentRepository
                .createQueryBuilder('student')
                .leftJoinAndSelect('student.class', 'class')
                .leftJoinAndSelect('student.user', 'user')
                .getMany();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: student,
            };
        });
    }
    showByClassId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const data = yield this.studentRepository
                .createQueryBuilder('student')
                .leftJoinAndSelect('student.class', 'class')
                .leftJoinAndSelect('student.user', 'user')
                .where('student.classId = :id', { id })
                .getMany();
            if (!data) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data,
            };
        });
    }
    showByUserId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const data = yield this.studentRepository
                .createQueryBuilder('student')
                .leftJoinAndSelect('student.class', 'class')
                .where('student.user.id = :id', { id })
                .getMany();
            if (!data) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data,
            };
        });
    }
    showByClassIdPaging(id, paging) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const data = yield this.studentRepository
                .createQueryBuilder('student')
                .leftJoinAndSelect('student.class', 'class')
                .leftJoinAndSelect('student.user', 'user')
                .where('student.classId = :id', { id })
                .skip(paging.skip)
                .take(paging.take)
                .getMany();
            if (!data) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                take: paging.take,
                skip: paging.skip,
                data,
            };
        });
    }
    create(data) {
        return __awaiter(this, void 0, void 0, function* () {
            const findStudent = yield this.studentRepository.findOne({ where: { userId: data.userId } });
            if (findStudent) {
                throw new common_1.HttpException('Student existed ', common_1.HttpStatus.BAD_REQUEST);
            }
            const student = yield this.studentRepository.create(data);
            yield this.studentRepository.save(student);
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: student,
            };
        });
    }
    read(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findStudent = yield this.studentRepository
                .createQueryBuilder('student')
                .leftJoinAndSelect('student.class', 'class')
                .leftJoinAndSelect('student.user', 'user')
                .where('student.id = :id', { id })
                .getOne();
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
    update(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            let findStudent = yield this.studentRepository.findOne({ where: { id } });
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.studentRepository.update({ id }, data);
            findStudent = yield this.studentRepository
                .createQueryBuilder('student')
                .leftJoinAndSelect('student.class', 'class')
                .leftJoinAndSelect('student.user', 'user')
                .where('student.id = :id', { id })
                .getOne();
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
    destroy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            IDisUUID_1.IDisUUID.checking(id);
            const findStudent = yield this.studentRepository.findOne({ where: { id } });
            if (!findStudent) {
                throw new common_1.HttpException('Not found', common_1.HttpStatus.NOT_FOUND);
            }
            yield this.studentRepository.delete({ id });
            return {
                status: common_1.HttpStatus.OK,
                message: 'Success',
                data: findStudent,
            };
        });
    }
};
StudentService = __decorate([
    common_1.Injectable(),
    __param(0, typeorm_1.InjectRepository(student_entity_1.Student)),
    __metadata("design:paramtypes", [typeorm_2.Repository])
], StudentService);
exports.StudentService = StudentService;
//# sourceMappingURL=student.service.js.map