import { StudentService } from './student.service';
import { StudentDTO } from './interface/student.interface';
import { Paging } from '../../common/ParamPaging';
export declare class StudentController {
    private studentService;
    constructor(studentService: StudentService);
    showAllStudent(): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student.entity").Student[];
    }>;
    showByClassId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student.entity").Student[];
    }>;
    showByUserId(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student.entity").Student[];
    }>;
    showByClassIdPaging(id: string, paging: Paging): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        take: number;
        skip: number;
        data: import("./entities/student.entity").Student[];
    }>;
    create(data: StudentDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student.entity").Student;
    }>;
    update(id: string, data: StudentDTO): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student.entity").Student;
    }>;
    destroy(id: string): Promise<{
        status: import("@nestjs/common").HttpStatus;
        message: string;
        data: import("./entities/student.entity").Student;
    }>;
}
