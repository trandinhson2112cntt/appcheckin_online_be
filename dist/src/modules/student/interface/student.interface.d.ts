import { User } from '../../user/entities/user.entity';
export interface IStudent {
    education: string;
    classId: string;
    className: string;
    userId: string;
    userName: string;
    user?: User;
}
export declare class StudentDTO implements IStudent {
    education: string;
    classId: string;
    className: string;
    userId: string;
    userName: string;
    user?: User;
}
