import { HttpStatus } from '@nestjs/common';
import { Student } from './entities/student.entity';
import { Repository } from 'typeorm';
import { IStudent } from './interface/student.interface';
import { IPaging } from '../../common/ParamPaging';
export declare class StudentService {
    private studentRepository;
    constructor(studentRepository: Repository<Student>);
    showAll(): Promise<{
        status: HttpStatus;
        message: string;
        data: Student[];
    }>;
    showByClassId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Student[];
    }>;
    showByUserId(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Student[];
    }>;
    showByClassIdPaging(id: string, paging: IPaging): Promise<{
        status: HttpStatus;
        message: string;
        take: number;
        skip: number;
        data: Student[];
    }>;
    create(data: IStudent): Promise<{
        status: HttpStatus;
        message: string;
        data: Student;
    }>;
    read(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Student;
    }>;
    update(id: string, data: Partial<IStudent>): Promise<{
        status: HttpStatus;
        message: string;
        data: Student;
    }>;
    destroy(id: string): Promise<{
        status: HttpStatus;
        message: string;
        data: Student;
    }>;
}
