import { User } from '../../user/entities/user.entity';
import { Class } from '../../class/entities/class.entity';
import { StudentClassSubject } from '../../student-class-subject/entities/student-class-subject.entity';
export declare class Student {
    id: string;
    education: string;
    class: Class;
    classId: string;
    className: string;
    studentClasses: StudentClassSubject[];
    userId: string;
    userName: string;
    user?: User;
}
