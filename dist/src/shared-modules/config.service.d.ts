export declare class ConfigService {
    constructor();
    get(key: string): string;
    getNumber(key: string): number;
}
