"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv = require("dotenv");
class ConfigService {
    constructor() {
        dotenv.config({
            path: `.env`,
        });
    }
    get(key) {
        return process.env[key];
    }
    getNumber(key) {
        return Number(this.get(key));
    }
}
exports.ConfigService = ConfigService;
//# sourceMappingURL=config.service.js.map