import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfig } from './database';
import { MajorModule } from '../src/modules/major/major.module';
import { UserModule } from '../src/modules/user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(TypeOrmConfig),
  ],
})
export class DatabaseModule { }
