import 'dotenv/config';
import { ConfigService } from '../src/shared-modules/config.service';
import Major from '../src/modules/major/entities/major.entity';
import { User } from '../src/modules/user/entities/user.entity';
import { Teacher } from '../src/modules/teacher/entities/teacher.entity';
import { Student } from '../src/modules/student/entities/student.entity';
import { Subject } from '../src/modules/subject/entities/subject.entity';
import { StudentClassSubject } from '../src/modules/student-class-subject/entities/student-class-subject.entity';
import { Semester } from '../src/modules/semester/entities/semester.entity';
import { Schedule } from '../src/modules/schedule/entities/schedule.entity';
import { Room } from '../src/modules/room/entities/room.entity';
import { Class } from '../src/modules/class/entities/class.entity';
import { ClassSubject } from '../src/modules/class-subject/entities/class-subject.entity';
import Notification from '../src/modules/notification/entities/notification.entity';
import { Absent } from '../src/modules/absent/entities/absent.entity';
import { Point } from '../src/modules/point/entities/point.entity';
const config = new ConfigService();

export const TypeOrmConfig: any = {
    type: 'postgres',
    // host: process.env.POSTGRES_HOST,
    // port: process.env.POSTGRES_PORT,
    // username: process.env.POSTGRES_USERNAME,
    // password: process.env.POSTGRES_PASSWORD,
    // database: process.env.POSTGRES_DATABASE,
    host: config.get('POSTGRES_HOST'),
    port: config.getNumber('POSTGRES_PORT'),
    username: config.get('POSTGRES_USERNAME'),
    password: config.get('POSTGRES_PASSWORD'),
    database: config.get('POSTGRES_DATABASE'),
    synchronize: true,
    logging: true,
    // tslint:disable-next-line:max-line-length
    entities: [Major, User, Teacher, Student, Subject, StudentClassSubject, Semester, Schedule, Room, Class, ClassSubject, Absent, Point, Notification],
};
